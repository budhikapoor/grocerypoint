/* 
	created by: Budhi Kapoor
	created at: May 02, 2020
	description:
		delete cart item
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spCart_DeleteCartItem', N'P') IS NOT NULL
	DROP PROCEDURE spCart_DeleteCartItem
GO 
CREATE PROCEDURE dbo.spCart_DeleteCartItem(
	@cartID bigint				-- 0	

)
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN

			DELETE FROM 
				tblCart 
			WHERE 
				( cartID = @cartID )

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO