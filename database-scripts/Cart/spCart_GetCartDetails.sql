/* 
	created by: Budhi Kapoor
	created at: April 20, 2020
	description:
		get cart details
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spCart_GetCartDetails', N'P') IS NOT NULL
	DROP PROCEDURE spCart_GetCartDetails
GO 
CREATE PROCEDURE dbo.spCart_GetCartDetails(
	@storeID bigint,					-- 0	
	@userID bigint						-- 1	
)
AS
BEGIN
	-- 1. get store details
	--SELECT
	--	storeName,
	--	storeID,
	--	c.city
	--FROM
	--	tblStore s		INNER JOIN
	--	tblCity c		ON s.cityID = c.cityID
	--WHERE
	--	( storeID = @storeID )

	-- 2. get cart details
	SELECT
		c.productID,
		c.userID,
		p.productName,
		c.price,
		c.qty,
		c.subtotal,
		p.image,
		c.cartID,
		c.storeID,
		pu.productUnit
	FROM
		tblCart c			INNER JOIN
		tblProduct p		ON c.productID = p.productID LEFT JOIN
		tblProductUnit pu	ON p.productUnitID = pu.productUnitID
	WHERE
		( c.userID = @userID ) AND
		( c.storeID = @storeID )


	-- 3. get cart total
	--SELECT
	--	SUM(ISNULL(subtotal, 0)) As totalCost,
	--	SUM(ISNULL(qty, 0)) AS totalItems
	--FROM
	--	tblCart
	--WHERE
	--	( userID = @userID ) AND
	--	( storeID = @storeID )

END
GO