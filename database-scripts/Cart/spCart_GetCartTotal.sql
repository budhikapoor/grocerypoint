/* 
	created by: Budhi Kapoor
	created at: April 19, 2020
	description:
		get cart total
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spCart_GetCartTotal', N'P') IS NOT NULL
	DROP PROCEDURE spCart_GetCartTotal
GO 
CREATE PROCEDURE dbo.spCart_GetCartTotal(
	@storeID bigint,					-- 0	
	@userID bigint						-- 1	
)
AS
BEGIN
	-- 1. get cart details
	SELECT
		SUM(ISNULL(subtotal, 0)) As totalCost,
		SUM(ISNULL(qty, 0)) AS totalItems,
		SUM(ISNULL(tax, 0)) AS totalTax
	FROM
		tblCart
	WHERE
		( userID = @userID ) AND
		( storeID = @storeID )

END
GO