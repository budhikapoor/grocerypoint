/* 
	created by: Budhi Kapoor
	created at: April 19, 2020
	description:
		save store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spCart_AddProductToCart', N'P') IS NOT NULL
	DROP PROCEDURE spCart_AddProductToCart
GO 
CREATE PROCEDURE dbo.spCart_AddProductToCart(
	@storeID bigint,					-- 0
	@productID bigint,					-- 1
	@qty bigint,						-- 2
	@userID bigint,						-- 3
	@ipAddress nvarchar(20),			-- 4
	@totalItems bigint OUTPUT,			-- 5
	@totalCost money OUTPUT				-- 6
)
AS
BEGIN
DECLARE
	@price money,
	@subtotal money,
	@isGst bit,
	@tax money,
	@total money,
	@totalTax money,
	@dateUtc smalldatetime,
	@taxPerc money

	-- 0.1 get details
	SELECT
		@price = price,
		@isGst = isGST
	FROM
		tblProduct
	WHERE
		( productID = @productID )

	-- 0.1 get tax details
	SELECT
		@price = price,
		@isGst = isGST
	FROM
		tblProduct
	WHERE
		( productID = @productID )

	-- 0.2 get tax details	
	SELECT
		@taxPerc = tax				
	FROM
		tbltax
	WHERE
		( isActive = 1 )

	-- 0.3 calculate subtotal to enter details
	SET @subtotal = @price * @qty
	
	IF @isGst = 1
		SET @tax = (@subtotal * @taxPerc)/100
	
	SET @tax = ISNULL(@tax, 0)
	SET @subtotal = ISNULL(@subtotal, 0)

	SET @total = @subtotal + @tax

	SET @dateUtc = GETUTCDATE()

	
	BEGIN TRY
		BEGIN TRAN

			IF EXISTS(
				SELECT cartID FROM tblCart WHERE productID = @productID AND storeID = @storeID AND userID = @userID
			)
			BEGIN				
				UPDATE tblCart SET
					qty = @qty,
					price = @price,
					subtotal = @subtotal,
					tax = @tax,
					total = @total,
					updatedUserID = @userID,
					dateUpdated = @dateUtc,
					ipAddressUpdated = @ipAddress
				WHERE
					( productID = @productID ) AND
					( storeID = @storeID ) AND
					( userID = @userID )
			END
			ELSE
				-- 1. save cart
				INSERT INTO tblCart(
					productID, qty, price, subtotal, tax, total, userID, dateAdded, addedUserID, ipAddress, storeID
				)
				SELECT @productID, @qty, @price, @subtotal, @tax, @total, @userID, @dateUtc, @userID, @ipAddress, @storeID
		
		-- 2 calculate total items and total in cart
		SELECT
			@totalCost = SUM(ISNULL(subtotal, 0)),
			@totalItems = SUM(ISNULL(qty, 0)),
			@totalTax = SUM(ISNULL(tax, 0))
		FROM
			tblCart
		WHERE
			( userID = @userID ) AND
			( storeID = @storeID )

		SET @totalCost = @totalCost
		SET @totalItems = @totalItems	
		SET @totalTax = @totalTax	
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO