/* 
	created by: Budhi Kapoor
	created at: April 23, 2020
	description:
		delete cart
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spCart_DeleteCart', N'P') IS NOT NULL
	DROP PROCEDURE spCart_DeleteCart
GO 
CREATE PROCEDURE dbo.spCart_DeleteCart(
	@storeID bigint,					-- 0	
	@userID bigint						-- 3
)
AS
BEGIN	
	BEGIN TRY
		BEGIN TRAN

			DELETE FROM 
				tblCart 
			WHERE 
				( storeID = @storeID ) AND 
				( userID = @userID )

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO