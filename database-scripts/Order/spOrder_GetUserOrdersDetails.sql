/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get user order
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_GetUserOrdersDetails', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_GetUserOrdersDetails
GO 
CREATE PROCEDURE dbo.spOrder_GetUserOrdersDetails(
	@orderID bigint						-- 1	
)
AS
BEGIN
	-- 1. get order details
	SELECT	DISTINCT
		o.orderID,
		CASE
			WHEN LEN(o.orderID) = 1 THEN 'GP000' + CAST(o.orderID As nvarchar(16)) 
			WHEN LEN(o.orderID) = 2 THEN 'GP00' + CAST(o.orderID As nvarchar(16)) 
			WHEN LEN(o.orderID) = 3 THEN 'GP0' + CAST(o.orderID As nvarchar(16))
			ELSE 'GP' + CAST(o.orderID As nvarchar(16))
		END As orderNumber,		
		s.storeName + ' - ' + c.city As storeName,		
		CASE 
			WHEN ( LEN(o.orderPhone) = 10 OR CHARINDEX('X', o.orderPhone) > 0 ) THEN ISNULL(dbo.fcFormat_PhoneNumberGet(1, o.orderPhone), '')
			WHEN LEN(o.orderPhone) > 1 THEN ISNULL(o.orderPhone, '')
			ELSE ''
		END As orderPhone,
		dbo.fcFormat_AddressText(
				1, 
				a.unit,
				a.streetAddress , 
				ca.city, 
				pr.provinceAbbr, 
				'Canada',  
				a.postalCode, 
				'') As deliveryAddress,
		o.qty,
		o.subtotal,
		o.tax,
		o.deliveryCharge,
		o.deliveryChargeTax,
		o.tip,
		o.total,		
		CONVERT(varchar, o.dateAdded, 107) As orderDate,
		os.orderStatus
	FROM
		tblOrder o			INNER JOIN
		tblOrderDetails od	ON o.orderID = od.orderID LEFT JOIN
		tblStore s			ON o.storeID = s.storeID LEFT JOIN
		tblOrderStatus os	ON o.orderStatusID = os.orderStatusID LEFT JOIN
		tblCity c			ON s.cityID = c.cityID LEFT JOIN		
		tblAddress a		ON o.addressID = a.addressID LEFT JOIN
		tblCity ca			ON a.cityID = ca.cityID LEFT JOIN
		tblProvince pr		ON a.provinceID = pr.provinceID		
		
	WHERE
		( o.orderID = @orderID )
END
GO