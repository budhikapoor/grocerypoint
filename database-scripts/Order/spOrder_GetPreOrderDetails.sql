/* 
	created by: Budhi Kapoor
	created at: April 19, 2020
	description:
		save store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_GetPreOrderDetails', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_GetPreOrderDetails
GO 
CREATE PROCEDURE dbo.spOrder_GetPreOrderDetails(
	@storeID bigint,					-- 0
	@userID bigint,						-- 1
	@addressID bigint= NULL				-- 2
)
AS
BEGIN
DECLARE	
	@dateUtc smalldatetime,
	@productSubtotal money,
	@totalItems bigint,
	@productTax money,
	@deliveryCharge money,
	@deliveryChargeTax money,
	@taxPerc money,
	@totalWithoutTips money
	
	SET @addressID = ISNULL(@addressID, 0)

	-- 0.1. get cart totals
	SELECT
		@productSubtotal = SUM(ISNULL(subtotal, 0)),
		@totalItems = SUM(ISNULL(qty, 0)),
		@productTax = SUM(ISNULL(tax, 0))
	FROM
		tblCart
	WHERE
		( userID = @userID ) AND
		( storeID = @storeID )

	-- 0.2 get tax details	
	SELECT
		@taxPerc = tax				
	FROM
		tbltax
	WHERE
		( isActive = 1 )

	-- 0.3 get delivery charges
	IF @addressID > 0
		SELECT
			@deliveryCharge = c.deliveryCharge				
		FROM
			tblAddress a	INNER JOIN
			tblCity c		ON a.cityID = c.cityID
		WHERE
			( a.addressID = @addressID )

	SET @deliveryCharge = ISNULL(@deliveryCharge, 0)
	IF @deliveryCharge > 0	
		SET @deliveryChargeTax = (@deliveryCharge * @taxPerc)/100

	SET @productSubtotal  = ISNULL(@productSubtotal, 0)
	SET @totalItems  = ISNULL(@totalItems, 0)
	SET @productTax  = ISNULL(@productTax, 0)
	SET @deliveryChargeTax = ISNULL(@deliveryChargeTax, 0)

	SET @totalWithoutTips = @productSubtotal + @productTax + @deliveryCharge + @deliveryChargeTax

	-- 1. get pre order details
	SELECT	
		@productSubtotal As productSubtotal,
		@totalItems As totalItems,
		@productTax As productTax,
		@deliveryCharge As deliveryCharge,
		@deliveryChargeTax As deliveryChargeTax,
		@totalWithoutTips As totalWithoutTips
END
GO