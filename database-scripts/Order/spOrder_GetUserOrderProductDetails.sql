/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get user order
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_GetUserOrderProductDetails', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_GetUserOrderProductDetails
GO 
CREATE PROCEDURE dbo.spOrder_GetUserOrderProductDetails(
	@orderID bigint						-- 1	
)
AS
BEGIN

	-- 1. get order details
	SELECT
		o.productID,
		o.userID,
		p.productName,
		o.price,
		o.qty,
		o.subtotal,		
		pu.productUnit
	FROM
		tblOrderDetails o			INNER JOIN
		tblProduct p				ON o.productID = p.productID LEFT JOIN
		tblProductUnit pu			ON p.productUnitID = pu.productUnitID	
	WHERE
		( o.orderID = @orderID )
END
GO