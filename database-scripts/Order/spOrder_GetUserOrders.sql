/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get user order
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_GetUserOrders', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_GetUserOrders
GO 
CREATE PROCEDURE dbo.spOrder_GetUserOrders(
	@userID bigint						-- 1	
)
AS
BEGIN

	-- 1. get pre order details
	SELECT	
		o.orderID,
		CASE
			WHEN LEN(o.orderID) = 1 THEN 'GP000' + CAST(o.orderID As nvarchar(16)) 
			WHEN LEN(o.orderID) = 2 THEN 'GP00' + CAST(o.orderID As nvarchar(16)) 
			WHEN LEN(o.orderID) = 3 THEN 'GP0' + CAST(o.orderID As nvarchar(16))
			ELSE 'GP' + CAST(o.orderID As nvarchar(16))
		END As orderNumber,		
		s.storeName + ' - ' + c.city As storeName,
		o.qty,
		o.subtotal,
		o.tax,
		o.total,		
		CONVERT(varchar, o.dateAdded, 107) As orderDate,
		os.orderStatus
	FROM
		tblOrder o			INNER JOIN
		tblStore s			ON o.storeID = s.storeID INNER JOIN
		tblOrderStatus os	ON o.orderStatusID = os.orderStatusID LEFT JOIN
		tblCity c			ON s.cityID = c.cityID
	WHERE
		( userID = @userID )
	ORDER BY
		o.orderID desc
		
END
GO