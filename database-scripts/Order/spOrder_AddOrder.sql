/* 
	created by: Budhi Kapoor
	created at: April 19, 2020
	description:
		save store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_AddOrder', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_AddOrder
GO 
CREATE PROCEDURE dbo.spOrder_AddOrder(
	@storeID bigint,					-- 0
	@userID bigint,						-- 1
	@addressID bigint,					-- 2
	@qty int,						-- 3
	@subtotal money,					-- 4
	@tax money,							-- 5
	@deliveryCharge money,				-- 6
	@serviceCharge money,				-- 7
	@deliveryChargeTax money,			-- 8
	@total money,						-- 9
	@ipAddress nvarchar(50),			-- 10
	@notes nvarchar(MAX),				-- 11
	@deliveryDate smalldatetime,		-- 12
	@time nvarchar(50),					-- 13
	@orderPhone nvarchar(20),			-- 14
	@tip money,							-- 15
	@chargeRef nvarchar(MAX),			-- 16
	@cardType nvarchar(50),				-- 17
	@last4 nvarchar(20),				-- 18
	@orderID bigint OUTPUT				-- 19	
)
AS
BEGIN
DECLARE	
	@dateUtc smalldatetime,
	@storeTotal money

	SET @subtotal = ISNULL(@subtotal, 0)
	SET @tax = ISNULL(@tax, 0)
	SET @storeTotal = @subtotal + @tax

	-- 0.1 get details
	SET @orderPhone = dbo.fcFormat_PhoneNumberForSave(@orderPhone)

	SET @dateUtc = GETUTCDATE()

	
	BEGIN TRY
		BEGIN TRAN
			-- 1. add order
			INSERT INTO tblOrder(
					userID, addressID, qty, subtotal, tax, serviceCharge, deliveryCharge, total, ipAddress, dateAdded, storeID,
					notes, orderStatusID, deliveryDate, deliveryChargeTax, orderPhone, tip, [time], chargeRef, isPaid, storeTotal,
					cardType, last4
				)
				SELECT @userID, @addressID, @qty, @subtotal, @tax, @serviceCharge, @deliveryCharge, @total, @ipAddress, @dateUtc, @storeID,
					@notes, 1, @deliveryDate, @deliveryChargeTax, @orderPhone, @tip, @time, @chargeRef, 0, @storeTotal,
					@cardType, @last4
				
				SET @orderID = SCOPE_IDENTITY()

				-- 2. add order details
				INSERT INTO tblOrderDetails(
					orderID, userID, storeID, productID, price, qty, tax, subtotal, dateAdded, total
				)
				SELECT 
					@orderID, userID, storeID, productID, price, qty, tax, subtotal, @dateUtc, total FROM tblCart
				WHERE
					( userID = @userID ) AND
					( storeID = @storeID ) 

				-- 3. delete cart
				DELETE FROM 
					tblCart 
				WHERE 
					( userID = @userID ) AND				
					( storeID = @storeID ) 


		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO