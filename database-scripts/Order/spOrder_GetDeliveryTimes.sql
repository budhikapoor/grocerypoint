/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get next 10 dates
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_GetDeliveryTimes', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_GetDeliveryTimes
GO 
CREATE PROCEDURE dbo.spOrder_GetDeliveryTimes(
	@selectedDate smalldatetime				-- 0	
)
AS
BEGIN
DECLARE
	@day As int,
	@startTime time,
	@endTime time

	SET @day = DATEPART(DW, @selectedDate)
	
	IF @day IN (1, 7)	
		SET @startTime = '11:00'		
	ELSE
		SET @startTime = '17:00'
		
	SET @endTime = '20:00'	


	; WITH times_CTE(
		time
	) AS
	 (
        SELECT 
			@startTime 
    UNION ALL
        SELECT 
			DATEADD(HOUR, 1, time)
        FROM 
			times_CTE
        WHERE 
			time < @endTime
	) 

	-- 1. get dates
	SELECT 
		time,	
		CONVERT(varchar(15),CAST(time AS TIME),100)  + ' - ' + CONVERT(varchar(15),CAST(DATEADD(HOUR, 1, time) AS TIME),100) As timeShow
	FROM
		times_CTE
END
GO