/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get next 10 dates
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spOrder_GetDeliveryDates', N'P') IS NOT NULL
	DROP PROCEDURE spOrder_GetDeliveryDates
GO 
CREATE PROCEDURE dbo.spOrder_GetDeliveryDates(
	@todayDateTime smalldatetime				-- 0	
)
AS
BEGIN
DECLARE
	@time As time,
	@startDate smalldatetime,
	@endDate smalldatetime

	SET @time = CAST(@todayDateTime as time)
	
	IF @time < '15:00'
	BEGIN
		SET @startDate = @todayDateTime
		SET @endDate = DATEADD(DAY, 10, @startDate)
	END
	ELSE
		SET @startDate = DATEADD(DAY, 1, @todayDateTime)
		SET @endDate = DATEADD(DAY, 10, @startDate)	

	-- 0.1 cte table
	; WITH dates_CTE(
		date
	) AS
	 (
        SELECT 
			@startDate 
    UNION ALL
        SELECT 
			DATEADD(day, 1, date)
        FROM 
			dates_CTE
        WHERE 
			date < @endDate
	) 

	-- 1. get dates
	SELECT 
		date,
		CAST(DATENAME(DW , date) As nvarchar(50)) + ', ' + CAST(DATENAME(MONTH , date) As nvarchar(50)) + ' ' + CAST(DATENAME(DAY , date) As nvarchar(50)) as dateShow
	FROM
		dates_CTE
END
GO