/* 
	created by: Amila 
	created at: April 09, 2019
	description:
		get all cities
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spCity_GetCities', N'P') IS NOT NULL
	DROP PROCEDURE spCity_GetCities
GO 
CREATE PROCEDURE [dbo].spCity_GetCities
AS
BEGIN
	--SET NOCOUNT ON;
    SELECT * FROM tblCity 
END


GO


