/* 
	created by: Budhi Kapoor 
	created at: April 15, 2019
	description:
		get all Property utilities
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetAllUtility', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetAllUtility
GO 
CREATE PROCEDURE spSystem_GetAllUtility	
AS
BEGIN
	--SET NOCOUNT ON;
    SELECT 
		u.utilityID,
		u.utility
	FROM 
		tblUtility u
	WHERE
		( u.isActive = 1 ) 
	ORDER BY
		u.sortOrder
END
GO


