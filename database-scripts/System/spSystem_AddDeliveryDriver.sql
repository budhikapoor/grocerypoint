/* 
	created by: Budhi Kapoor
	created at: May 08, 2020
	description:
		save delivery driver
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spSystem_AddDeliveryDriver', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_AddDeliveryDriver
GO 
CREATE PROCEDURE dbo.spSystem_AddDeliveryDriver(	
	@firstName nvarchar(100),			-- 1
	@lastName nvarchar(100),			-- 2
	@email nvarchar(100),				-- 3
	@phone nvarchar(100),				-- 4
	@dateOfBirth smalldatetime,			-- 5
	@postalCode nvarchar(20),			-- 6
	@provinceID bigint,					-- 7
	@cityID bigint,						-- 8
	@isSundayMorning bit,				-- 9
	@isSundayEvening bit,				-- 10
	@isMondayMorning bit,				-- 11
	@isMondayAfternoon bit,				-- 12
	@isMondayEvening bit,				-- 13
	@isTuesdayMorning bit,				-- 14
	@isTuesdayAfternoon bit,			-- 15
	@isTuesdayEvening bit,				-- 16
	@isWednesdayMorning bit,			-- 17
	@isWednesdayAfternoon bit,			-- 18
	@isWednesdayEvening bit,			-- 19
	@isThursdayMorning bit,				-- 20
	@isThursdayAfternoon bit,			-- 21
	@isThursdayEvening bit,				-- 22
	@isFridayMorning bit,				-- 22
	@isFridayAfternoon bit,				-- 23
	@isFridayEvening bit,				-- 24
	@isSaturdayMorning bit,				-- 25
	@isSaturdayAfternoon bit,			-- 26
	@isSaturdayEvening bit,				-- 27
	@hoursWork int, 					-- 28
	@driverLicense nvarchar(20),		-- 29
	@isLiftWeight bit,					-- 30
	@campaignID int,					-- 31
	@description nvarchar(MAX),			-- 32	
	@ipAddress nvarchar(20),			-- 34	
	@deliveryDriverID bigint OUTPUT		-- 35
)
AS
BEGIN
DECLARE
	@userID bigint

	SET @phone = dbo.fcFormat_PhoneNumberForSave(@phone)
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. save store
			INSERT INTO tblDeliveryDriver(
				firstName, lastName, email, phone, dateOfBirth, postalCode, provinceID, cityID, isSundayMorning, isSundayEvening,
				isMondayMorning, isMondayAfternoon, isMondayEvening, isTuesdayMorning, isTuesdayAfternoon, isTuesdayEvening,
				isWednesdayMorning, isWednesdayAfternoon, isWednesdayEvening, isThursdayMorning, isThursdayAfternoon, isThursdayEvening,
				isFridayMorning, isFridayAfternoon, isFridayEvening, isSaturdayMorning, isSaturdayAfternoon, isSaturdayEvening,
				hoursWork, driverLicense, isLiftWeight, campaignID, description, dateAdded, ipAddress
			)
			VALUES(
				@firstName, @lastName, @email, @phone, @dateOfBirth, @postalCode, @provinceID, @cityID, @isSundayMorning, @isSundayEvening,
				@isMondayMorning, @isMondayAfternoon, @isMondayEvening, @isTuesdayMorning, @isTuesdayAfternoon, @isTuesdayEvening,
				@isWednesdayMorning, @isWednesdayAfternoon, @isWednesdayEvening, @isThursdayMorning, @isThursdayAfternoon, @isThursdayEvening,
				@isFridayMorning, @isFridayAfternoon, @isFridayEvening, @isSaturdayMorning, @isSaturdayAfternoon, @isSaturdayEvening,
				@hoursWork, @driverLicense, @isLiftWeight, @campaignID, @description, GETUTCDATE(), @ipAddress
			)	
		
		SELECT @deliveryDriverID = SCOPE_IDENTITY()		

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO