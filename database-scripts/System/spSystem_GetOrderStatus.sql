/* 
	created by: Budhi Kapoor 
	created at: April 21, 2020
	description:
		get all order status
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetOrderStatus', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetOrderStatus
GO 
CREATE PROCEDURE dbo.spSystem_GetOrderStatus
AS
BEGIN
	SELECT
		orderStatusID,
		orderStatus
	FROM
		tblOrderStatus
	WHERE
		( isActive = 1  )
END


GO


