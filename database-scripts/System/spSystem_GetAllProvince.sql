/* 
	created by: Budhi Kapoor 
	created at: April 16, 2019
	description:
		get all provinces
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetAllProvince', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetAllProvince
GO 
CREATE PROCEDURE dbo.spSystem_GetAllProvince	
AS
BEGIN
	--SET NOCOUNT ON;
    SELECT 
		p.provinceID,
		p.description As province
	FROM 
		tblProvince p
	WHERE
		( p.isActive = 1 ) AND
		( p.countryID = 1 )	
END
GO


