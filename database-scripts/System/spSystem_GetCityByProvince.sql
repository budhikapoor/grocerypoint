/* 
	created by: Budhi Kapoor 
	created at: April 10, 2019
	description:
		get all cities by province
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetCityByProvince', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetCityByProvince
GO 
CREATE PROCEDURE dbo.spSystem_GetCityByProvince(
	@provinceID int			-- 0
)
AS
BEGIN
	--SET NOCOUNT ON;
    SELECT 
		c.cityID,
		c.city
	FROM 
		tblCity c
	WHERE
		( c.provinceID = @provinceID ) 
END
GO


