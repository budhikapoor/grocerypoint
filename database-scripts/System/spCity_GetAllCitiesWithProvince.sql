/* 
	created by: Budhi Kapoor 
	created at: May 09, 2019
	description:
		get all cities with province
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spCity_GetAllCitiesWithProvince', N'P') IS NOT NULL
	DROP PROCEDURE spCity_GetAllCitiesWithProvince
GO
CREATE PROCEDURE [dbo].spCity_GetAllCitiesWithProvince
AS
BEGIN
	--SET NOCOUNT ON;
    SELECT 
		 c.cityID,
		 c.city + ', ' + p.provinceAbbr As city
	FROM 
		tblCity c		INNER JOIN
		tblProvince p	ON c.provinceID = p.provinceID
	WHERE
		( c.isActive = 1 ) 		
END
GO


