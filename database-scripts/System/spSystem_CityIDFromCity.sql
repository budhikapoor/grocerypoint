/*
	created by: Budhi Kapoor
	create at: July, 11, 2019
	description:
		get cityID from city
	
	usage
		global
*/
IF OBJECT_ID (N'dbo.spSystem_CityIDFromCity', N'TF') IS NOT NULL
	DROP PROCEDURE spSystem_CityIDFromCity
GO
CREATE PROCEDURE spSystem_CityIDFromCity(
    @city nvarchar(100),
	@provinceID int,
	@cityID int OUTPUT
)
AS
BEGIN
	IF LEN(@city) > 0
	BEGIN
		IF EXISTS(SELECT c.cityID FROM tblCity c WHERE c.city = @city )
		BEGIN	
			SELECT
				@cityID = c.cityID
			FROM
				tblCity c
			WHERE
				(c.city = @city)
		END
		ELSE
		BEGIN				
			INSERT INTO tblCity(
				city, provinceID, isActive
			)
			SELECT @city, @provinceID, 1

			SET @cityID = SCOPE_IDENTITY()
		END			
	END	
END
GO