/* 
	created by: MUNEER MK
	created at: 26-NOV-19
	description:
		Get Page Sub Content
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetPageSubContent', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetPageSubContent
GO

CREATE PROCEDURE [dbo].[spSystem_GetPageSubContent]
	@contentID bigint
AS
BEGIN	
	select * from tblSubContent where contentId=@contentID and isActive=1
END
