/*
	created by: Budhi Kapoor
	create at: August, 21, 2019
	description:
		get page content
	
	usage
		global
*/
IF OBJECT_ID (N'dbo.spSystem_GetPageContent', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetPageContent
GO 
CREATE PROCEDURE spSystem_GetPageContent(
    @contentID bigint
)
AS
BEGIN
	SELECT
		contentID,
		content,
		name,
		isActive
	FROM
		tblContent
	WHERE
		( contentID = @contentID )

END
GO