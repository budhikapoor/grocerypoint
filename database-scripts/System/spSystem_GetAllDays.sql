/* 
	created by: Budhi Kapoor 
	created at: September 13, 2019
	description:
		get all days
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetAllDays', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetAllDays
GO 
CREATE PROCEDURE spSystem_GetAllDays	
AS
BEGIN
	--SET NOCOUNT ON;
    SELECT 
		d.dayID,
		d.day
	FROM 
		tblDay d
	WHERE
		( d.isActive = 1 ) 	
END
GO


