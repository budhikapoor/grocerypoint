/* 
	created by: Budhi Kapoor 
	created at: April 21, 2020
	description:
		get all campaign
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spSystem_GetCampaign', N'P') IS NOT NULL
	DROP PROCEDURE spSystem_GetCampaign
GO 
CREATE PROCEDURE dbo.spSystem_GetCampaign
AS
BEGIN
	SELECT
		campaignID,
		campaign
	FROM
		tblCampaign
	WHERE
		( isActive = 1  )
END


GO


