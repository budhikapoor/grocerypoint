/* 
	created by: Budhi Kapoor
	created at: April 26, 2020
	description:
		change password
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_ChangePassword', N'P') IS NOT NULL
	DROP PROCEDURE spUser_ChangePassword
GO 
CREATE PROCEDURE [dbo].spUser_ChangePassword(
	@userID bigint,						-- 0	
	@userPasswordHash binary(64),		-- 1
	@userPasswordSalt binary(128),		-- 2
	@ipAddress nvarchar(50)				-- 3	
)
AS
BEGIN
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	DECLARE
		@utcDate AS smalldatetime = GETUTCDATE()
	
	
	-- 0.1. change password
	BEGIN
		UPDATE tblUser SET
			userPasswordHash = @userPasswordHash,
			userPasswordSalt = @userPasswordSalt,
			ipAddressUpdated = @ipAddress,
			dateUpdated = @utcDate
		WHERE
			( userID = @userID )
		
	END
END

