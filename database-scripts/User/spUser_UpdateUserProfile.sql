/* 
	created by: Budhi Kapoor
	created at: Apr 26, 2020
	description:
		update user details
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_UpdateUserProfile', N'P') IS NOT NULL
	DROP PROCEDURE spUser_UpdateUserProfile
GO 
CREATE PROCEDURE dbo.spUser_UpdateUserProfile(
	@userID bigint,						-- 0
	@email nvarchar(100),				-- 1
	@userFirstName nvarchar(100),  		-- 2
	@userLastName nvarchar(100),		-- 3
	@cityID bigint,						-- 4
	@phone nvarchar(50),				-- 5
	@provinceID bigint,					-- 6
	@postalCode nvarchar(20),			-- 7
	@ipAddress nvarchar(50)				-- 8
)
AS
BEGIN		
DECLARE 
	@errNum int,
	@errDesc nvarchar(MAX),
	@utcDate smalldatetime

	SET @phone = dbo.fcFormat_PhoneNumberForSave(@phone)
	SET @utcDate = GETUTCDATE()

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRAN	
	
		-- 1. update user
		UPDATE tblUser SET
			userName = @email,
			ipAddressUpdated = @ipAddress,
			dateUpdated = @utcDate
		WHERE
			( userID = @userID )

		-- 2. update user details
		UPDATE tblUserDetails SET
			userEmail = @email,
			userFirstName = @userFirstName,
			userLastName = @userLastName,
			cityID = @cityID,
			phone = @phone,
			provinceID = @provinceID,
			zipCode = @postalCode
		WHERE
			( userID = @userID )
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @errNum = ERROR_NUMBER()
		SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH		

END
GO



