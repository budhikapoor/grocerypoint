/* 
	created by: Budhi Kapoor
	created at: April 04, 2020
	description:
		get user details
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_GetUserByID', N'P') IS NOT NULL
	DROP PROCEDURE spUser_GetUserByID
GO 
CREATE PROCEDURE dbo.spUser_GetUserByID(
	@userID bigint
)
AS
BEGIN	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON	

	DECLARE	
		@userName nvarchar(100),		
		@userTypeID int,
		@userStatusID int,
		@userFirstName nvarchar(200),
		@userLastName nvarchar(200),
		@phone nvarchar(50),
		@isEmailVerified bit,
		@isFirstLogin bit,
		@profilePhoto nvarchar(200),
		@facebook nvarchar(200),
		@linkedin nvarchar(200),
		@phoneShow nvarchar(50),		
		@ext nvarchar(20),
		@i int,
		@userGetID bigint,
		@storeID bigint,
		@storeName nvarchar(200),
		@city nvarchar(200),
		@cityID bigint,
		@postalCode nvarchar(50),
		@provinceID bigint,
		@province nvarchar(100)

	-- 0.1. get details
	SELECT		
		@userGetID = u.userID,		
		@userName = u.userName,	
		@userTypeID = u.userTypeID,
		@userStatusID = u.userStatusID,
		@userFirstName = ud.userFirstName,
		@userLastName = ISNULL(ud.userLastName, ''),
		@phone = ISNULL(ud.phone, ''),
		@isEmailVerified = ISNULL(u.isEmailVerified, 0),
		@isFirstLogin = ISNULL(u.isFirstLogin, 0),
		@profilePhoto = ISNULL(ud.profilePhoto, ''),
		@facebook = ISNULL(ud.facebook, ''),
		@linkedin = ISNULL(ud.linkedin, ''),
		@storeID = ISNULL(u.storeID, 0),
		@storeName = ISNULL(s.storeName, ''),
		@cityID = ISNULL(ud.cityID, 0),
		@city = ISNULL(c.city, ''),
		@postalCode = ISNULL(ud.zipCode, ''),
		@provinceID = ISNULL(ud.provinceID, 0),
		@province = ISNULL(p.description, '')
	FROM
		tblUser u			INNER JOIN
		tblUserDetails ud	ON u.userID = ud.userID LEFT JOIN
		tblStore s			ON u.storeID = s.storeID LEFT JOIN
		tblCity c			ON ud.cityID = c.cityID LEFT JOIN
		tblProvince p		ON ud.provinceID = p.provinceID
	WHERE
		( u.userID = @userID )

	-- 0.2. get phone show
	IF (LEN(@phone) = 10 OR CHARINDEX('X', @phone) > 0 )
		SET @phoneShow = dbo.fcFormat_PhoneNumberGet(1, @phone)
	ELSE
		SET @phoneShow = @phone

	-- 0.3. get ext and phone
	SET @i = CHARINDEX('X', @phone)	
	
	
	IF @i > 0
	BEGIN
		SET @ext = RIGHT(@phone, LEN(@phone) - @i)
		SET @phone = LEFT(@phone, @i - 1)
	END
	ELSE
		SET @ext = ''

	-- 0.1. get all details
	SELECT
		@userGetID AS userID,		
		@userName AS email,		
		@userTypeID AS userTypeID,
		@userStatusID AS userStatusID,
		@userFirstName AS userFirstName,
		@userLastName AS userLastName,
		@phone AS phone,
		@isEmailVerified AS isEmailVerified,
		--@isFirstLogin AS isFirstLogin,
		--@profilePhoto AS profilePhoto,
		--@facebook AS facebook,
		--@linkedin AS linkedin,
		@phoneShow As phoneShow,
		@ext As ext,
		@storeID AS storeID,
		@storeName As storeName,
		@cityID AS cityID,
		@city As city,
		@postalCode As postalCode,
		@provinceID As provinceID,
		@province As province
END
GO