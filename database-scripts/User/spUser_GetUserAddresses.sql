/* 
	created by: Budhi Kapoor
	created at: April 24, 2020
	description:
		get user addresses
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_GetUserAddresses', N'P') IS NOT NULL
	DROP PROCEDURE spUser_GetUserAddresses
GO 
CREATE PROCEDURE dbo.spUser_GetUserAddresses(
	@userID bigint,				-- 0
	@addressID bigint = NULL	-- 1
)
AS
BEGIN	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	SET @addressID = ISNULL(@addressID, 0)

	-- 0.1. get all details
	SELECT		
		a.addressID,
		a.addressDirections,
		a.addressTitle,		
		a.postalCode,		
		a.streetAddress,
		a.unit,
		a.userID,
		c.city,
		p.provinceAbbr,
		at.addressType,
		CASE
			WHEN @addressID > 0 AND a.addressID = @addressID THEN CAST(1 As bit)
			ELSE CAST(0 As bit)
		END As isSelected
	FROM
		tblAddress a		INNER JOIN
		tblCity c			ON a.cityID = c.cityID INNER JOIN
		tblProvince p		ON a.provinceID = p.provinceID INNER JOIN
		tblAddressType at	ON a.addressTypeID = at.addressTypeID
	WHERE
		( a.isActive = 1 ) AND	
		( a.userID = @userID )
END
GO