/* 
	created by: Budhi Kapoor
	created at: Apr 18, 2020
	description:
		save user
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_UpdateStore', N'P') IS NOT NULL
	DROP PROCEDURE spUser_UpdateStore
GO 
CREATE PROCEDURE dbo.spUser_UpdateStore(
	@userID bigint,						-- 0
	@storeID nvarchar(100)				-- 1	
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;



	BEGIN TRY
		BEGIN TRAN	
	
		-- 1. add new user
		UPDATE tblUser SET
			storeID = @storeID,
			dateUpdated = GETUTCDATE()
		WHERE
			( userID = @userID )
		
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH		

END
GO



