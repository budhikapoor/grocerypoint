/* 
	created by: MUNEER MK
	created at: 26-NOV-19
	description:
		Forgot Password
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_ForgotPassword', N'P') IS NOT NULL
	DROP PROCEDURE spUser_ForgotPassword
GO 

CREATE PROCEDURE [dbo].[spUser_ForgotPassword] 
	@email nvarchar(100),
	@guid varchar(100),
	@status bit out
AS
BEGIN

  if EXISTS (SELECT * from tblUserDetails WHERE userEmail = @email)

  begin
  insert into tblforgotPassword (email,createdOn,[guid],isReset) values (@email,getdate(),@guid,0)
  set @status=1
  end
  else
    set @status=0

END





