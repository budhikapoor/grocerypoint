/* 
	created by: Budhi Kapoor
	created at: Apr 24, 2020
	description:
		save user address
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_AddAddress', N'P') IS NOT NULL
	DROP PROCEDURE spUser_AddAddress
GO 
CREATE PROCEDURE dbo.spUser_AddAddress(
	@userID bigint,						-- 0
	@streetAddress nvarchar(100),	    -- 1
	@unit nvarchar(50),  				-- 2
	@provinceID bigint,					-- 3
	@cityID bigint,						-- 4
	@postalCode nvarchar(50),			-- 5
	@addressTitle nvarchar(100),		-- 6
	@addressTypeID int,					-- 7
	@addressDirections  nvarchar(200),	-- 8
	@ipAddress  nvarchar(50),			-- 9
	@addressID bigint OUTPUT			-- 10
)
AS
BEGIN		
DECLARE 
	@errNum int,
	@errDesc nvarchar(MAX)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRAN	
	
		-- 1. add new user
		INSERT INTO tblAddress(
			userID, streetAddress, unit, cityID, provinceID, postalCode, countryID, addressTitle, addressTypeID, addressDirections,
			dateAdded, ipAddress, isActive, isDeleted
		)  
		VALUES(
			@userID, @streetAddress, @unit, @cityID, @provinceID, @postalCode, 1, @addressTitle, @addressTypeID, @addressDirections,
			GETUTCDATE(), @ipAddress, 1, 0
		) 

		SET @addressID = SCOPE_IDENTITY()		
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @errNum = ERROR_NUMBER()
		SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH		

END
GO



