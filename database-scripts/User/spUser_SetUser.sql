/* 
	created by: Budhi Kapoor
	created at: Apr 18, 2020
	description:
		save user
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_SetUser', N'P') IS NOT NULL
	DROP PROCEDURE spUser_SetUser
GO 
CREATE PROCEDURE dbo.spUser_SetUser(
	@userFirstName nvarchar(100),		-- 0
	@userLastName nvarchar(100),	    -- 1
	@userTypeID tinyint,  				-- 2
	@userName nvarchar(150),			-- 3
	@userPasswordHash binary(64),		-- 4
	@userPasswordSalt binary(128),		-- 5
	@hearByID tinyint,					-- 6
	@userPhone nvarchar(30),			-- 7
	@cityID bigint,						-- 8
	@userID bigint OUTPUT				-- 9
)
AS
BEGIN		
DECLARE 
	@errNum int,
	@errDesc nvarchar(MAX),
	@provinceID bigint

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SET @userPhone = dbo.fcFormat_PhoneNumberForSave(@userPhone)

	-- 0.1 get province ID
	IF @cityID > 0
		SELECT
			@provinceID = provinceID
		FROM		
			tblCity
		WHERE
			( cityID = @cityID ) 

	

	BEGIN TRY
		BEGIN TRAN	
	
		-- 1. add new user
		INSERT INTO tblUser(
			userTypeID, userName, userLoginFailCount, userPasswordHash,
			userPasswordSalt, isFirstLogin, isEmailVerified, userStatusID
		)  
		VALUES(
			@userTypeID, @userName, 0, @userPasswordHash, 
			@userPasswordSalt, 1, 0, 1
		) 

		SET @userID = SCOPE_IDENTITY()

		INSERT INTO tblUserDetails(
			userID, userFirstName, userLastName, hearByID, userEmail, phone, cityID, provinceID, dateAdded
		)
		VALUES(
			@userID, @userFirstName, @userLastName, @hearByID, @userName, @userPhone, @cityID, @provinceID, GETUTCDATE()
		)
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @errNum = ERROR_NUMBER()
		SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH		

END
GO



