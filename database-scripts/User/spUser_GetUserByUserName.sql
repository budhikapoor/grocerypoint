/* 
	created by: Budhi Kapoor
	created at: April 04, 2020
	description:
		get user details
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_GetUserByUserName', N'P') IS NOT NULL
	DROP PROCEDURE spUser_GetUserByUserName
GO 
CREATE PROCEDURE dbo.spUser_GetUserByUserName 
	@userName nvarchar(150)
AS
BEGIN
	--SET NOCOUNT ON;
	DECLARE
		@isFirstLogin bit

	--0.1. update is first login to 0 to if login first time
	SELECT
		@isFirstLogin = u.isFirstLogin
	FROM
		tblUser u
	WHERE
		( u.userName = @userName )

	IF ISNULL(@isFirstLogin, 0) = 1
		UPDATE tblUser SET
			isFirstLogin = 0
		WHERE
			( userName = @userName )

    SELECT 
		userName,
		u.userID, 
		userPasswordHash,
		userPasswordSalt,
		userTypeID,	
		ISNULL(ud.userFirstName, '') AS userFirstName,
		u.isEmailVerified,
		ISNULL(@isFirstLogin, 
		CAST(0 as Bit)) AS isFirstLogin,
		ISNULL(ud.profilePhoto, '') AS profilePhoto,
		ISNULL(u.storeID, 0) AS storeID,
		ISNULL(s.storeName, '') AS storeName,
		ISNULL(ud.cityID, 0) AS cityID,
		ISNULL(c.city, '') AS city, 
		ISNULL(ud.zipCode, '') As postalCode,
		ISNULL(ud.provinceID, 0) As provinceID,
		ISNULL(p.description, '') AS province
    FROM 
		tblUser u			INNER JOIN
		tblUserDetails ud	ON u.userID = ud.userID LEFT JOIN
		tblStore s			ON u.storeID = s.storeID LEFT JOIN
		tblCity c			ON ud.cityID = c.cityID LEFT JOIN
		tblProvince p		ON ud.provinceID = p.provinceID
	WHERE 
		( userName= @userName )
END

GO


