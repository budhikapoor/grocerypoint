/* 
	created by: Budhi Kapoor
	created at: Apr 18, 2020
	description:
		save user
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spUser_VerifyEmail', N'P') IS NOT NULL
	DROP PROCEDURE spUser_VerifyEmail
GO 
CREATE PROCEDURE dbo.spUser_VerifyEmail(
	@userID bigint,						-- 0
	@email nvarchar(100),				-- 1
	@message nvarchar(100) OUTPUT		-- 2
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
DECLARE
	@isEmailVerified bit

	IF EXISTS(
				SELECT userID FROM tblUser WHERE userID = @userID AND userName = @email
			)
	BEGIN
		SELECT
			@isEmailVerified = isEmailVerified
		FROM
			tblUser
		WHERE
			( userID = @userID ) AND
			( userName = @email )

		IF @isEmailVerified = 1
		BEGIN
			SET @message = 'Email already verified!'			
			RETURN		
		END


		BEGIN TRY
			BEGIN TRAN
	
			-- 1. add new user
			UPDATE tblUser SET
				isEmailVerified = 1
			WHERE
				( userID = @userID ) AND
				( userName = @email )
		
			SET @message = 'Thanks for verifying your email. Please login.'
		
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			--SET @errNum = ERROR_NUMBER()
			--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
		END CATCH
	END
	ELSE
		SET @message = 'Account doesn''t exist!'	
	

END
GO



