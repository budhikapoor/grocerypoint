/* 
	created by: Budhi Kapoor
	created at: April 27, 2020
	description:
		replace data
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spEmail_GetReplaceEmailData', N'P') IS NOT NULL
	DROP PROCEDURE spEmail_GetReplaceEmailData
GO 
CREATE PROCEDURE spEmail_GetReplaceEmailData(	
    @userID int = NULL,										-- 1
	@businessID int = NULL,									-- 2
	@propertyID int = NULL,									-- 3
	@objectTypeID int = NULL,								-- 4
	@roommateRoomID int = NULL,								-- 5
	@templateBody nvarchar(max)		            OUTPUT,		-- 6	
	@templateSubject nvarchar(max)	            OUTPUT,		-- 7
	@templateFrom nvarchar(max) = NULL			OUTPUT		-- 8
)
AS
BEGIN
DECLARE
	@body nvarchar(MAX),
	@fromEmail nvarchar(100),
	@subject nvarchar(300),
	@firstName nvarchar(100),
	@lastName nvarchar(100),
	@userTypeID int,
	@email nvarchar(100),
	@isMergeContent bit
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	-- 0.1. get templates details
	IF @emailTemplateID > 0
		SELECT
			@fromEmail = fromEmail,
			@subject = [subject],
			@body = description
		FROM
			tblEmailTemplate
		WHERE
			( emailTemplateID = @emailTemplateID )


	-- 0.2. get templates details
	IF @userID > 0
		SELECT
			@firstName = ud.userFirstName,
			@lastName = ud.userLastName,
			@email = ud.userEmail,
			@userTypeID = u.userTypeID		
		FROM
			tblUser u			INNER JOIN
			tblUserDetails ud	ON u.userID = ud.userID
		WHERE
			( u.userID = @userID )

	-- 0.3. get template details
	IF @objectTypeID = 1
	BEGIN
		SELECT
			propertyID
		FROM
			tblProperty
		WHERE
			( propertyID = @propertyID )

	END
	ELSE IF @objectTypeID = 2
	BEGIN
		SELECT
			roommateRoomID
		FROM
			tblRoommateRoom
		WHERE
			( roommateRoomID = @roommateRoomID )
	END
	
	-- 0.4. determine if content is embedded
	IF CHARINDEX('[', @subject) > 0 OR CHARINDEX('[', @body) > 0
		SET @isMergeContent = 1
	ELSE
		SET @isMergeContent = 0

	-- 0.4. merge data	
	IF @isMergeContent = 1
	BEGIN
		SET @firstName = ISNULL(@firstName, '')
		SET @lastName = ISNULL(@lastName, '')
		SET @email = ISNULL(@email, '')

		SET @subject = REPLACE(@subject, '[0]', @firstName)
		SET @subject = REPLACE(@subject, '[1]', @lastName)
		SET @subject = REPLACE(@subject, '[2]', @email)
		SET @subject = REPLACE(@subject, '[4]', @userID)

		SET @body = REPLACE(@body, '[0]', @firstName)
		SET @body = REPLACE(@body, '[1]', @lastName)
		SET @body = REPLACE(@body, '[2]', @email)
		SET @body = REPLACE(@body, '[4]', @userID)

		SET @templateBody = ISNULL(@body, '')
		SET @templateBody = ISNULL(@subject, '')
		SET @templateFrom = ISNULL(@fromEmail, '')
	END
END
GO