/*
	created by: Budhi Kapoor
	create at: April, 27, 2020
	description:
		get email details
		[0] - User First name
		[1] - website url
		[2] - user email
		[3] - userID
		[4] - Order ID
		[5] - Order Number
		[6] - Order date
		[7] - Delivery Date
		[8] - User Full Name
		[9] - Order Phone
		[10] - Order Delivery Address
		[11] - Order Details Table row
		[12] - Order subtotal
		[13] - Order Tax
		[14] - Order Delivery Charge
		[15] - Order Delivery Charge Tax
		[16] - Order Tip
		[17] - Order Total
		[18] - Store Name
		[19] - Store User First Name
		[20] - Store Order Total
		[21] - Order Instructions		
	usage
		global
*/
IF OBJECT_ID (N'dbo.spEmail_GetEmailDetails', N'P') IS NOT NULL
	DROP PROCEDURE spEmail_GetEmailDetails
GO
CREATE PROCEDURE dbo.spEmail_GetEmailDetails(
    @emailTemplateID int,				-- 0
	@userID bigint = NULL,				-- 1
	@storeID bigint = NULL,				-- 2
	@orderID bigint = NULL,				-- 2
	@userTypeID int						-- 3	
)
AS
BEGIN
DECLARE	
	@firstName nvarchar(100),
	@lastName nvarchar(100),	
	@email nvarchar(100),
	@isMergeContent bit,
	@baseUrl nvarchar(200),
	@fromEmail nvarchar(100),
	@subject nvarchar(300),
	@body nvarchar(MAX),
	@toEmail nvarchar(100),
	@displayName nvarchar(200),
	@orderNumber nvarchar(200),
	@userFullName nvarchar(200),
	@orderDate nvarchar(50),
	@orderPhone nvarchar(50),
	@deliveryAddress nvarchar(MAX),
	@orderQuantity int,
	@orderSubtotal money,
	@orderTax money,
	@orderDeliveryCharge money,
	@orderDeliveryChargeTax money,
	@orderTips money,
	@orderTotal money,
	@orderStatus nvarchar(100),
	@orderDeliveryDate nvarchar(100),
	@orderDeliveryTime nvarchar(100),
	@storeName nvarchar(200),
	@isGetOrderProductDetailsRow bit,
	@productName nvarchar(100),
	@chargeID int,
	@productID bigint,	
	@qty int,
	@price money,
	@subtotal money,
	@tax money,	
	@total money,
	@productUnit nvarchar(100),
	@orderCharges nvarchar(MAX),
	@storeUserID bigint,
	@storeFirstName nvarchar(200),
	@storeOrderTotal money,
	@orderInstructions nvarchar(MAX)

DECLARE @charges TABLE(
	chargeID int IDENTITY(1, 1),
	productID bigint,
	productName nvarchar(MAX),
	qty int,
	price money,
	subtotal money,
	tax money,
	total money,
	productUnit nvarchar(100)
)

	SET @orderCharges = ''	
	--SET @baseUrl = 'https://localhost:44330/'
	SET @baseUrl = 'https://grocerypoint.ca/'	

	-- 0.1. get template details
	IF @emailTemplateID > 0
		SELECT
			@fromEmail = fromEmail,
			@subject = subject,
			@body = description,
			@displayName = 'Grocery Point'
		FROM
			tblEmailTemplate
		WHERE
			( emailTemplateID = @emailTemplateID )

	
	-- 0.2. get content is required
	IF CHARINDEX('[11]', @subject) > 0 OR CHARINDEX('[11]', @body) > 0
		SET @isGetOrderProductDetailsRow = 1

	-- 0.2. get email to
	IF @userTypeID = 3 --customer
		SELECT
			@toEmail = userName
		FROM
			tblUser
		WHERE
			( userID = @userID )
	ELSE IF @userTypeID = 2 --store
		SELECT
			@toEmail = email,
			@storeUserID = userMainID
		FROM
			tblStore
		WHERE
			( storeID = @storeID )

	SET @storeUserID = ISNUll(@storeUserID, 0)

	IF @storeUserID > 0
		SELECT
			@storeFirstName = ud.userFirstName						
		FROM
			tblUser u			INNER JOIN
			tblUserDetails ud	ON u.userID = ud.userID
		WHERE
			( u.userID = @storeUserID )


	-- 0.3. get templates details
	IF @userID > 0
		SELECT
			@firstName = ud.userFirstName,
			@lastName = ud.userLastName,
			@email = u.userName,
			@userFullName = ud.userFirstName + ' ' + ud.userLastName			
		FROM
			tblUser u			INNER JOIN
			tblUserDetails ud	ON u.userID = ud.userID
		WHERE
			( u.userID = @userID )	

	-- 0.4. get Order summary
	--IF @orderID > 0
	--	SELECT
	--		@orderNumber = 			
	--			CASE
	--				WHEN LEN(o.orderID) = 1 THEN 'GP000' + CAST(o.orderID As nvarchar(16)) 
	--				WHEN LEN(o.orderID) = 2 THEN 'GP00' + CAST(o.orderID As nvarchar(16)) 
	--				WHEN LEN(o.orderID) = 3 THEN 'GP0' + CAST(o.orderID As nvarchar(16))
	--				ELSE 'GP' + CAST(o.orderID As nvarchar(16))
	--			END,
	--		@orderPhone = 
	--			CASE 
	--				WHEN ( LEN(o.orderPhone) = 10 OR CHARINDEX('X', o.orderPhone) > 0 ) THEN ISNULL(dbo.fcFormat_PhoneNumberGet(1, o.orderPhone), '')
	--				WHEN LEN(o.orderPhone) > 1 THEN ISNULL(o.orderPhone, '')
	--				ELSE ''
	--			END,
	--		@deliveryAddress = 
	--			dbo.fcFormat_AddressText(
	--					1, 
	--					a.unit,
	--					a.streetAddress , 
	--					ca.city, 
	--					pr.provinceAbbr, 
	--					'Canada',  
	--					a.postalCode, 
	--					''),
	--		@orderQuantity = o.qty,
	--		@orderSubtotal = o.subtotal,
	--		@orderTax = o.tax,
	--		@orderDeliveryCharge = o.deliveryCharge,
	--		@orderDeliveryChargeTax = o.deliveryChargeTax,
	--		@orderTips = o.tip,
	--		@orderTotal = o.total,		
	--		@orderDate = CONVERT(varchar, o.dateAdded, 107),
	--		@orderStatus = os.orderStatus,
	--		@orderDeliveryDate = CONVERT(varchar, o.deliveryDate, 107),
	--		@storeName = s.storeName + ' - ' + c.city,
	--		@orderInstructions = ISNULL(o.notes, '')
	--	FROM
	--		tblOrder o			INNER JOIN
	--	--	tblOrderDetails od	ON o.orderID = od.orderID LEFT JOIN
	--		tblStore s			ON o.storeID = s.storeID LEFT JOIN
	--		tblOrderStatus os	ON o.orderStatusID = os.orderStatusID LEFT JOIN
	--		tblCity c			ON s.cityID = c.cityID LEFT JOIN		
	--		tblAddress a		ON o.addressID = a.addressID LEFT JOIN
	--		tblCity ca			ON a.cityID = ca.cityID LEFT JOIN
	--		tblProvince pr		ON a.provinceID = pr.provinceID
	--	WHERE
	--		( o.orderID = @orderID )

	-- 0.5. get Order product details
	IF @isGetOrderProductDetailsRow = 1 AND @orderID > 0
	BEGIN
		INSERT INTO @charges
		SELECT
			o.productID,			
			p.productName,
			o.qty,
			o.price,			
			o.subtotal,	
			o.tax,
			o.total,	
			pu.productUnit
		FROM
			tblOrderDetails o			INNER JOIN
			tblProduct p				ON o.productID = p.productID LEFT JOIN
			tblProductUnit pu			ON p.productUnitID = pu.productUnitID	
		WHERE
			( o.orderID = @orderID )		
			ORDER BY				
				p.productName

		SELECT TOP 1
			@chargeID = chargeID,			
			@productName = productName,
			@qty = qty,
			@price = price,
			@subtotal = subtotal,
			@tax = tax,
			@total = total,
			@productUnit = productUnit
		FROM
			@charges
		ORDER BY
			chargeID
		
		WHILE @chargeID > 0 
		BEGIN			
			SET @orderCharges = @orderCharges + '<tr>'
	
			--Qty td
			SET @orderCharges = @orderCharges + '<td valign="top" align="center">'
			SET @orderCharges = @orderCharges + CAST(@qty As varchar(16))
			SET @orderCharges = @orderCharges + '</td>'

			--product name td
			SET @orderCharges = @orderCharges + '<td valign="top" align="center">'
			SET @orderCharges = @orderCharges + @productName
			SET @orderCharges = @orderCharges + '</td>'

			--price td
			SET @orderCharges = @orderCharges + '<td valign="top" align="center">'
			SET @orderCharges = @orderCharges + '$' + CAST(@price As varchar(16))
			SET @orderCharges = @orderCharges + '</td>'

			--subtotal td
			SET @orderCharges = @orderCharges + '<td valign="top" align="center">'
			SET @orderCharges = @orderCharges + '$' + CAST(@subtotal As varchar(16))
			SET @orderCharges = @orderCharges + '</td>'

			--tax td
			SET @orderCharges = @orderCharges + '<td valign="top" align="center">'
			SET @orderCharges = @orderCharges + '$' + CAST(@tax As varchar(16))
			SET @orderCharges = @orderCharges + '</td>'
			
			--total td
			SET @orderCharges = @orderCharges + '<td valign="top" align="center">'
			SET @orderCharges = @orderCharges + '$' + CAST(@total As varchar(16))
			SET @orderCharges = @orderCharges + '</td>'

			--tr
			SET @orderCharges = @orderCharges + '</tr>'

			SELECT TOP 1
				@chargeID = chargeID,			
				@productName = productName,
				@qty = qty,
				@price = price,
				@subtotal = subtotal,
				@tax = tax,
				@total = total,
				@productUnit = productUnit
			FROM
				@charges
			WHERE
				( chargeID > @chargeID )
			ORDER BY
				chargeID

			IF @@ROWCOUNT > 0 
				CONTINUE
			ELSE
				BREAK
		END
	END

	DELETE FROM @charges
	-- 0.5. get Order details
	IF @orderID > 0
		SELECT
			@orderNumber = 			
				CASE
					WHEN LEN(o.orderID) = 1 THEN 'GP000' + CAST(o.orderID As nvarchar(16)) 
					WHEN LEN(o.orderID) = 2 THEN 'GP00' + CAST(o.orderID As nvarchar(16)) 
					WHEN LEN(o.orderID) = 3 THEN 'GP0' + CAST(o.orderID As nvarchar(16))
					ELSE 'GP' + CAST(o.orderID As nvarchar(16))
				END,
			@orderPhone = 
				CASE 
					WHEN ( LEN(o.orderPhone) = 10 OR CHARINDEX('X', o.orderPhone) > 0 ) THEN ISNULL(dbo.fcFormat_PhoneNumberGet(1, o.orderPhone), '')
					WHEN LEN(o.orderPhone) > 1 THEN ISNULL(o.orderPhone, '')
					ELSE ''
				END,
			@deliveryAddress = 
				dbo.fcFormat_AddressText(
						1, 
						a.unit,
						a.streetAddress , 
						ca.city, 
						pr.provinceAbbr, 
						'Canada',  
						a.postalCode, 
						''),
			@orderQuantity = o.qty,
			@orderSubtotal = o.subtotal,
			@orderTax = o.tax,
			@orderDeliveryCharge = o.deliveryCharge,
			@orderDeliveryChargeTax = o.deliveryChargeTax,
			@orderTips = o.tip,
			@orderTotal = o.total,		
			@storeOrderTotal = o.storeTotal,
			@orderDate = CONVERT(varchar, o.dateAdded, 107),
			@orderStatus = os.orderStatus,
			@orderDeliveryDate = CONVERT(varchar, o.deliveryDate, 107),
			@storeName = s.storeName + ' - ' + c.city,
			@orderInstructions = ISNULL(o.notes, '')
		FROM
			tblOrder o			INNER JOIN
		--	tblOrderDetails od	ON o.orderID = od.orderID LEFT JOIN
			tblStore s			ON o.storeID = s.storeID LEFT JOIN
			tblOrderStatus os	ON o.orderStatusID = os.orderStatusID LEFT JOIN
			tblCity c			ON s.cityID = c.cityID LEFT JOIN		
			tblAddress a		ON o.addressID = a.addressID LEFT JOIN
			tblCity ca			ON a.cityID = ca.cityID LEFT JOIN
			tblProvince pr		ON a.provinceID = pr.provinceID
		WHERE
			( o.orderID = @orderID )
	
	-- 0.4. determine if content is embedded
	IF CHARINDEX('[', @subject) > 0 OR CHARINDEX('[', @body) > 0
		SET @isMergeContent = 1
	ELSE
		SET @isMergeContent = 0

	-- 0.4. merge data	
	IF @isMergeContent = 1
	BEGIN
		SET @firstName = ISNULL(@firstName, '')
		SET @lastName = ISNULL(@lastName, '')
		SET @email = ISNULL(@email, '')
		SET @storeFirstName = ISNULL(@storeFirstName, '')


		IF @orderID > 0
		BEGIN
			SET @orderID = ISNULL(@orderID, '')
			SET @orderNumber = ISNULL(@orderNumber, '')
			SET @orderNumber = ISNULL(@orderNumber, '')
			SET @userFullName = ISNULL(@userFullName, '')
			SET @orderDate = ISNULL(@orderDate, '')
			SET @orderPhone = ISNULL(@orderPhone, '')
			SET @deliveryAddress = ISNULL(@deliveryAddress, '')
			SET @orderQuantity = ISNULL(@orderQuantity, 0)
			SET @orderSubtotal = ISNULL(@orderSubtotal, 0)
			SET @orderTax = ISNULL(@orderTax, 0)
			SET @orderDeliveryCharge = ISNULL(@orderDeliveryCharge, 0)
			SET @orderDeliveryChargeTax = ISNULL(@orderDeliveryChargeTax, 0)
			SET @orderTips = ISNULL(@orderTips, 0)		
			SET @storeOrderTotal = ISNULL(@storeOrderTotal, 0)
			SET @orderTotal = ISNULL(@orderTotal, 0)
			SET @orderStatus = ISNULL(@orderStatus, '')
			SET @orderDeliveryDate = ISNULL(@orderDeliveryDate, '')
			SET @storeName = ISNULL(@storeName, '')
			SET @orderCharges = ISNULL(@orderCharges, '')				
			

		END

		SET @subject = REPLACE(@subject, '[0]', @firstName)
		SET @subject = REPLACE(@subject, '[1]', @baseUrl)
		SET @subject = REPLACE(@subject, '[2]', @email)
		SET @subject = REPLACE(@subject, '[3]', @userID)
		SET @subject = REPLACE(@subject, '[8]', @userFullName)
		SET @subject = REPLACE(@subject, '[19]', @storeFirstName)

		IF @orderID > 0
		BEGIN
			SET @subject = REPLACE(@subject, '[4]', @orderID)
			SET @subject = REPLACE(@subject, '[5]', @orderNumber)
			SET @subject = REPLACE(@subject, '[6]', @orderDate)
			SET @subject = REPLACE(@subject, '[7]', @orderDeliveryDate)
			SET @subject = REPLACE(@subject, '[9]', @orderPhone)
			SET @subject = REPLACE(@subject, '[10]', @deliveryAddress)
			SET @subject = REPLACE(@subject, '[11]', @orderCharges)
			SET @subject = REPLACE(@subject, '[12]', '$' + CAST(@orderSubtotal As varchar(16)))
			SET @subject = REPLACE(@subject, '[13]', '$' + CAST(@orderTax As varchar(16)))
			SET @subject = REPLACE(@subject, '[14]', '$' + CAST(@orderDeliveryCharge As varchar(16)))
			SET @subject = REPLACE(@subject, '[15]', '$' + CAST(@orderDeliveryChargeTax As varchar(16)))
			SET @subject = REPLACE(@subject, '[16]', '$' + CAST(@orderTips As varchar(16)))
			SET @subject = REPLACE(@subject, '[17]', '$' + CAST(@orderTotal As varchar(16)))
			SET @subject = REPLACE(@subject, '[18]', @storeName)
			SET @subject = REPLACE(@subject, '[20]', '$' + CAST(@storeOrderTotal As varchar(16)))
			SET @subject = REPLACE(@subject, '[21]', @orderInstructions)		
		END

		SET @body = REPLACE(@body, '[0]', @firstName)
		SET @body = REPLACE(@body, '[1]', @baseUrl)
		SET @body = REPLACE(@body, '[2]', @email)
		SET @body = REPLACE(@body, '[3]', @userID)
		SET @body = REPLACE(@body, '[8]', @userFullName)
		SET @body = REPLACE(@body, '[19]', @storeFirstName)

		IF @orderID > 0
		BEGIN
			SET @body = REPLACE(@body, '[4]', @orderID)
			SET @body = REPLACE(@body, '[5]', @orderNumber)
			SET @body = REPLACE(@body, '[6]', @orderDate)
			SET @body = REPLACE(@body, '[7]', @orderDeliveryDate)
			SET @body = REPLACE(@body, '[9]', @orderPhone)
			SET @body = REPLACE(@body, '[10]', @deliveryAddress)
			SET @body = REPLACE(@body, '[11]', @orderCharges)
			SET @body = REPLACE(@body, '[12]', '$' + CAST(@orderSubtotal As varchar(16)))
			SET @body = REPLACE(@body, '[13]', '$' + CAST(@orderTax As varchar(16)))
			SET @body = REPLACE(@body, '[14]', '$' + CAST(@orderDeliveryCharge As varchar(16)))
			SET @body = REPLACE(@body, '[15]', '$' + CAST(@orderDeliveryChargeTax As varchar(16)))
			SET @body = REPLACE(@body, '[16]', '$' + CAST(@orderTips As varchar(16)))
			SET @body = REPLACE(@body, '[17]', '$' + CAST(@orderTotal As varchar(16)))
			SET @body = REPLACE(@body, '[18]', @storeName)
			SET @body = REPLACE(@body, '[20]', '$' + CAST(@storeOrderTotal As varchar(16)))
			SET @body = REPLACE(@body, '[21]', @orderInstructions)			
		END

		SET @body = ISNULL(@body, '')
		SET @subject = ISNULL(@subject, '')		
	END

	SELECT
		@fromEmail As fromEmail,
		@subject As subject,
		@body As body,
		@toEmail As toEmail,
		@displayName AS displayName	
		
END
GO