/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get category  by id
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetCategoryByID', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetCategoryByID
GO 
CREATE PROCEDURE dbo.spStore_GetCategoryByID(
	@categoryID bigint						-- 0		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	-- 0.1. get property features
	SELECT
		c.category,
		c.categoryID,
		c.isActive,
		c.storeID,
		c.dateUpdated
	FROM
		tblCategory c
	WHERE		
		( c.categoryID = @categoryID )
END
GO