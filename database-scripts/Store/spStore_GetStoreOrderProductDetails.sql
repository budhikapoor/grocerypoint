/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get store order
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_GetStoreOrderProductDetails', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetStoreOrderProductDetails
GO 
CREATE PROCEDURE dbo.spStore_GetStoreOrderProductDetails(
	@orderID bigint				
)
AS
BEGIN

	-- 1. get order details
	SELECT
		o.productID,
		o.userID,
		p.productName,
		o.price,
		o.qty,
		o.subtotal,		
		pu.productUnit,
		o.orderDetailsID
	FROM
		tblOrderDetails o			INNER JOIN
		tblProduct p				ON o.productID = p.productID LEFT JOIN
		tblProductUnit pu			ON p.productUnitID = pu.productUnitID	
	WHERE
		( o.orderID = @orderID )
END
GO