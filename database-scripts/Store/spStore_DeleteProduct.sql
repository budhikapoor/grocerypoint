/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		update store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_DeleteProduct', N'P') IS NOT NULL
	DROP PROCEDURE spStore_DeleteProduct
GO 
CREATE PROCEDURE dbo.spStore_DeleteProduct(
	@productID bigint,					-- 0	
	@ipAddress nvarchar(50),			-- 1
	@updatedUserID bigint				-- 2
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblProduct SET
				isActive = 0,		
				ipAddress = @ipAddress,
				updatedUserID = @updatedUserID,
				dateUpdated = GETUTCDATE()
			WHERE
				( productID = @productID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO