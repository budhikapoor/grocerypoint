/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		save store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_AddStore', N'P') IS NOT NULL
	DROP PROCEDURE spStore_AddStore
GO 
CREATE PROCEDURE dbo.spStore_AddStore(	
	@storeName nvarchar(300),			-- 1
	@streetAddress nvarchar(200),		-- 2
	@unit nvarchar(100),				-- 3
	@cityID bigint,						-- 4
	@provinceID bigint,					-- 5
	@logo  nvarchar(200),				-- 6
	@phone nvarchar(20),				-- 7
	@email nvarchar(100),				-- 8
	@postalCode nvarchar(20), 			-- 9
	@firstName nvarchar(100),			-- 10
	@lastName nvarchar(100),			-- 11
	@passwordHash binary(64),			-- 12
	@passwordSalt binary(128),			-- 13
	@ipAddress nvarchar(50),			-- 14	
	@storeID bigint OUTPUT				-- 15
)
AS
BEGIN
DECLARE
	@userID bigint

	SET @phone = dbo.fcFormat_PhoneNumberForSave(@phone)
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. save store
			INSERT INTO tblStore(
				storeName, streetAddress, unit, cityID, provinceID, logo, phone, email,
				postalCode, ipAddress, dateAdded, isActive, isDeleted, percentage, isText, isCall,
				textPhone, callPhone
			)
			VALUES(
				@storeName, @streetAddress, @unit, @cityID, @provinceID, @logo, @phone, @email,
				@postalCode, @ipAddress, GETUTCDATE(), 0, 0, 0, 0, 0,
				@phone, @phone
			)	
		
		SELECT @storeID = SCOPE_IDENTITY()

		-- 2. Add user
		INSERT INTO tblUser(
			userTypeID, userName, userLoginFailCount, userPasswordHash,
			userPasswordSalt, isFirstLogin, isEmailVerified, userStatusID, storeID
		)  
		VALUES(
			2, @email, 0, @passwordHash, 
			@passwordSalt, 1, 0, 1, @storeID
		) 

		SET @userID = SCOPE_IDENTITY()

		-- 3. Add user details
		INSERT INTO tblUserDetails(
			userID, userFirstName, userLastName, userEmail, phone, cityID, provinceID, dateAdded
		)
		VALUES(
			@userID, @firstName, @lastName, @email, @phone, @cityID, @provinceID, GETUTCDATE()
		)

		-- 4. update main userID
		UPDATE tblStore SET
			userMainID = @userID
		WHERE
			( storeID = @storeID )

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO