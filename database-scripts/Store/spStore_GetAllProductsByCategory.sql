/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get all categories
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllProductsByCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllProductsByCategory
GO 
CREATE PROCEDURE dbo.spStore_GetAllProductsByCategory(	
	@categoryID bigint,						-- 1
	@subCategoryID bigint = NULL			-- 2	
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	SET @subCategoryID = ISNULL(@subCategoryID, 0)

	-- 0.1. get product details
	IF @subCategoryID > 0
		SELECT
			p.categoyID,
			p.productID,
			p.productName,
			p.description,
			p.image,
			p.isGST,
			p.maxLimit,
			p.minLimit,
			p.price,
			p.productUnitID,
			p.subCategoryID,
			p.totalQuantity,
			c.category,
			sc.subCategory,
			ISNULL(pu.productUnit, '') AS productUnit,
			p.isActive,
			p.storeID,
			p.dateUpdated
		FROM
			tblProduct p		INNER JOIN
			tblCategory c		ON p.categoyID = c.categoryID LEFT JOIN
			tblSubCategory sc	ON p.subCategoryID = sc.subCategoryID LEFT JOIN
			tblProductUnit pu	ON p.productUnitID = pu.productUnitID 
		WHERE
			( p.isActive = 1 ) AND
			( p.categoyID = @categoryID ) AND
			( sc.subCategoryID = @subCategoryID )
	ELSE
		SELECT
			p.categoyID,
			p.productID,
			p.productName,
			p.description,
			p.image,
			p.isGST,
			p.maxLimit,
			p.minLimit,
			p.price,
			p.productUnitID,
			p.subCategoryID,
			p.totalQuantity,
			c.category,
			'' AS subCategory,
			ISNULL(pu.productUnit, '') AS productUnit,
			p.isActive,
			p.storeID,
			p.dateUpdated
		FROM
			tblProduct p		INNER JOIN
			tblCategory c		ON p.categoyID = c.categoryID LEFT JOIN			
			tblProductUnit pu	ON p.productUnitID = pu.productUnitID 
		WHERE
			( p.isActive = 1 ) AND
			( p.categoyID = @categoryID )			
END
GO