/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get category  by id
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetSubCategoryByID', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetSubCategoryByID
GO 
CREATE PROCEDURE dbo.spStore_GetSubCategoryByID(
	@subCategoryID bigint						-- 0		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	-- 0.1. get property features
	SELECT
		s.subCategory,
		s.subCategory,
		s.categoryID,
		s.isActive,
		s.storeID,
		s.dateUpdated
	FROM
		tblSubCategory s
	WHERE		
		( s.subCategoryID = @subCategoryID )
END
GO