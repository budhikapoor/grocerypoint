/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get product  by id
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetProductByID', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetProductByID
GO 
CREATE PROCEDURE dbo.spStore_GetProductByID(
	@productID bigint						-- 0		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	-- 0.1. get property features
	SELECT
			p.categoyID,
			p.productID,
			p.productName,
			p.description,
			p.image,
			p.isGST,
			ISNULL(p.maxLimit, 0) AS maxLimit,
			ISNULL(p.minLimit, 0) AS minLimit,
			p.price,
			p.productUnitID,
			p.subCategoryID,
			p.totalQuantity,		
			p.isActive,
			p.storeID,
			p.dateUpdated
		FROM
			tblProduct p
		WHERE
			( p.productID = @productID )		
END
GO