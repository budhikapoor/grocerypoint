/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		add store sub categories
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_AddSubCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_AddSubCategory
GO 
CREATE PROCEDURE dbo.spStore_AddSubCategory(
	@storeID bigint,					-- 0
	@categoryID bigint,					-- 1
	@subCategory nvarchar(200),			-- 1
	@addedUserID bigint					-- 2	
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. save category
			INSERT INTO tblSubCategory(
				categoryID, subCategory, storeID, addedUserID, dateAdded, isActive
			)
			VALUES(
				@categoryID, @subCategory, @storeID,  @addedUserID, GETUTCDATE(), 1
			)	

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO