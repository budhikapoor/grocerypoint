/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get product  by id
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetProductUnitByID', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetProductUnitByID
GO 
CREATE PROCEDURE dbo.spStore_GetProductUnitByID(
	@productUnitID bigint						-- 0		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	-- 0.1. get productUnit 
	SELECT			
			p.productUnitID,
			p.productUnit,
			p.isActive,
			p.storeID			
		FROM
			tblProductUnit p
		WHERE
			( p.productUnitID = @productUnitID )		
END
GO