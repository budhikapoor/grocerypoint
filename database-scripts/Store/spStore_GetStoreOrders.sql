/* 
	created by: Budhi Kapoor
	created at: April 25, 2020
	description:
		get user order
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_GetStoreOrders', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetStoreOrders
GO 
CREATE PROCEDURE dbo.spStore_GetStoreOrders(
	@storeID bigint						-- 1	
)
AS
BEGIN

	-- 1. get pre order details
	SELECT	
		o.orderID,
		CASE
			WHEN LEN(o.orderID) = 1 THEN 'GP000' + CAST(o.orderID As nvarchar(16)) 
			WHEN LEN(o.orderID) = 2 THEN 'GP00' + CAST(o.orderID As nvarchar(16)) 
			WHEN LEN(o.orderID) = 3 THEN 'GP0' + CAST(o.orderID As nvarchar(16))
			ELSE 'GP' + CAST(o.orderID As nvarchar(16))
		END As orderNumber,		
		s.storeName + ' - ' + c.city As storeName,
		o.qty,
		o.subtotal AS clientTotal,
		s.percentage,
		o.subtotal - (o.subtotal * s.percentage)/100 AS amountPaid,
		o.tax,
		o.subtotal,
		o.total,		
		CONVERT(varchar, o.dateAdded, 107) As orderDate,
		os.orderStatus,		
		CASE 
			WHEN ( LEN(o.orderPhone) = 10 OR CHARINDEX('X', o.orderPhone) > 0 ) THEN ISNULL(dbo.fcFormat_PhoneNumberGet(1, o.orderPhone), '')
			WHEN LEN(o.orderPhone) > 1 THEN ISNULL(o.orderPhone, '')
			ELSE ''
		END As orderPhone,
		ud.userFirstName + ' ' + ud.userLastName AS customerName,
		CONVERT(varchar, o.deliveryDate, 107) As deliveryDate,
		ISNULL(o.notes, '') AS orderInstructions
	FROM
		tblOrder o			INNER JOIN
		tblStore s			ON o.storeID = s.storeID INNER JOIN
		tblOrderStatus os	ON o.orderStatusID = os.orderStatusID LEFT JOIN
		tblCity c			ON s.cityID = c.cityID LEFT JOIN
		tblUser u			ON o.userID = u.userID LEFT JOIN
		tblUserDetails ud	ON o.userID = ud.userID
	WHERE
		( o.storeID = @storeID )
END
GO