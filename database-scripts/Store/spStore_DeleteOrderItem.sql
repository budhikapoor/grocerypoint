/* 
	created by: Budhi Kapoor
	created at: May 14, 2020
	description:
		delete order item
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_DeleteOrderItem', N'P') IS NOT NULL
	DROP PROCEDURE spStore_DeleteOrderItem
GO 
CREATE PROCEDURE dbo.spStore_DeleteOrderItem(
	@orderDetailsID bigint				-- 0	

)
AS
BEGIN
DECLARE
	@orderID bigint,
	@qty int,
	@tax money,
	@subtotal money,
	@total money

	-- 0.1. get order item details
	SELECT
		@orderID = orderID,
		@qty = qty,
		@subtotal = subtotal,
		@tax = tax,
		@total = total
	FROM 
		tblOrderDetails 
	WHERE 
		( orderDetailsID = @orderDetailsID )

	SET @qty = ISNULL(@qty, 0)
	SET @subtotal = ISNULL(@subtotal, 0)
	SET @tax = ISNULL(@tax, 0)
	SET @total = ISNULL(@total, 0)

	BEGIN TRY
		BEGIN TRAN
			
			-- 1. Delete Order item
			DELETE FROM 
				tblOrderDetails 
			WHERE 
				( orderDetailsID = @orderDetailsID )
			
			-- 2. UPDATE Order
			UPDATE tblOrder SET
				qty = qty - @qty,
				subtotal = subtotal - @subtotal,
				tax = tax - @tax,
				storeTotal = storeTotal - @total,
				total = total - @total
			WHERE
				( orderID = @orderID )

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO