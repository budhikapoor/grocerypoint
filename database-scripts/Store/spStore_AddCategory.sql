/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		set store categories
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_AddCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_AddCategory
GO 
CREATE PROCEDURE dbo.spStore_AddCategory(
	@storeID bigint,					-- 0
	@category nvarchar(200),			-- 1
	@addedUserID bigint					-- 2	
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. save category
			INSERT INTO tblCategory(
				category, storeID, addedUserID, dateAdded, isActive
			)
			VALUES(
				@category, @storeID,  @addedUserID, GETUTCDATE(), 1
			)	

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO