/* 
	created by: Budhi Kapoor
	created at: April 29, 2020
	description:
		get all products units
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllProductsUnitsByStore', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllProductsUnitsByStore
GO 
CREATE PROCEDURE dbo.spStore_GetAllProductsUnitsByStore(	
	@storeID bigint						-- 0	
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	
	-- 0.1. get product unit
		SELECT
			pu.productUnit,
			pu.productUnitID,
			pu.isActive
		FROM			
			tblProductUnit pu
		WHERE
			( pu.isActive = 1 ) AND			
			( pu.storeID = @storeID ) 			

END
GO