/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		update store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_UpdateProduct', N'P') IS NOT NULL
	DROP PROCEDURE spStore_UpdateProduct
GO 
CREATE PROCEDURE dbo.spStore_UpdateProduct(
	@productID bigint,					-- 0
	@productName nvarchar(300),			-- 1
	@price money,						-- 2
	@isGST bit,							-- 3
	@totalQuantity bigint,				-- 4
	@productUnitID bigint,				-- 5
	@maxLimit bigint,					-- 6
	@minLimit bigint,					-- 7
	@subCategoryID bigint = NULL,		-- 8
	@categoyID bigint, 					-- 9
	@description nvarchar(MAX),			-- 10
	@image nvarchar(300),				-- 11
	@ipAddress nvarchar(50),			-- 12
	@updatedUserID bigint				-- 13	
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblProduct SET
				productName = @productName,		
				price = @price,					
				isGST = @isGST,						
				totalQuantity = @totalQuantity,			
				productUnitID = @productUnitID,			
				maxLimit = @maxLimit,				
				minLimit = @minLimit,				
				subCategoryID = @subCategoryID,	
				categoyID = @categoyID, 				
				description = @description,		
				image = @image,
				updatedUserID = @updatedUserID,
				dateUpdated = GETUTCDATE()
			WHERE
				( productID = @productID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO