/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		update store sub categories
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_UpdateSubCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_UpdateSubCategory
GO 
CREATE PROCEDURE dbo.spStore_UpdateSubCategory(
	@subCategoryID bigint,				-- 0
	@subCategory nvarchar(200),			-- 1
	@categoryID bigint,					-- 2
	@updatedUserID bigint				-- 3	
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblSubCategory SET
				subCategory = @subCategory,
				categoryID = @categoryID,
				updatedUserID = @updatedUserID,
				dateUpdated = GETUTCDATE()
			WHERE
				( subCategoryID = @subCategoryID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO