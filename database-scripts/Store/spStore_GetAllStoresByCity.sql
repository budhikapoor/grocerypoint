/* 
	created by: Budhi Kapoor
	created at: April 21, 2020
	description:
		get all stores
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllStoresByCity', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllStoresByCity
GO 
CREATE PROCEDURE dbo.spStore_GetAllStoresByCity(	
	@cityID bigint					-- 1		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	SELECT
		s.storeID,
		s.storeName,
		s.logo,
		s.streetAddress,
		c.city,
		p.provinceAbbr As province,
		s.postalCode,
		ISNULL(s.callPhone, '') AS callPhone,
		ISNULL(s.textPhone, '') AS textPhone,
		ISNULL(s.isText, CAST(0 AS bit)) AS isText,
		ISNULL(s.isCall, CAST(0 AS bit)) AS isCall
	FROM
		tblStore s			INNER JOIN
		tblCity c			ON s.cityID = c.cityID INNER JOIN
		tblProvince p		ON s.provinceID = p.provinceID
	WHERE
		( s.isActive = 1 ) AND
		( s.cityID = @cityID )	
END
GO