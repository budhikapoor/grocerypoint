/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		update store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_UpdateOrderStatus', N'P') IS NOT NULL
	DROP PROCEDURE spStore_UpdateOrderStatus
GO 
CREATE PROCEDURE dbo.spStore_UpdateOrderStatus(
	@orderID bigint,					-- 0
	@orderStatusID bigint,				-- 1
	@userID bigint						-- 2
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblOrder SET
				orderStatusID = @orderStatusID,
				storeUpdatedUserID = @userID,
				storeUpdatedDate = GETUTCDATE()
			WHERE
				( orderID = @orderID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO