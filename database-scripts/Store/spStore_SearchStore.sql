/* 
	created by: Budhi Kapoor
	created at: April 22, 2020
	description:
		search store
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_SearchStore', N'P') IS NOT NULL
	DROP PROCEDURE spStore_SearchStore
GO 
CREATE PROCEDURE dbo.spStore_SearchStore(
	@storeID bigint,					-- 0	
	@searchPar nvarchar(100)			-- 1	
)
AS
BEGIN	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	-- 1. get all properties of User
	IF LEN(@searchPar) > 0 
		SELECT
			p.categoyID,
			p.productID,
			p.productName,
			p.description,
			p.image,
			p.isGST,
			p.maxLimit,
			p.minLimit,
			p.price,
			p.productUnitID,
			p.subCategoryID,
			p.totalQuantity,
			c.category,
			sc.subCategory,
			ISNULL(pu.productUnit, '') AS productUnit,
			p.isActive,
			p.storeID,
			p.dateUpdated
		FROM
			tblProduct p		INNER JOIN
			tblCategory c		ON p.categoyID = c.categoryID LEFT JOIN
			tblSubCategory sc	ON p.subCategoryID = sc.subCategoryID LEFT JOIN
			tblProductUnit pu	ON p.productUnitID = pu.productUnitID 
		WHERE
			( p.isActive = 1 ) AND
			( p.storeID = @storeID ) AND		
			( 
				( p.productName LIKE ('%' + @searchPar + '%')) OR
				( p.description LIKE ('%' + @searchPar + '%'))							
			)
		ORDER BY
			1 desc
	
END
GO