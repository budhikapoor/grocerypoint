/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		save store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_AddProductUnit', N'P') IS NOT NULL
	DROP PROCEDURE spStore_AddProductUnit
GO 
CREATE PROCEDURE dbo.spStore_AddProductUnit(
	@storeID bigint,					-- 0
	@productUnit nvarchar(300),			-- 1	
	@productUnitID bigint OUTPUT			-- 2
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. save product
			INSERT INTO tblProductUnit(
				productUnit, isActive, storeID
			)
			VALUES(
				@productUnit, 1, @storeID
			)	
		
		SELECT @productUnitID = SCOPE_IDENTITY()

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO