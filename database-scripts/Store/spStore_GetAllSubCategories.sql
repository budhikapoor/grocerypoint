/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get all sub categories
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllSubCategories', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllSubCategories
GO 
CREATE PROCEDURE dbo.spStore_GetAllSubCategories(
	@storeID bigint						-- 0		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	-- 0.1. get property features
	SELECT
		c.subCategory,
		c.subCategoryID,
		c.categoryID,
		c.isActive,
		c.storeID,
		c.dateUpdated
	FROM
		tblSubCategory c
	WHERE
		( c.isActive = 1 ) AND
		( c.storeID = @storeID )
END
GO