/* 
	created by: Budhi Kapoor
	created at: April 29, 2020
	description:
		delete category
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_DeleteCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_DeleteCategory
GO 
CREATE PROCEDURE dbo.spStore_DeleteCategory(
	@categoryID bigint,					-- 0	
	@ipAddress nvarchar(50),			-- 1
	@updatedUserID bigint				-- 2
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblCategory SET
				isActive = 0,				
				updatedUserID = @updatedUserID,
				dateUpdated = GETUTCDATE()
			WHERE
				( categoryID = @categoryID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO