/* 
	created by: Budhi Kapoor
	created at: April 23, 2020
	description:
		get all products
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllProductsByStoreCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllProductsByStoreCategory
GO 
CREATE PROCEDURE dbo.spStore_GetAllProductsByStoreCategory(	
	@categoryID bigint =null,				-- 1
	@storeID bigint,						-- 2
	@subCategoryID bigint = NULL			-- 3	
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	SET @subCategoryID = ISNULL(@subCategoryID, 0)

	SET @categoryID = ISNULL(@categoryID, 0)
	-- 0.1. get product details

	IF @categoryID > 0
		SELECT
			p.categoyID,
			p.productID,
			p.productName,
			p.description,
			p.image,
			p.isGST,
			ISNULL(p.maxLimit, 0) AS maxLimit,
			ISNULL(p.minLimit, 0) AS minLimit,
			p.price,
			p.productUnitID,
			p.subCategoryID,
			p.totalQuantity,
			c.category,
			sc.subCategory,
			ISNULL(pu.productUnit, '') AS productUnit,
			p.isActive,
			p.storeID,
			p.dateUpdated
		FROM
			tblProduct p		INNER JOIN
			tblCategory c		ON p.categoyID = c.categoryID LEFT JOIN
			tblSubCategory sc	ON p.subCategoryID = sc.subCategoryID LEFT JOIN
			tblProductUnit pu	ON p.productUnitID = pu.productUnitID 
		WHERE
			( p.isActive = 1 ) AND
			( p.categoyID = @categoryID ) AND
			( p.storeID = @storeID ) AND
			( c.categoryID = @categoryID )
	ELSE
		SELECT
			p.categoyID,
			p.productID,
			p.productName,
			p.description,
			p.image,
			p.isGST,
			ISNULL(p.maxLimit, 0) AS maxLimit,
			ISNULL(p.minLimit, 0) AS minLimit,
			p.price,
			p.productUnitID,
			p.subCategoryID,
			p.totalQuantity,
			c.category,
			'' AS subCategory,
			ISNULL(pu.productUnit, '') AS productUnit,
			p.isActive,
			p.storeID,
			p.dateUpdated
		FROM
			tblProduct p		INNER JOIN
			tblCategory c		ON p.categoyID = c.categoryID LEFT JOIN			
			tblProductUnit pu	ON p.productUnitID = pu.productUnitID 
		WHERE
			( p.isActive = 1 ) AND
			--( p.categoyID = @categoryID ) and 
			( p.storeID =@storeID )
END
GO