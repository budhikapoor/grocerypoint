/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		update store categories
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_UpdateCategory', N'P') IS NOT NULL
	DROP PROCEDURE spStore_UpdateCategory
GO 
CREATE PROCEDURE dbo.spStore_UpdateCategory(
	@categoryID bigint,					-- 0
	@category nvarchar(200),			-- 1
	@updatedUserID bigint				-- 2	
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblCategory SET
				category = @category,
				updatedUserID = @updatedUserID,
				dateUpdated = GETUTCDATE()
			WHERE
				( categoryID = @categoryID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO