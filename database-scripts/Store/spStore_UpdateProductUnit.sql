/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		update store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_UpdateProductUnit', N'P') IS NOT NULL
	DROP PROCEDURE spStore_UpdateProductUnit
GO 
CREATE PROCEDURE dbo.spStore_UpdateProductUnit(
	@productUnitID bigint,					-- 0
	@productUnit nvarchar(300)				-- 1
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. update category
			UPDATE tblProductUnit SET
				productUnit = @productUnit				
			WHERE
				( productUnitID = @productUnitID )			

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO