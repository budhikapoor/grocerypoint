/* 
	created by: Budhi Kapoor
	created at: April 19, 2020
	description:
		get store details
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_GetStoreDetails', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetStoreDetails
GO 
CREATE PROCEDURE dbo.spStore_GetStoreDetails(
	@storeID bigint					-- 0
)
AS
BEGIN
	-- 1. get store details
	SELECT
		storeName,
		storeID,
		c.city,
		s.isText,
		s.isCall,
		s.textPhone,
		s.callPhone,
		s.email,
		s.logo,
		s.percentage,
		s.phone,
		s.postalCode,
		s.provinceID,
		s.streetAddress,
		s.unit
	FROM
		tblStore s		INNER JOIN
		tblCity c		ON s.cityID = c.cityID
	WHERE
		( storeID = @storeID )

END
GO