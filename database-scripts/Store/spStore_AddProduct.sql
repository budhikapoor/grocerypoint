/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		save store products
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_AddProduct', N'P') IS NOT NULL
	DROP PROCEDURE spStore_AddProduct
GO 
CREATE PROCEDURE dbo.spStore_AddProduct(
	@storeID bigint,					-- 0
	@productName nvarchar(300),			-- 1
	@price money,						-- 2
	@isGST bit,							-- 3
	@totalQuantity bigint,				-- 4
	@productUnitID bigint,				-- 5
	@maxLimit bigint,					-- 6
	@minLimit bigint,					-- 7
	@subCategoryID bigint = NULL,		-- 8
	@categoyID bigint, 					-- 9
	@description nvarchar(MAX),			-- 10
	@image nvarchar(300),				-- 11
	@ipAddress nvarchar(50),			-- 12
	@addedUserID bigint,				-- 13
	@productID bigint OUTPUT			-- 14
)
AS
BEGIN	
	
	BEGIN TRY
		BEGIN TRAN

			-- 1. save product
			INSERT INTO tblProduct(
				productName, price, isGST, totalQuantity, productUnitID, maxLimit, minLimit, subCategoryID,
				categoyID, description, image, ipAddress, storeID, addedUserID, dateAdded, isActive, isDeleted
			)
			VALUES(
				@productName, @price, @isGST, @totalQuantity, @productUnitID, @maxLimit, @minLimit, @subCategoryID,
				@categoyID, @description, @image, @ipAddress, @storeID,  @addedUserID, GETUTCDATE(), 1, 0
			)	
		
		SELECT @productID = SCOPE_IDENTITY()

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO