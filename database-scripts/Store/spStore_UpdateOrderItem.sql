/* 
	created by: Budhi Kapoor
	created at: May 14, 2020
	description:
		delete order item
			
	used: 
		
*/
IF OBJECT_ID (N'dbo.spStore_UpdateOrderItem', N'P') IS NOT NULL
	DROP PROCEDURE spStore_UpdateOrderItem
GO 
CREATE PROCEDURE dbo.spStore_UpdateOrderItem(
	@orderDetailsID bigint,		-- 0
	@qty bigint,				-- 1
	@userID bigint				-- 2

)
AS
BEGIN
DECLARE
	@orderID bigint,
	@storeID bigint,
	@price money,
	@OldQty int,
	@tax money,
	@taxNew money,
	@subtotal money,
	@subtotalNew money,
	@total money,
	@totalNew money,
	@productID bigint

	SET @userID = ISNULL(@userID, 0)

	-- 0.1. get order item details
	SELECT
		@orderID = orderID,
		@storeID = storeID,
		@price = price,
		@OldQty = qty,
		@subtotal = subtotal,
		@tax = tax,
		@total = total,
		@productID = productID
	FROM 
		tblOrderDetails 
	WHERE 
		( orderDetailsID = @orderDetailsID )

	SET @OldQty = ISNULL(@OldQty, 0)
	SET @price = ISNULL(@price, 0)
	SET @subtotal = ISNULL(@subtotal, 0)
	SET @tax = ISNULL(@tax, 0)
	SET @total = ISNULL(@total, 0)
	SET @subtotalNew = @price * @qty
	SET @taxNew = @subtotalNew * 0.05
	SET @totalNew = @subtotalNew + @taxNew

	BEGIN TRY
		BEGIN TRAN
			
			-- 1. Delete Order item
			DELETE FROM tblOrderDetails WHERE orderDetailsID = @orderDetailsID

			-- 2. Update Order item			
			INSERT INTO tblOrderDetails(
				orderID, storeID, productID, price, qty, tax, subtotal, dateAdded, total, userID
			)
			SELECT @orderID, @storeID, @productID, @price, @qty, @taxNew, @subtotalNew, GETUTCDATE(), @totalNew, @userID
		
			
			-- 3. remove old data from order
			UPDATE tblOrder SET
				qty = qty - @OldQty,
				subtotal = subtotal - @subtotal,				
				tax = tax - @tax,
				storeTotal = storeTotal - @total,
				total = total - @total
			WHERE
				( orderID = @orderID )

			-- 4. add new data in order
			UPDATE tblOrder SET
				qty = qty + @qty,
				subtotal = subtotal + @subtotalNew,
				tax = tax + @taxNew,
				storeTotal = storeTotal + @totalNew,
				total = total + @totalNew
			WHERE
				( orderID = @orderID )

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		--SET @errNum = ERROR_NUMBER()
		--SET @errDesc = CAST(ERROR_LINE() As nvarchar(20)) + '|' + ERROR_PROCEDURE() + '|' + ERROR_MESSAGE()
	END CATCH	
END
GO