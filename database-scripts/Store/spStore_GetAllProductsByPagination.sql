/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get all categories
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllProductsByPagination', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllProductsByPagination
GO 
CREATE PROCEDURE dbo.spStore_GetAllProductsByPagination(	
	@categoryID bigint,						-- 1
	@subCategoryID bigint = NULL,			-- 2
	@page int,								-- 3
	@size int,								-- 4
	@sort nvarchar(50),						-- 5
	@totalrow INT  OUTPUT					-- 6
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	DECLARE 
		@offset int,
		@newsize INT,
		@offset1 int,
		@sql nvarchar(MAX)

    IF @page = 0 
    BEGIN
		SET @offset = @page
        SET @newsize = @size
	END      
    ELSE 
    BEGIN
		SET @offset = @page * @size
		SET @newsize = @size - 1
    END
    
	SET @offset1 = @offset + @newsize

    SET @sql = '
     ;WITH OrderedSet AS
    (
      SELECT  
		p.categoyID,
		p.productID,
		p.productName,
		p.description,
		p.image,
		p.isGST,
		p.maxLimit,
		p.minLimit,
		p.price,
		p.productUnitID,
		p.subCategoryID,
		p.totalQuantity,
		c.category,
		'''' AS subCategory,
		ISNULL(pu.productUnit, '''') AS productUnit,
		p.isActive,
		p.storeID,
		p.dateUpdated,
		ROW_NUMBER() OVER (ORDER BY   (SELECT NULL) ) AS [Index]
      FROM
			tblProduct p		INNER JOIN
			tblCategory c		ON p.categoyID = c.categoryID LEFT JOIN
			tblSubCategory sc	ON p.subCategoryID = sc.subCategoryID LEFT JOIN
			tblProductUnit pu	ON p.productUnitID = pu.productUnitID 
		WHERE
			( p.isActive = 1 ) AND
			( p.categoyID =  ' + CAST(@categoryID As nvarchar(16)) + '  )

    )
   SELECT * FROM OrderedSet WHERE [Index] BETWEEN  ' + CAST(@offset As nvarchar(16)) + ' AND  ' + CAST(@offset1 As nvarchar(16))+ ''
   
   -- 1. Execute query
   --PRINT @sql
   EXECUTE (@sql)

   SET @totalrow = (SELECT COUNT(*) FROM tblProduct WHERE ( isActive = 1 ) AND ( categoyID = @categoryID ))
	--PRINT @totalrow
			
END
GO