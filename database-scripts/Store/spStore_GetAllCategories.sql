/* 
	created by: Budhi Kapoor
	created at: April 16, 2020
	description:
		get all categories
			
	used:
		
*/
IF OBJECT_ID (N'dbo.spStore_GetAllCategories', N'P') IS NOT NULL
	DROP PROCEDURE spStore_GetAllCategories
GO 
CREATE PROCEDURE dbo.spStore_GetAllCategories(
	@storeID bigint						-- 0		
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	-- 0.1. get property features
	SELECT
		c.category,
		c.categoryID,
		c.isActive,
		c.storeID,
		c.dateUpdated
	FROM
		tblCategory c
	WHERE
		( c.isActive = 1 ) AND
		( c.storeID = @storeID )
END
GO