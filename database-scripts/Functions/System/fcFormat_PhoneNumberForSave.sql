/*
	created by: Budhi Kapoor
	create at: May, 22, 2019
	description:
		remove extra spaces, -, ( ) from phone number during saving phone
	
	usage
		global
*/
IF OBJECT_ID (N'dbo.fcFormat_PhoneNumberForSave', N'TF') IS NOT NULL
	DROP FUNCTION fcFormat_PhoneNumberForSave
GO
CREATE FUNCTION dbo.fcFormat_PhoneNumberForSave(
    @phone nvarchar(50)
)
RETURNS nvarchar(50)
AS
BEGIN	
	SET @phone = REPLACE(REPLACE(REPLACE(REPLACE(@phone, '-', ''), ' ', ''), '(', ''), ')', '')	
	

	RETURN @phone
END
GO