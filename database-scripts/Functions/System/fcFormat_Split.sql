/*
	created by: Budhi Kapoor
	create at: Apr, 09, 2019
	description:
		splits string and presents as table
	
	usage
		global
*/
IF OBJECT_ID (N'dbo.fcFormat_Split', N'TF') IS NOT NULL
	DROP FUNCTION fcFormat_Split
GO
CREATE FUNCTION fcFormat_Split(
    @v nvarchar(2048),
    @delimeter nchar(1)
)
RETURNS @tParts TABLE ( part nvarchar(2048) )
AS
BEGIN
DECLARE
	@start int,
    @pos int
                
    IF @v IS NULL
		RETURN 		
  
    IF SUBSTRING(@v, 1, 1 ) = @delimeter 
    BEGIN
        SET @start = 2
		INSERT INTO @tParts
        VALUES(
			NULL
		)
    END
    ELSE
		SET @start = 1
		
    WHILE 1 = 1
    BEGIN
        SET @pos = CHARINDEX( @delimeter, @v, @start )
        IF @pos = 0
			SET @pos = LEN(@v) + 1
			
        IF @pos - @start > 0                  
            INSERT INTO @tParts
            VALUES( 
				SUBSTRING( @v, @start, @pos - @start )
			)
        ELSE
			INSERT INTO @tParts
			VALUES(
				NULL
			)
			
		SET @start = @pos + 1
		
        IF @start > LEN(@v) 
           BREAK
    END
    RETURN
END
GO