/*
	created by: Budhi Kapoor
	create at: Apr, 07, 2019
	description:
		generic string formatting database level routine
		formatts phone number for user user input based on the country
		phone number always being stored as digit string in the database and stripped from all formatting upon writing data
	
	usage
		everywhere where we need to select phone number
*/
IF OBJECT_ID (N'dbo.fcFormat_PhoneNumberGet', N'FN') IS NOT NULL
	DROP FUNCTION fcFormat_PhoneNumberGet
GO
CREATE FUNCTION dbo.fcFormat_PhoneNumberGet(	
	@countryID int,
	@phoneNo nvarchar(50)
)
RETURNS nvarchar(50)
AS
BEGIN	
DECLARE
	@phoneNoReturn nvarchar(50),
	@phoneLen smallint,
	@phonePart nvarchar(10),
	@ext nvarchar(10), 
	@i int,
	@l int
	
	-- 1. normalize phone number and remove any formatting content
	SET @phoneNo = REPLACE( REPLACE(@phoneNo, ' ', ''), ' ', '')
	SET @phoneNo = REPLACE(@phoneNo, N' ', '')
	SET @phoneNo = REPLACE(@phoneNo, N'(', '')
	SET @phoneNo = REPLACE(@phoneNo, N')', '')
	SET @phoneNo = REPLACE(@phoneNo, N'-', '')
	SET @phoneNo = REPLACE(@phoneNo, N'.', '')
	
	-- 2. split extention and phone number
	SET @i = CHARINDEX('X', @phoneNo)
	SET @l = CHARINDEX('Y', @phoneNo)
	
	SET @phoneNoReturn = cast(@i As varchar)
	IF @i > 0
	BEGIN
		SET @ext = RIGHT(@phoneNo, LEN(@phoneNo) - @i)
		SET @phoneNo = LEFT(@phoneNo, @i - 1)
	END
	ELSE
		SET @ext = ''
	
	-- 3. determine length of the phone string
	SET @phoneLen = LEN(@phoneNo)
	
	-- 4. format based on the country patterns	
	IF @countryID IN (1, 2) -- canada, usa
	BEGIN
		IF @phoneLen = 10
			SET @phoneNoReturn = '(' + SUBSTRING(@phoneNo, 0, 4) + ') ' + SUBSTRING(@phoneNo, 4, 3) + '-' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF @phoneLen = 7
			SET @phoneNoReturn = '(   ) ' + SUBSTRING(@phoneNo, 0, 4) + '-' + SUBSTRING(@phoneNo, 4, 4)	
		ELSE
			SET @phoneNoReturn = N''
	END
	ELSE IF @countryID = 3			-- australia, new zealand
	BEGIN		
		IF @phoneLen = 10
			SET @phoneNoReturn = '(' + SUBSTRING(@phoneNo, 0, 3) + ') ' + SUBSTRING(@phoneNo, 3, 4) + '-' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF @phoneLen = 9
			SET @phoneNoReturn = '(0' + SUBSTRING(@phoneNo, 0, 2) + ') ' + SUBSTRING(@phoneNo, 2, 4) + '-' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF @phoneLen = 8
			SET @phoneNoReturn = '(  ) ' + SUBSTRING(@phoneNo, 0, 5) + '-' + SUBSTRING(@phoneNo, 5, 4)	
		ELSE
			SET @phoneNoReturn = ''
	END
	ELSE IF @countryID = 5			-- new zealand
	BEGIN	
		IF @phoneLen = 9
		BEGIN
			IF  SUBSTRING(@phoneNo, 0, 3) IN ('02')
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 3) + ' ' + SUBSTRING(@phoneNo, 6, 4)
			ELSE
				SET @phoneNoReturn = '' + SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 3) + ' ' + SUBSTRING(@phoneNo, 6, 4)
		END
		ELSE IF @phoneLen = 8
			SET @phoneNoReturn = '0' + SUBSTRING(@phoneNo, 0, 2) + ' ' + SUBSTRING(@phoneNo, 2, 3) + ' ' + SUBSTRING(@phoneNo, 6, 4)
		ELSE IF @phoneLen = 10
			SET @phoneNoReturn = '' + SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF @phoneLen = 11
			SET @phoneNoReturn = '' + SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 5)
		ELSE
			SET @phoneNoReturn = ''
	END
	ELSE IF @countryID = 4			-- united kingdom
	BEGIN
		IF @phoneLen = 11
		BEGIN			
			IF LEFT(@phoneNo, 3) = N'011'																-- (011x) xxx xxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 5) + ' ' + SUBSTRING(@phoneNo, 5, 3) + ' ' + SUBSTRING(@phoneNo, 8, 4) 				
			ELSE IF LEFT(@phoneNo, 2) = N'01' AND SUBSTRING(@phoneNo, 4, 1) = N'1'	-- (01x1) xxx xxxx
				SET @phoneNoReturn =  SUBSTRING(@phoneNo, 0, 5) + ' ' + SUBSTRING(@phoneNo, 5, 3) + ' ' + SUBSTRING(@phoneNo, 8, 4)  	
			ELSE IF LEFT(@phoneNo, 2) = N'01'															-- (01xxx) xxxxxx
				SET @phoneNoReturn =  SUBSTRING(@phoneNo, 0, 6) + ' ' + SUBSTRING(@phoneNo, 6, 6) 				
			ELSE IF LEFT(@phoneNo, 2) = N'02'															-- (02x) xxxx xxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 				
			ELSE IF LEFT(@phoneNo, 2) = N'03'															-- 03x xxxx xxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 
			ELSE IF LEFT(@phoneNo, 2) = N'04'															-- 03x xxxx xxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 					
			ELSE IF LEFT(@phoneNo, 2) = N'05'															-- 05x xxxx xxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 	
			ELSE IF LEFT(@phoneNo, 2) = N'06'															-- 05x xxxx xxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 				
			ELSE IF LEFT(@phoneNo, 2) = N'07'															-- 07xxx xxxxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 
			ELSE IF LEFT(@phoneNo, 2) = N'08'															-- 07xxx xxxxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 
			ELSE IF LEFT(@phoneNo, 2) = N'09'															-- 07xxx xxxxxx
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) 
			ELSE
				SET @phoneNoReturn = ''
		END
		ELSE IF @phoneLen = 10
		BEGIN
			/*
				There are two formats for 10 character phone.
					(01xxx) xxxxx
					(01xxxx) xxxx
				Since we do not idetinfy phone  numbers based on geo code so I will
				use the second format.
			*/
			IF LEFT(@phoneNo, 2) = N'01'
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 7) + ' ' + SUBSTRING(@phoneNo, 7, 3) 
			ELSE
				SET @phoneNoReturn = @phoneNo
		END

		IF @phoneNoReturn = '0'
			SET @phoneNoReturn = ''
	END
	ELSE IF @countryID = 7 -- singapore
	BEGIN
		/*
			3xxx xxxx - Voice Over IP services
			6xxx xxxx - Fixed Line services inclusive of Fixed Line Voice Over IP services like StarHub Digital Voice and SingTel mio Voice
			8xxx xxxx - Mobile phone services
			9xxx xxxx - Mobile phone services Includes Paging Services like SUNPAGE
			800 xxx xxxx - Toll-Free International services
			1800 xxx xxxx - Toll-Free line services
			1900 xxx xxxx - Premium Service
		*/
		IF LEN(@phoneNo) = 8
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 5) + ' ' + SUBSTRING(@phoneNo, 5, 4)
		ELSE IF LEN(@phoneNo) = 10 
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF LEN(@phoneNo) = 11
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 5) + ' ' + SUBSTRING(@phoneNo, 5, 3) + ' ' + SUBSTRING(@phoneNo, 8, 4)
		ELSE
			SET @phoneNoReturn = ''
	END
	ELSE IF @countryID = 69		-- germany
	BEGIN
		/*
			089/12345-123
			0151/1234567
		*/
		IF LEN(@phoneNo) = 10
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + '/' + SUBSTRING(@phoneNo, 4, 4) + '-' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF LEN(@phoneNo) = 11
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + '/' + SUBSTRING(@phoneNo, 4, 4) + '-' + SUBSTRING(@phoneNo, 8, 4)
		ELSE IF LEN(@phoneNo) = 12
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 5) + '/' + SUBSTRING(@phoneNo, 5, 4) + '-' + SUBSTRING(@phoneNo, 9, 4)
		ELSE
			SET @phoneNoReturn = @phoneNo
	END
	ELSE IF @countryID = 71		-- greece
	BEGIN
		SET @phonePart = LEFT(@phoneNo, 1)
		/*
			xxx xxxx
 			01 0xxx xxxx
			21 x xxx xxxx
		*/		
		IF LEN(@phoneNo) = 7
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 4)			
		ELSE IF LEN(@phoneNo) = 10 AND @phonePart IN ('6', '7', '8', '9') 
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 4)			
		ELSE IF LEN(@phoneNo) = 10
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 4) + ' ' + SUBSTRING(@phoneNo, 7, 4)
		ELSE
			SET @phoneNoReturn = @phoneNo
	END
	ELSE IF @countryID = 110 -- malasya
	BEGIN
		IF LEN(@phoneNo) > 8
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + '-' + SUBSTRING(@phoneNo, 3, 4) + ' ' + RIGHT(@phoneNo, LEN(@phoneNo) - 6)
		ELSE
			SET @phoneNoReturn = @phoneNo
		/*
			03-xxxx xxxx
			0x-xxx xxxx
			08x-xxx xxx			
		*/
	END
	ELSE IF @countryID IN(154, 163)	-- saudi arabia, south africa
	BEGIN
		IF @phoneLen = 10
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF @phoneLen = 9
			SET @phoneNoReturn = '0' + SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 3) + ' ' + SUBSTRING(@phoneNo, 6, 4)		
		ELSE
			SET @phoneNoReturn = @phoneNo
	END
	ELSE IF @countryID = 87		-- italy
	BEGIN		
		/*
			06 xxxxxxxx  
			0549 xxxxxx  san marino
			06 698x xxxx vatican city
 			0347 xxxxxxx  
		*/		
		
		IF LEN(@phoneNo) = 10
		BEGIN
			IF LEFT(@phoneNo, 5) = '06698'
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 4)			
			ELSE IF LEFT(@phoneNo, 2) = '06'
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 10)
			ELSE IF LEFT(@phoneNo, 4) = '0549'
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 5) + ' ' + SUBSTRING(@phoneNo, 5, 11)
			ELSE
				SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 6)	
		END
		ELSE IF LEN(@phoneNo) > 10
			SET @phoneNoReturn = LEFT(@phoneNo, LEN(@phoneNo) - 7) + ' ' + RIGHT(@phoneNo,7)			
		ELSE
			SET @phoneNoReturn = @phoneNo
	END
	ELSE IF @countryID = 185	-- united arab emirates
	BEGIN		
		/*
			(0x) xxx xxxx
			05x xxx xxxx (mobiles)
			xxx xxxx (special services)
		*/		
		IF @phoneLen = 10
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 4) + ' ' + SUBSTRING(@phoneNo, 4, 3) + ' ' + SUBSTRING(@phoneNo, 7, 4)
		ELSE IF @phoneLen = 9		
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 3) + ' ' + SUBSTRING(@phoneNo, 6, 4)	
		ELSE IF @phoneLen = 8		
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 0, 3) + ' ' + SUBSTRING(@phoneNo, 3, 3) + ' ' + SUBSTRING(@phoneNo, 6, 4)			
		ELSE	
			SET @phoneNoReturn = @phoneNo
	END
	ELSE IF @countryID IN(113, 56)	-- malta, equador)
	BEGIN		
		/*
			9xx xxxx (before 2002, from within Malta)
			99xx xxxx (after 2002, from within Malta)
			+356 99xx xxxx (after 2002, outside Malta)
		*/	
		IF @phoneLen = 16
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 4) + ' ' + SUBSTRING(@phoneNo, 5, 4) + ' ' + SUBSTRING(@phoneNo, 9, 4) + ' ' + SUBSTRING(@phoneNo, 13, 4)
		ELSE IF @phoneLen = 15
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 3) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4) + ' ' + SUBSTRING(@phoneNo, 12, 4)	
		ELSE IF @phoneLen = 14
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 2) + ' ' + SUBSTRING(@phoneNo, 3, 4) + ' ' + SUBSTRING(@phoneNo, 7, 4) + ' ' + SUBSTRING(@phoneNo, 11, 4)	
		ELSE IF @phoneLen = 13
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 1) + ' ' + SUBSTRING(@phoneNo, 2, 4) + ' ' + SUBSTRING(@phoneNo, 6, 4) + ' ' + SUBSTRING(@phoneNo, 10, 4)	
		ELSE IF @phoneLen = 12
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 4) + ' ' + SUBSTRING(@phoneNo, 5, 4) + ' ' + SUBSTRING(@phoneNo, 9, 4)	
		ELSE IF @phoneLen = 11
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 3) + ' ' + SUBSTRING(@phoneNo, 4, 4) + ' ' + SUBSTRING(@phoneNo, 8, 4)	
		ELSE IF @phoneLen = 10
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 2) + ' ' + SUBSTRING(@phoneNo, 3, 4) + ' ' + SUBSTRING(@phoneNo, 7, 4)	
		ELSE IF @phoneLen = 9		
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 1) + ' ' + SUBSTRING(@phoneNo, 2, 4) + ' ' + SUBSTRING(@phoneNo, 6, 4)	
		ELSE IF @phoneLen = 8		
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 4) + ' ' + SUBSTRING(@phoneNo, 5, 4)		
		ELSE IF @phoneLen = 7		
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 3) + ' ' + SUBSTRING(@phoneNo, 4, 4)		
		ELSE IF @phoneLen = 6		
			SET @phoneNoReturn = SUBSTRING(@phoneNo, 1, 2) + ' ' + SUBSTRING(@phoneNo, 3, 4)
		ELSE	
			SET @phoneNoReturn = @phoneNo
	END
	ELSE
		SET @phoneNoReturn = @phoneNo
	
	-- 5. set output parameter
	SET @phoneNoReturn = ISNULL(@phoneNoReturn, '')
	IF LEN(@ext) > 0 
		SET @phoneNoReturn = @phoneNoReturn + ' ext. ' + @ext

	RETURN @phoneNoReturn
END
GO