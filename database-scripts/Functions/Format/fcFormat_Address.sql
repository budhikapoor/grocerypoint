/*
	created by: Budhi Kapoor
	create at: Mar, 15, 2020
	description:
		formats date into day/time like string format
	
	usage
		global
*/
IF OBJECT_ID (N'dbo.fcFormat_Address', N'FN') IS NOT NULL
	DROP FUNCTION fcFormat_Address
GO
CREATE FUNCTION fcFormat_Address(
	@countryID int,
	@title nvarchar(100) = NULL,
	@unitNo nvarchar(20) = NULL,
	@street nvarchar(100),
	@city nvarchar(80),
	@provinceAbbr nvarchar(50),
	@country nvarchar(50),
	@zipCode nvarchar(20),
	@zipCodeExt nvarchar(20) = NULL
)
RETURNS nvarchar(500)
AS
BEGIN
DECLARE
	@address As nvarchar(500)
		
	-- 1. normalize data
	SET @unitNo = ISNULL(@unitNo, '')
	SET @zipCodeExt = ISNULL(@zipCodeExt, '')
	SET @title = ISNULL(@title, '')
	
	IF LEN(@title) > 0 
		SET @address = @title + '<br/>'
	ELSE
		SET @address = ''
		
	IF @countryID = 5					-- New Zealand
	BEGIN
		IF LEN(@unitNo) > 0
			SET @address = @address + @unitNo + '<br/>'
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
	
		IF LEN(@country) > 0
			SET @address = @address + @city + ', ' + @country + '<br/>' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
		ELSE
			SET @address = @address + @city + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
	END
	ELSE IF @countryID = 4				-- UK
	BEGIN
		IF LEN(@unitNo) > 0
			SET @address = @address + @unitNo + '<br/>'
		
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
		
		SET @address = @address + @city + ', ' + @provinceAbbr + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
	END
	ELSE IF @countryID = 3				-- Australia
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		SET @address = @address + @city + ', ' + @provinceAbbr + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 		
	END
	ELSE IF @countryID = 2				-- USA
	BEGIN		
		IF LEN(@street) > 0 
			SET @address = @address + @street 		
		
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo
			
		IF LEN(@street) > 0 OR LEN(@unitNo) > 0
			SET @address = @address + '<br/>'
		
		SET @address = @address + @city + ', ' + @provinceAbbr +  ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 
		
		IF LEN(@zipCodeExt) > 0
			SET @address = @address + '-' + @zipCodeExt
	END
	ELSE IF @countryID = 7				-- Singapore
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>'
		
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo + '<br/>'
			
		SET @address = @address + 'SINGAPORE ' + @zipCode
	END 
	ELSE IF @countryID = 163			-- South Africa
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		SET @address = @address + @city + '<br/>' + @zipCode + ' South Africa'
	END 
	ELSE IF @countryID = 251			-- Hong Kong
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		SET @address = @address + @city + '<br/> Hong Kong'	
	END
	ELSE IF @countryID IN(87)			-- Italy
	BEGIN
		SET @address = @street 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo

		IF LEN(@address) > 0 
			SET @address = @address + '<br/>'

		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)   + ' ' + @city 
	END
	ELSE IF @countryID IN(89, 95)		-- Japan, South Korea
	BEGIN
		SET @address = dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) + ', ' + @city 
			
		IF LEN(@street) > 0 
			SET @address = @address +  '<br/>' + @street 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address +  '<br/>' + @unitNo
	END
	ELSE IF @countryID = 180			-- Turkey
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)  + ' ' + @city
		
		IF LEN(@provinceAbbr) > 0 
			SET @address = @address + '/' + @provinceAbbr 
			
		SET @address = @address + '<br/>Turkey'
	END
	ELSE IF @countryID = 110			-- Malasya
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)  + ' ' + @city
		
		IF LEN(@provinceAbbr) > 0 
			SET @address = @address + '<br/>' + @provinceAbbr 	
	END
	ELSE IF @countryID IN (69, 71)		-- Germany, Greece
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo
			
		IF LEN(@address) > 0 
			SET @address = @address + '<br/>' 
			
		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)  + ' ' + @city + N'<br/>Ελληνικά'
	END
	ELSE IF @countryID IN (154)			-- Saudi Arabia
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo
			
		IF LEN(@address) > 0 
			SET @address = @address + N'<br/>' 
			
		SET @address = @address + @city + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, @zipCodeExt) + N'<br/>SAUDI ARABIA'
	END		
	ELSE IF @countryID = 170			-- Switzeland                     
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo
			
		IF LEN(@address) > 0 
			SET @address = @address + N'<br/>' 
			
		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, @zipCodeExt) + ' ' + @city + N'<br/>SWITZERLAND'
	END		
	ELSE IF @countryID = 146			-- Russia
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + ' ' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + @city + N'<br/>' + @provinceAbbr + N'<br/>' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)	
	END
	ELSE IF @countryID = 185			-- United Arab Emirates
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + ' ' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + N'<br/>' + @city + N'<br/>' + @provinceAbbr + N'<br/>United Arab Emirates'
	END
	ELSE IF @countryID = 113			-- Malta
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		SET @address = @address + @city + ', ' + @provinceAbbr + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 	
	END	
	ELSE IF @countryID = 56				-- Ecuador
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 

		IF LEN(@street) > 0 
			SET @address = @address + @zipCode + '<br/>' 

		SET @address = @address + @city + '<br/>Ecuador'	
	END 
	ELSE								-- Canada
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + '<br/>' 
			
		SET @address = @address + @city + ', ' + @provinceAbbr + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 	
	END
	
	SET @address = LTRIM(RTRIM(ISNULL(@address, '')))
	
	RETURN @address 
END
GO