/*
	created by: Budhi Kapoor
	create at: Mar, 22, 2020
	description:
		formats date into day/time like string format
	
	usage
		global
*/
IF OBJECT_ID (N'dbo.fcFormat_AddressText', N'FN') IS NOT NULL
	DROP FUNCTION fcFormat_AddressText
GO
CREATE FUNCTION dbo.fcFormat_AddressText(
	@countryID int,
	@unitNo nvarchar(20) = NULL,
	@street nvarchar(100),
	@city nvarchar(80),
	@provinceAbbr nvarchar(50),
	@country nvarchar(50),
	@zipCode nvarchar(20),
	@zipCodeExt nvarchar(20) = NULL
)
RETURNS nvarchar(500)
AS
BEGIN
DECLARE
	@address As nvarchar(500)
		
	-- 1. normalize data
	SET @unitNo = ISNULL(@unitNo, '')
	SET @zipCodeExt = ISNULL(@zipCodeExt, '')
	SET @address = ''
	
	IF @countryID = 5							-- New Zealand
		SET @address = @unitNo + ' ' + @street + ' ' + CHAR(10) + @city + ', ' + @country + CHAR(10) + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 
	ELSE IF @countryID = 4						-- UK
	BEGIN
		IF LEN(@unitNo) > 0
			SET @address = @unitNo + CHAR(10)
		ELSE
			SET @address = ''
				
		SET @address = @address + @street + ' ' + CHAR(10) + @city + ', ' + @provinceAbbr + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 		
	END
	ELSE IF @countryID = 3						-- Australia
		SET @address = @unitNo + ' ' + @street + ' ' + CHAR(10) + @city + ', ' + @provinceAbbr +  ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 
	ELSE IF @countryID = 2						-- USA
	BEGIN
		SET @address =  @street 
		
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo
		
		SET @address = @address + ' ' + CHAR(10) + @city + ', ' + @provinceAbbr +  ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) 
		
		IF LEN(@zipCodeExt) > 0
			SET @address = @address + '-' + @zipCodeExt
	END
	ELSE IF @countryID = 7						-- Singapore
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @street + CHAR(10)
		
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo + CHAR(10)
			
		SET @address = ' ' + @address + ' SINGAPORE ' + @zipCode
	END
	ELSE IF @countryID IN (69, 71)				-- Germany, Greece
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo		
			
		SET @address = @address + ' ' + @city + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
	END	
	ELSE IF @countryID = 163					-- South Africa
	BEGIN
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		IF LEN(@street) > 0 
			SET @address = @address + @street + CHAR(10)
			
		SET @address = @address + @city + ' ' + CHAR(10) + @zipCode + ' South Africa'
	END
	ELSE IF @countryID = 251					-- Hong Kong
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @street + CHAR(10)
		
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo + CHAR(10)
			
		SET @address = ' ' + @address + ' HONG KONG '
	END
	ELSE IF @countryID IN(89, 95)				-- Japan, South Korea
	BEGIN
		SET @address = dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) + ', ' + @city
			
		IF LEN(@street) > 0 
			SET @address = @address + ' ' + @street 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address +  ' ' + @unitNo
	END
	ELSE IF @countryID = 154					-- Saudi Arabia
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo
			
		IF LEN(@address) > 0 
			SET @address = @address + N' ' 
			
		SET @address = @address + @city + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, @zipCodeExt) + N' SAUDI ARABIA'
	END	
	ELSE IF @countryID = 170					-- Switzeland
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + ' ' + @unitNo	
			
		SET @address = @address + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, @zipCodeExt) + ' ' + @city + N' Switzeland'  
	END
	ELSE IF @countryID = 180					-- Turkey
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + ' ' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)  + ' ' + @city
		
		IF LEN(@provinceAbbr) > 0 
			SET @address = @address + '/' + @provinceAbbr + ' ' 			
			
		SET @address = @address + ' Turkey'
	END
	ELSE IF @countryID = 110					-- Malaysia
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + ' ' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)  + ' ' + @city
		
		IF LEN(@provinceAbbr) > 0 
			SET @address = @address + ' ' + @provinceAbbr 	
	END
	ELSE IF @countryID = 146					-- Russia
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + ' ' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + @city + N' ' + @provinceAbbr + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
	END
	ELSE IF @countryID = 87						-- Italy
	BEGIN
		SET @address = @street + ' '
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '

		SET @address = @address + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) + ' ' + @city
	END
	ELSE IF @countryID = 185					-- United Arab Emirates
	BEGIN
		IF LEN(@street) > 0 
			SET @address = @address + @street + ' ' 
			
		IF LEN(@unitNo) > 0 
			SET @address = @address + @unitNo + ' '
			
		SET @address = @address + N' ' + @city + N' ' + @provinceAbbr + N' UAE'
	END
	ELSE IF @countryID = 113					-- Malta							
		SET @address = @unitNo + ' ' + @street + ' ' + CHAR(10) + @city + ', ' + @provinceAbbr +  ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
	ELSE IF @countryID = 56						-- Ecuador	
		SET @address = @unitNo + ' ' + @street + ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL) + ' ' + @city + ', ' + @provinceAbbr +  ' Ecuador '
	ELSE										-- Canada
		SET @address = @unitNo + ' ' + @street + ' ' + @city + ', ' + @provinceAbbr +  ' ' + dbo.fcFormat_ZipCodeGet(@countryID, @zipCode, NULL)
	
	SET @address = LTRIM(RTRIM(ISNULL(@address, '')))
	
	RETURN @address 
END
GO