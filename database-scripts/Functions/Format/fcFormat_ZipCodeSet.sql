/*
	created by: Budhi Kapoor
	create at: Mar, 22, 2020
	description:
		normalizes zip code string allowing to keep only digits in the database
	
	usage
		everywhere where we need to update/add phone in the system
*/
IF OBJECT_ID (N'dbo.fcFormat_ZipCodeSet', N'FN') IS NOT NULL
	DROP FUNCTION fcFormat_ZipCodeSet
GO
CREATE FUNCTION dbo.fcFormat_ZipCodeSet(	
	@zipCode nvarchar(50)
)
RETURNS nvarchar(50)
AS
BEGIN	
DECLARE
	@zipCodeReturn nvarchar(50)
	
	-- 1. normalize phone number and remove any formatting content
	SET @zipCode = ISNULL(@zipCode, '')
	SET @zipCode = REPLACE( REPLACE(@zipCode, ' ', ''), ' ', '')
	SET @zipCode = REPLACE(@zipCode, N' ', '')
	SET @zipCode = REPLACE(@zipCode, N'(', '')
	SET @zipCode = REPLACE(@zipCode, N')', '')
	SET @zipCode = REPLACE(@zipCode, N'-', '')
	SET @zipCode = REPLACE(@zipCode, N'.', '')
	SET @zipCode = REPLACE(@zipCode, N'_', '')
		
	SET @zipCodeReturn = UPPER(ISNULL(@zipCode, ''))
	
	RETURN @zipCodeReturn
END
GO