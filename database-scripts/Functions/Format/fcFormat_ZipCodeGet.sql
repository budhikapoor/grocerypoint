/*
	created by: Budhi Kapoor
	created at: Mar 22, 2020
	description:
		formats zip code foir the user output
		
*/
IF OBJECT_ID (N'dbo.fcFormat_ZipCodeGet', N'FN') IS NOT NULL
	DROP FUNCTION fcFormat_ZipCodeGet
GO
CREATE FUNCTION dbo.fcFormat_ZipCodeGet(
	@countryID int,
	@zip nvarchar(10),
	@zipExt nvarchar(10)
) 
RETURNS nvarchar(30)
AS
BEGIN
DECLARE 		
	@zipOutput	 nvarchar(10)
		
	SET @zip = dbo.fcFormat_ZipCodeSet(@zip)
		
	IF @countryID = 1			-- canada
	BEGIN
		IF LEN(@zip) = 6 
			SET @zipOutput = LEFT(@zip, 3) + ' ' + RIGHT(@zip, 3)
		ELSE
			SET @zipOutput = LEFT(@zip, 3) -- fsa only				
	END
	ELSE IF @countryID IN (2, 154)	-- usa, saudi arabia
	BEGIN
		IF LEN(@zipExt) > 0 
			SET @zipOutput = @zip + '-' + @zipExt
		ELSE
			SET @zipOutput = @zip
	END
	ELSE IF @countryID = 4		-- united kingdom	 "A9 9AA", "A99 9AA", "AA9 9AA", "AA99 9AA", "A9A 9AA", "AA9A 9AA", 
	BEGIN
		IF LEN(@zip) = 5														-- "A9 9AA"
			SET @zipOutput = LEFT(@zip, 2 ) + ' ' + RIGHT(@zip, 3)
		ELSE IF LEN(@zip) = 6													-- "A99 9AA", "AA9 9AA", "A9A 9AA"
			SET @zipOutput = LEFT(@zip, 3) + ' ' + RIGHT(@zip, 3)
		ELSE IF LEN(@zip) = 7													-- "AA99 9AA", "AA9A 9AA"
			SET @zipOutput = LEFT(@zip, 4) + ' ' + RIGHT(@zip, 3)
		ELSE
			SET @zipOutput = @zip	
	END
	ELSE IF @countryID = 95		-- south korea
	BEGIN
		IF LEN(@zip) = 6 
			SET @zipOutput = LEFT(@zip, 3) + '-' + RIGHT(@zip, 3)
		ELSE
			SET @zipOutput = LEFT(@zip, 3) -- district only				
	END
	ELSE IF @countryID = 89		-- japan
	BEGIN
		IF LEN(@zip) = 7 
			SET @zipOutput = LEFT(@zip, 3) + '-' + RIGHT(@zip, 4)
		ELSE
			SET @zipOutput = LEFT(@zip, 3) -- district only				
	END
	ELSE IF @countryID = 71		-- greece
	BEGIN
		IF LEN(@zip) = 5
			SET @zipOutput = LEFT(@zip, 3) + ' ' + RIGHT(@zip, 2)
		ELSE
			SET @zipOutput = @zip
	END
	ELSE IF @countryID = 113	-- malta
	BEGIN
		IF LEN(@zip) = 7
			SET @zipOutput = LEFT(@zip, 3) + ' ' + RIGHT(@zip, 4)
		ELSE
			SET @zipOutput = LEFT(@zip, 3) -- fsa only	
	END
	ELSE
		SET @zipOutput = @zip	
				
	SET @zipOutput = UPPER(ISNULL(@zipOutput, ''))
	
	RETURN @zipOutput
END
GO
