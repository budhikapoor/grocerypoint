﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Common
{
    public static class MySession
    {
        public static int Id { get; set; }
        public static int UserTypeID { get; set; }
        public static string profilePhoto { get; set; }
        public static string facebook { get; set; }
        public static string linkedin { get; set; }
        public static string phoneShow { get; set; }
        public static int i { get; set; }
        public static string ext { get; set; }
        public static int userGetID { get; set; }

        public static string Email { get; set; }
    
        public static string FirstName { get; set; }

        public static string LastName { get; set; }
        public static string Password { get; set; }
        
        //public string Telephone { get; set; }
        //public int CityId { get; set; }
        //public int provinceID { get; set; }
        //public byte[] UserPasswordHash { get; set; }
        //public byte[] UserPasswordSalt { get; set; }
        //[Required]
        public static string UserName { get; set; }
        public static int UserId { get; set; }
    }
}
