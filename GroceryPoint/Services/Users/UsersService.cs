﻿using GroceryPoint.Dtos.Users;
using GroceryPoint.IRepos.Users;
using GroceryPoint.IServices.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Security.Cryptography;

namespace GroceryPoint.Services.Users
{
    public class UsersService: IUsersService
    {
        IUsersRepository _usersRepository;

        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public Int64 UpdateDefaultStore(Int64 userID, Int64 storeID)
        {
           return  _usersRepository.UpdateDefaultStore(userID, storeID);
        }

        public Int64 AddAddress(AddressDto address)
        {
            Int64 addressID = 0;
            try
            {
                addressID = _usersRepository.AddAddress(address);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return addressID;
        }

        public IEnumerable<AddressDetailsDto> GetUserAddresses(Int64 userID, Int64 addressID)
        {
            return _usersRepository.GetUserAddresses(userID, addressID);
        }

        public UserDto GetUserProfile(Int64 userID)
        {
            return _usersRepository.GetUserProfile(userID);
        }

        public Int64 UpdateUserProfile(UserDto user)
        {
            Int64 userID = 0;
            try
            {
                userID = _usersRepository.UpdateUserProfile(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return userID;
        }

        public Int64 ChangePassword(Int64 userID, string password, string ipAddress)
        {           
            try
            {
                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                userID = _usersRepository.ChangePassword(userID, passwordHash, passwordSalt, ipAddress);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return userID;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        public string VerifyEmail(Int64 userID, string email)
        {
            string message = "";
            try
            {
                message = _usersRepository.VerifyEmail(userID, email);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return message;
        }
    }
}
