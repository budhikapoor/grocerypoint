﻿using GroceryPoint.IRepos.Cart;
using GroceryPoint.IServices.Cart;
using GroceryPoint.Dtos.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
namespace GroceryPoint.Services.Cart
{
    public class CartService: ICartService
    {
        ICartRepository _cartRepository;

        public CartService(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;
        }

        public CartSummaryDto AddItemsToCart(CartDto cart)
        {
            CartSummaryDto cartSummary = null;
            try
            {
                cartSummary = _cartRepository.AddItemsToCart(cart);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return cartSummary;
        }

        public CartSummaryDto GetCartTotal(Int64 storeID, Int64 userID)
        {
            return _cartRepository.GetCartTotal(storeID, userID);
        }

        public IEnumerable<CartDetailsDto>  GetCartDetails(Int64 storeID, Int64 userID)
        {
            return _cartRepository.GetCartDetails(storeID, userID);
        }

        public string DeleteCart(Int64 storeID, Int64 userID)
        {
            return _cartRepository.DeleteCart(storeID, userID);
        }

        public string DeleteCartItem(Int64 cartID)
        {
            return _cartRepository.DeleteCartItem(cartID);
        }
    }
}
