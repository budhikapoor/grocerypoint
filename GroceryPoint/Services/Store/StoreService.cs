﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;
using GroceryPoint.IServices.Store;
using GroceryPoint.IRepos.Store;
using System.Security.Cryptography;
using System.Text;

namespace GroceryPoint.Services.Store
{
    public class StoreService: IStoreService
    {
        IStoreRepository _storeRepository;

        public StoreService(IStoreRepository storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public IEnumerable<StoreSummaryDto> GetAllStoresByCity(Int64 cityID)
        {
            return _storeRepository.GetAllStoresByCity(cityID);
        }

        public IEnumerable<ProductDto> SearchStore(Int64 storeID, string searchPar)
        {
            return _storeRepository.SearchStore(storeID, searchPar);
        }

        public IEnumerable<StoreOrderDto> GetAllStoreOrders(Int64 storeID)
        {
            return _storeRepository.GetAllStoreOrders(storeID);
        }

        public StoreOrderDetailsDto GetStoreOrderDetails(Int64 orderID)
        {
            return _storeRepository.GetStoreOrderDetails(orderID);
        }

        public Int64 UpdateOrderStatus(Int64 orderID, Int64 orderStatusID, Int64 userID)
        {
            try
            {
                orderID = _storeRepository.UpdateOrderStatus(orderID, orderStatusID, userID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return orderID;
        }

        public Int64 AddStore(StoreDto store, string password)
        {
            Int64 storeID = 0;
            try
            {
                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                storeID = _storeRepository.AddStore(store, passwordHash, passwordSalt);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return storeID;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        public StoreDto GetStoreDetails(Int64 storeID)
        {
            return _storeRepository.GetStoreDetails(storeID);
        }

        public string DeleteOrderItem(Int64 orderDetailsID)
        {
            return _storeRepository.DeleteOrderItem(orderDetailsID);
        }

        public string UpdateOrderItem(Int64 orderDetailsID, Int64 qty, Int64 userID)
        {
            return _storeRepository.UpdateOrderItem(orderDetailsID, qty, userID);
        }

        public StoreOrderDetailsDto GetStoreOrderBasicData(Int64 orderID)
        {
            return _storeRepository.GetStoreOrderBasicData(orderID);
        }
    }
}
