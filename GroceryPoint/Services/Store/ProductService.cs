﻿using GroceryPoint.IRepos.Store;
using GroceryPoint.IServices.Store;
using GroceryPoint.Dtos.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace GroceryPoint.Services.Store
{
    public class ProductService: IProductService
    {
        IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Int64 AddProduct(ProductDto product)
        {
            Int64 productID = 0;
            try
            {
                productID = _productRepository.AddProduct(product);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productID;
        }

        public IEnumerable<ProductDto> GetStoreProducts(Int64 categoryID, Int64 storeID)
        {
            return _productRepository.GetStoreProducts(categoryID, storeID);
        }

        public ProductDto GetProductDetails(Int64 productID)
        {
            return _productRepository.GetProductDetails(productID);
        }

        public Int64 UpdateProduct(ProductDto product)
        {
            Int64 productID = 0;
            try
            {
                productID = _productRepository.UpdateProduct(product);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productID;
        }

        public Int64 DeleteProduct(Int64 productID, string ipAddress, Int64 updatedUserID)
        {            
            try
            {
                productID = _productRepository.DeleteProduct(productID, ipAddress, updatedUserID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productID;
        }

        public Int64 DeleteCategory(Int64 categoryID, string ipAddress, Int64 updatedUserID)
        {
            try
            {
                categoryID = _productRepository.DeleteCategory(categoryID, ipAddress, updatedUserID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return categoryID;
        }

        public Int64 AddProductUnit(ProductUnitDto productUnit)
        {
            Int64 productUnitID = 0;
            try
            {
                productUnitID = _productRepository.AddProductUnit(productUnit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productUnitID;
        }

        public IEnumerable<ProductUnitDto> GetStoreProductUnits( Int64 storeID)
        {
            return _productRepository.GetStoreProductUnits(storeID);
        }

        public ProductUnitDto GetProductUnitDetails(Int64 productUnitID)
        {
            return _productRepository.GetProductUnitDetails(productUnitID);
        }

        public Int64 UpdateProductUnit(ProductUnitDto productUnit)
        {
            Int64 productUnitID = 0;
            try
            {
                productUnitID = _productRepository.UpdateProductUnit(productUnit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productUnitID;
        }

        public Int64 DeleteProductUnit(Int64 productUnitID)
        {
            try
            {
                productUnitID = _productRepository.DeleteProductUnit(productUnitID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return productUnitID;
        }
    }
}
