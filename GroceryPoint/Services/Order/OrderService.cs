﻿using GroceryPoint.IRepos.Order;
using GroceryPoint.IServices.Order;
using GroceryPoint.Dtos.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace GroceryPoint.Services.Order
{
    public class OrderService: IOrderService
    {
        IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Int64 AddOrder(OrderDto order)
        {
            Int64 orderID = 0;
            try
            {
                orderID = _orderRepository.AddOrder(order);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return orderID;
        }

        public PreOrderDetailsDto GetPreOrderDetails(Int64 storeID, Int64 userID, Int64 addressID)
        {
            return _orderRepository.GetPreOrderDetails(storeID, userID, addressID);
        }

        public IEnumerable<object> GetAllDeliveryDates(DateTime todayDateTime)
        {
            return _orderRepository.GetAllDeliveryDates(todayDateTime);
        }

        public IEnumerable<object> GetAllDeliveryTimes(DateTime selectedDate)
        {
            return _orderRepository.GetAllDeliveryTimes(selectedDate);
        }

        public IEnumerable<UserOrdersDto> GetUserOrders(Int64 userID)
        {
            return _orderRepository.GetUserOrders(userID);
        }

        public UserOrderDetails GetUserOrderDetails(Int64 orderID)
        {
            return _orderRepository.GetUserOrderDetails(orderID);
        }
    }
}
