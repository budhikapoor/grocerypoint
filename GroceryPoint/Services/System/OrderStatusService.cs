﻿using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using GroceryPoint.IServices.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Services.System
{
    public class OrderStatusService: IOrderStatusService
    {
        IOrderStatusRepository _orderstatusRepository;
        public OrderStatusService(IOrderStatusRepository orderstatusRepository)
        {
            _orderstatusRepository = orderstatusRepository;
        }

        public IEnumerable<object> GetAllOrderStatus()
        {
            return _orderstatusRepository.GetAllOrderStatus();
        }
    }
}
