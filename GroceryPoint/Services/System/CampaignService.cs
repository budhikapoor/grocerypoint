﻿using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using GroceryPoint.IServices.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Services.System
{
    public class CampaignService: ICampaignService
    {
        ICampaignRepository _campaignRepository;

        public CampaignService(ICampaignRepository campaignRepository)
        {
            _campaignRepository = campaignRepository;
        }

        public IEnumerable<object> GetAllCampaign()
        {
            return _campaignRepository.GetAllCampaign();
        }
    }
}
