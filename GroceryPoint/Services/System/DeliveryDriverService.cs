﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using GroceryPoint.IServices.System;
using GroceryPoint.IRepos.System;
using System.Security.Cryptography;
using System.Text;

namespace GroceryPoint.Services.System
{
    public class DeliveryDriverService: IDeliveryDriverService
    {
        IDeliveryDriverRepository _deliveryDriverRepository;

        public DeliveryDriverService(IDeliveryDriverRepository deliveryDriverRepository)
        {
            _deliveryDriverRepository = deliveryDriverRepository;
        }

        public Int64 AddDeliveryDriver(DeliveryDriverDto deliveryDriver)
        {
            Int64 deliveryDriverID = 0;
            try
            {
                deliveryDriverID = _deliveryDriverRepository.AddDeliveryDriver(deliveryDriver);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return deliveryDriverID;
        }
    }
}
