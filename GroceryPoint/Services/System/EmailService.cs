﻿using GroceryPoint.IRepos.System;
using GroceryPoint.IServices.System;
using GroceryPoint.Dtos.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace GroceryPoint.Services.System
{
    public class EmailService:IEmailService
    {
        IEmailRepository _emailRepository;

        public EmailService(IEmailRepository emailRepository)
        {
            _emailRepository = emailRepository;
        }

        public EmailDto GetEmailDetails(Int64 emailTemplateID, Int64 userID, Int64 storeID, Int64 orderID, Int64 userTypeID)
        {
            return _emailRepository.GetEmailDetails(emailTemplateID, userID, storeID, orderID, userTypeID);
        }

    }
}
