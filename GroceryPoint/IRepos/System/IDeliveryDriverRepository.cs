﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;

namespace GroceryPoint.IRepos.System
{
    public interface IDeliveryDriverRepository
    {
        Int64 AddDeliveryDriver(DeliveryDriverDto deliveryDriver);
    }
}
