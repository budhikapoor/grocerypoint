﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.IRepos.System
{
    public interface IOrderStatusRepository
    {
        IEnumerable<object> GetAllOrderStatus();
    }
}
