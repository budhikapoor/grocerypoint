﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Order;

namespace GroceryPoint.IRepos.Order
{
    public interface IOrderRepository
    {
        Int64 AddOrder(OrderDto order);
        PreOrderDetailsDto GetPreOrderDetails(Int64 storeID, Int64 userID, Int64 addressID);
        IEnumerable<object> GetAllDeliveryDates(DateTime todayDateTime);
        IEnumerable<object> GetAllDeliveryTimes(DateTime selectedDate);
        IEnumerable<UserOrdersDto> GetUserOrders(Int64 userID);

        UserOrderDetails GetUserOrderDetails(Int64 orderID);
    }
}
