﻿using GroceryPoint.IRepos.Store;
using GroceryPoint.Dtos.Store;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace GroceryPoint.IRepos.Store
{
    public interface IProductRepository
    {
        Int64 AddProduct(ProductDto product);
        IEnumerable<ProductDto> GetStoreProducts(Int64 categoryID, Int64 storeID);

        ProductDto GetProductDetails(Int64 productID);
        Int64 UpdateProduct(ProductDto product);

        Int64 DeleteProduct(Int64 productID, string ipAddress, Int64 updatedUserID);

        Int64 DeleteCategory(Int64 categoryID, string ipAddress, Int64 updatedUserID);

        Int64 AddProductUnit(ProductUnitDto productUnit);

        IEnumerable<ProductUnitDto> GetStoreProductUnits(Int64 storeID);

        ProductUnitDto GetProductUnitDetails(Int64 productUnitID);

        Int64 UpdateProductUnit(ProductUnitDto productUnit);

        Int64 DeleteProductUnit(Int64 productUnitID);
        
    }
}
