﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Dtos;

namespace GroceryPoint.IRepos.Store
{
    public interface IStoreRepository
    {
        IEnumerable<StoreSummaryDto> GetAllStoresByCity(Int64 cityID);

        IEnumerable<ProductDto> SearchStore(Int64 storeID, string searchPar);

        IEnumerable<StoreOrderDto> GetAllStoreOrders (Int64 storeID);

        StoreOrderDetailsDto GetStoreOrderDetails(Int64 orderID);

        Int64 UpdateOrderStatus(Int64 orderID, Int64 orderStatusID, Int64 userID);

        Int64 AddStore(StoreDto store, byte[] UserPasswordHash, byte[] UserPasswordSalt);
        StoreDto GetStoreDetails(Int64 storeID);

        string DeleteOrderItem(Int64 orderDetailsID);
        string UpdateOrderItem(Int64 orderDetailsID, Int64 qty, Int64 userID);

        StoreOrderDetailsDto GetStoreOrderBasicData(Int64 orderID);

    }
}
