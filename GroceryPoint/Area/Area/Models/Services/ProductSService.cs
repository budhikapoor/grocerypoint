﻿using GroceryPoint.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GroceryPoint.Area.Area.Models.Services
{
    public interface IProductsService
    {
        List<Product> GetProducts(int storeId, int categoryid);
    }
    public class ProductSService : IProductsService
    {
        private readonly IConfiguration _configuration;
        public ProductSService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        private SqlConnection con;
        private void connection()
        {
            string connectionString = _configuration.GetConnectionString("GroceryPointDBConnectionStrings");
            con = new SqlConnection(connectionString);
        }
   

        public List<Product> GetProducts(int storeId, int categoryid)
        {
            connection();
            List<Product> Productlist = new List<Product>();

            SqlCommand cmd = new SqlCommand("spStore_GetAllProductsByStoreCategory", con);
            //SqlParameter categoryID = new SqlParameter();
            //categoryID.ParameterName = "@categoryID";
            //categoryID.Value = id;
            //categoryID.SqlDbType = SqlDbType.Int;
            //SqlParameter storeID = new SqlParameter();
            //categoryID.ParameterName = "@storeID";
            //categoryID.Value = 1;
            //categoryID.SqlDbType = SqlDbType.Int;
            //cmd.Parameters.Add(storeID);
            cmd.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeId;
            cmd.Parameters.Add("@categoryID", SqlDbType.BigInt).Value = categoryid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Productlist.Add(
                    new Product
                    {
                        productID = Convert.ToInt32(dr["productID"]),
                        productName = Convert.ToString(dr["productName"]),
                        image = Convert.ToString(dr["image"]),
                        price = Convert.ToDouble(dr["price"]),
                        storeID = Convert.ToInt32(dr["storeID"]),
                        categoyID = Convert.ToInt32(dr["categoyID"]),
                        isActive = Convert.ToBoolean(dr["isActive"])
                    });
            }

            return Productlist;
        }
    }
}

