﻿using GroceryPoint.Dtos.Campaign;
using GroceryPoint.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GroceryPoint.Area.Area.Models.Services
{
    public interface ICategoryService
    {
        bool AddCategory(Category categoryobj);
        bool UpdateCategory(Category categoryobj);
        List<Category> GetCategory(int storeid);
        List<Province> GetprovinceList();
        List<City> GetcityList();
        List<CampaignDto> GetCampaignList();
        List<City> CityByProvinceId(int id);
    }

    public class CategoryService : ICategoryService
    {
        private readonly IConfiguration _configuration;
        private SqlConnection _connection = new SqlConnection();

        public CategoryService(IConfiguration configuration )
        {
            _configuration = configuration;
           
        }
     
    private SqlConnection con;
        private void connection()
        {
            string connectionString = _configuration.GetConnectionString("GroceryPointDBConnectionStrings");
            con = new SqlConnection(connectionString);
        }
        public bool AddCategory(Category categoryobj)
        {
            categoryobj.dateAdded = DateTime.Now;
            connection();
            SqlCommand cmd = new SqlCommand("spStore_AddCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("category", categoryobj.categoryName);
            cmd.Parameters.AddWithValue("storeID", categoryobj.storeID);
            cmd.Parameters.AddWithValue("addedUserID", 1);
        
            con.Open();
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                con.Close();
                return (true);
            }
            else
            {
                con.Close();
                return (false);

            }
            
        }

        public List<Category> GetCategory(int storeid)
        {
            connection();
            List<Category> Categorylist = new List<Category>();

            SqlCommand cmd = new SqlCommand("spStore_GetAllCategories", con);
            SqlParameter parameterActive = new SqlParameter();

            SqlParameter parameterstoreID = new SqlParameter();
            parameterstoreID.ParameterName = "@storeID";
            parameterstoreID.Value = storeid;
            parameterstoreID.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(parameterstoreID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Categorylist.Add(
                    new Category
                    {
                        categoryId = Convert.ToInt32(dr["categoryId"]),
                        storeID = Convert.ToInt32(dr["storeID"]),
                        categoryName = Convert.ToString(dr["category"]),
                        isActive = Convert.ToBoolean(dr["isActive"])
                    });
            }

            return Categorylist;
        }

        public List<Province> GetprovinceList()
        {
            connection();
            List<Province> Provincelist = new List<Province>();

            SqlCommand cmd = new SqlCommand("spSystem_GetAllProvince", con);
            SqlParameter parameterActive = new SqlParameter();
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Provincelist.Add(
                    new Province
                    {
                        provinceID = Convert.ToInt32(dr["provinceID"]),
                        provinceAbbr = Convert.ToString(dr["province"]),
                    });
            }

            return Provincelist;
        }

        public List<City> CityByProvinceId(int id)
        {
            connection();
            List<City> Statelist = new List<City>();

            SqlCommand cmd = new SqlCommand("spSystem_GetCityByProvince", con);
            SqlParameter parameterActive = new SqlParameter();

            SqlParameter provinceID = new SqlParameter();
            provinceID.ParameterName = "@provinceID";
            provinceID.Value = id;
            provinceID.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(provinceID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Statelist.Add(
                    new City
                    {
                        CityId = Convert.ToInt32(dr["cityID"]),
                        CityName = Convert.ToString(dr["city"]),
                    });
            }

            return Statelist;
        }

        public List<City> GetcityList()
        {
            connection();
            List<City> Citylist = new List<City>();

            SqlCommand cmd = new SqlCommand("spCity_GetCities", con);
            SqlParameter parameterActive = new SqlParameter();

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Citylist.Add(
                    new City
                    {
                        CityId = Convert.ToInt32(dr["cityID"]),
                        CityName = Convert.ToString(dr["city"]),
                    });
            }

            return Citylist;
        }

        public List<CampaignDto> GetCampaignList()
        {
            connection();
            List<CampaignDto> CampaignDtolist = new List<CampaignDto>();

            SqlCommand cmd = new SqlCommand("spSystem_GetCampaign", con);
            SqlParameter parameterActive = new SqlParameter();

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                CampaignDtolist.Add(
                    new CampaignDto
                    {
                        campaignID = Convert.ToInt32(dr["campaignID"]),
                        campaignName = Convert.ToString(dr["campaign"]),
                    });
            }

            return CampaignDtolist;
        }

        public bool UpdateCategory(Category categoryobj)
        {
            SqlConnection cons;

            string connectionStrings = _configuration.GetConnectionString("GroceryPointDBConnectionStrings");
            cons = new SqlConnection(connectionStrings);

            using (var command = new SqlCommand())
            {
                if (categoryobj.addedUserID == 0)
                {
                    categoryobj.addedUserID = 1;
                }
                
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_UpdateCategory";
                command.Parameters.Add("@categoryId", SqlDbType.BigInt).Value = categoryobj.categoryId;
                command.Parameters.Add("@category", SqlDbType.NVarChar).Value = categoryobj.categoryName;
                command.Parameters.Add("@updatedUserID", SqlDbType.BigInt).Value = categoryobj.addedUserID;
             
              
                command.Connection = cons;
                cons.Open();
                try
                {
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    cons.Close();
                }

            }
            return true;
        }
    }
}
