﻿
using GroceryPoint.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Areas.Area.Models.Services
{
    public interface IUserService
    {
        UserInfo Authenticate(string username, string password);
        int AddUser(UserInfo userInfo);
        //int userData(UserInfo userInfo);


    }
    public class UserService : IUserService
    {
        private readonly IConfiguration _configuration;


        public UserService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        private SqlConnection con;
        public UserInfo Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = GetUser(username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.UserPasswordHash, user.UserPasswordSalt))
                return null;

            // authentication successful
            return user;
        }



        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
        public UserInfo GetUser(string username)
        {
            UserInfo user = null;
            SqlConnection cons;
            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_GetUserByUserName";
                command.Parameters.Add("@userName", SqlDbType.VarChar).Value = username;
                
                string connectionStrings =_configuration.GetConnectionString("GroceryPointDBConnectionStrings");
                cons = new SqlConnection(connectionStrings);
                command.Connection =cons;
                cons.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            user = new UserInfo
                            {
                                UserName = (string)reader["userName"],
                                UserId = (int)reader["userId"],
                                UserPasswordHash = !Convert.IsDBNull(reader["userPasswordHash"]) ? (byte[])reader["userPasswordHash"] : new byte[0],
                                UserPasswordSalt = !Convert.IsDBNull(reader["userPasswordSalt"]) ? (byte[])reader["userPasswordSalt"] : new byte[0],
                                FirstName = (string)reader["userFirstName"],
                                UserTypeID = (int)reader["userTypeID"],
                                profilePhoto = (string)reader["profilePhoto"],
                                CityId = (int)reader["cityID"],
                                //CityName = (string)reader["cityName"],
                                storeId = (Int64)reader["storeID"],
                                StoreName = (string)reader["storeName"],
                                CityName = (string)reader["city"],                               
                                                       
                            };
                        }
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    cons.Close();
                }
            }

            return user;
        }
        private void connection()
        {
            string connectionString = _configuration.GetConnectionString("GroceryPointDBConnectionStrings");
            con = new SqlConnection(connectionString);
        }
        public int AddUser(UserInfo userInfo )
        {
            if (string.IsNullOrWhiteSpace(userInfo.Password))
                throw new Exception("Password is required");

            if (GetUser(userInfo.UserName) != null)
                throw new Exception("Email \"" + userInfo.UserName + "\" is already taken");

            CreatePasswordHash(userInfo.Password, out byte[] passwordHash, out byte[] passwordSalt);

            userInfo.UserPasswordHash = passwordHash;
            userInfo.UserPasswordSalt = passwordSalt;

            return AddUserInfo(userInfo);
        }
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        public int AddUserInfo(UserInfo user)
        {
            int UserId=0;
            SqlConnection sqlConnections;
            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_SetUser";
                command.Parameters.Add("@userFirstName", SqlDbType.VarChar).Value = user.FirstName;
                command.Parameters.Add("@userLastName", SqlDbType.VarChar).Value = user.LastName;
                command.Parameters.Add("@userTypeID", SqlDbType.TinyInt).Value = 3;

                command.Parameters.Add("@userName", SqlDbType.VarChar).Value = user.UserName;
                command.Parameters.Add("@userPasswordHash", SqlDbType.Binary).Value = user.UserPasswordHash;
                command.Parameters.Add("@userPasswordSalt", SqlDbType.Binary).Value = user.UserPasswordSalt;              
                command.Parameters.Add("@hearByID", SqlDbType.Int).Value = 1;
                command.Parameters.Add("@userPhone", SqlDbType.VarChar).Value = user.Telephone;
                command.Parameters.Add("@cityID", SqlDbType.BigInt).Value = user.CityId;
                //command.Parameters.Add("@getUserID", SqlDbType.BigInt).Value =0;

                command.Parameters.Add("@userID", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                #region connection
                string connectionStrings = _configuration.GetConnectionString("GroceryPointDBConnectionStrings");

                #endregion

                sqlConnections = new SqlConnection(connectionStrings);
                command.Connection = sqlConnections;
                sqlConnections.Open();
                try
                {
                    command.ExecuteNonQuery();
                    UserId = Convert.ToInt32(command.Parameters["@userID"].Value);
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    sqlConnections.Close();
                }

            }

            return UserId;
        }
    }
}

