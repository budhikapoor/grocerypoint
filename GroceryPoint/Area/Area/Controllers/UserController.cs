﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Areas.Area.Models.Services;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace GroceryPoint.Areas.Area.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        //private IMapper _mapper;

        public UserController(
            IUserService userService)
        {
            _userService = userService;
            //_mapper = mapper;
        }
        [Route("login")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult Authenticate(string username, string password)
        {
            UserInfo user = _userService.Authenticate(username,password);
            return Ok(user);
        }
        [Route("addUser")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult AddUser([FromBody] UserInfo userInfoObj)
        {

            int user = _userService.AddUser(userInfoObj);
            return Ok(user);
        }
         
    }
}