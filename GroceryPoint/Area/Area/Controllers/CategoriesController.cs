﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Area.Area.Models.Services;
using GroceryPoint.Areas.Area.Controllers;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ApiControllerAttribute = GroceryPoint.Areas.Area.Controllers.ApiControllerAttribute;

namespace GroceryPoint.Area.Area.Controllers
{
    //[Produces("application/json")]
    [Route("api")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        [Route("addCategory")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult AddCategory([FromBody] Category categoryobj)
        {
           
            bool user = _categoryService.AddCategory(categoryobj);
            return Ok(user);
        }
        [HttpPost("updateCategory")]
        public IActionResult UpdateCategory([FromBody] Category categoryobj)
        {

            bool user = _categoryService.UpdateCategory(categoryobj);
            return Ok(user);
        }












        [HttpGet]
        [Route("CategoryList")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult Category(int storeid)
        {
            var user = _categoryService.GetCategory(storeid);
            return Ok(user);
        }




        [HttpGet]
        [Route("provinceList")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult ProvinceList()
        {
            var province = _categoryService.GetprovinceList();
            return Ok(province);
        } 
        [HttpGet]
        [Route("CityByProvinceId")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult CityByProvinceId(int id)
        {
            var Province = _categoryService.CityByProvinceId(id);
            return Ok(Province);
        }
        [HttpGet]
        [Route("cityList")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult cityList()
        {
            var cityList = _categoryService.GetcityList();
            return Ok(cityList);
        }
        [HttpGet]
        [Route("CampaignList")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult CampaignListList()
        {
            var campaignList = _categoryService.GetCampaignList();
            return Ok(campaignList);
        }
    }
}