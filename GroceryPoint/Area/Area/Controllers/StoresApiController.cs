﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Areas.Area.Controllers;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GroceryPoint.Area.Area.Controllers
{
    public class StoresApiController : Controller
    {
        private readonly IConfiguration _configuration;
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;
        private string ApiGlobalUrl;

        [Obsolete]
        public StoresApiController(IConfiguration configuration, IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }
        public async Task<IActionResult> AddStore()
        {
            return View();
        }
        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> AddStore(StoreDto storeDto, IFormFile file)
        {
            string StorName = "Store2";
            string path_Root = _appEnvironment.WebRootPath;
            string filename = file.FileName;
            string filepath = path_Root + "//ProductImages";
            string FileNameForDb = Guid.NewGuid() + file.FileName;

            //if (!(Directory.Exists(path_Root + "//StoreImages//" + StorName)))
            //{
            //    Directory.CreateDirectory(path_Root + "//ProductImages//" + StorName);
            //}

            string saveImg = path_Root + "\\StoreImages\\" + FileNameForDb;
            using (var stream = new FileStream(saveImg, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            string apiUrl = ApiGlobalUrl + "api/AddProduct";

            StoreDto _storeDto = new StoreDto();
            _storeDto.storeName = storeDto.storeName;
            _storeDto.streetAddress = storeDto.streetAddress;
            _storeDto.unit = storeDto.unit;
            _storeDto.cityID = storeDto.cityID;
            _storeDto.provinceID = storeDto.provinceID;
            _storeDto.logo = storeDto.logo;
            _storeDto.phone = storeDto.phone;
            _storeDto.email = storeDto.email;
            _storeDto.postalCode = storeDto.postalCode;
            _storeDto.firstName = storeDto.firstName;
            _storeDto.lastName = storeDto.lastName;
            _storeDto.ipAddress = storeDto.ipAddress;



            using (var httpClient = new HttpClient())
            {
                var res = JsonConvert.SerializeObject(_storeDto);
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(_storeDto), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != null)
                    {
                        TempData["success"] = "Added Successfully";

                        return RedirectToAction("Index", "Products");
                    }
                    //_productDto = JsonConvert.DeserializeObject<ProductUnitDto>(apiResponse);
                }
            }


            ViewBag.FileNameForDb = FileNameForDb;

            ViewBag.CityId = new SelectList(await CityList(), "CityId", "CityName");
            ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr");

            return View();
        }
        public async Task<List<City>> CityList()
        {
            string apiUrl = ApiGlobalUrl + "api/CityList";

            List<City> CityList = new List<City>();

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            return CityList;
        }
        public async Task<List<Province>> ProvinceList()
        {
            string apiUrl = ApiGlobalUrl + "api/provinceList";

            List<Province> ProvinceList = new List<Province>();

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProvinceList = JsonConvert.DeserializeObject<List<Province>>(apiResponse);
                }
            }
            return ProvinceList;
        }


    }
}