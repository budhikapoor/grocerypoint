﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Area.Area.Models.Services;
using GroceryPoint.Areas.Area.Controllers;
using Microsoft.AspNetCore.Mvc;
using ApiControllerAttribute = Microsoft.AspNetCore.Mvc.ApiControllerAttribute;

namespace GroceryPoint.Area.Area.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    [ApiController]
    public class ProductsApiController : ControllerBase
    {
        private IProductsService _ProductsService;

        public ProductsApiController(IProductsService ProductsService)
        {
            _ProductsService = ProductsService;
        }

        [HttpGet]
        [Route("ProductList")]
        [AllowAnonymous]
        //[HttpPost("authenticate")]
        public IActionResult Product(int storeId, int  categoryid)
        {
            var Product = _ProductsService.GetProducts(storeId, categoryid);
            return Ok(Product);
        }
    }
}