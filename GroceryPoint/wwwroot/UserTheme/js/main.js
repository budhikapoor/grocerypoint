(function ($) {
  "use strict";

  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return false;
  });

  // Header scroll class
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
  } 

$('.card-link').removeClass('collapsed');

$('.filter-open').click(function(){
$('#sidebar').css('left', '0');
$('html').addClass('filter-active');
});
$('.filter-close').click(function(){
$('#sidebar').css('left', '-320px');
$('html').removeClass('filter-active');
});
$('#checkput-ancr').click(function(){
$(this).addClass('active');
$('.checkout-blk').addClass('active');
$('.checkout-active').addClass('active');
});
$('.checkout-active').click(function(){
$('#checkput-ancr').removeClass('active');
$('.checkout-blk').removeClass('active');
$(this).removeClass('active');
});
$('.close-checkout').click(function(){
$('#checkput-ancr').removeClass('active');
$('.checkout-blk').removeClass('active');
$('.checkout-active').removeClass('active');
});

$(".search-lable").click(function(){
    $('.products-dropdown').toggleClass('active');
  });
$("div#sidebar .hide-tab").click(function(){
    $('div#sidebar ul.cat-ul').toggleClass('active');
  });
$("div#sidebar ul.cat-ul li:not('.sub-cat')").click(function(){
    $(this).parent().toggleClass('active');
  });
$("div#sidebar ul.cat-ul li.sub-cat li").click(function(){
    $(this).parents('ul.cat-ul').toggleClass('active');
  });

$("#phone").inputmask({"mask": "(999) 999-9999"});
})(jQuery);
/***************************** select **************************/

      //$(".myselect").select2();

