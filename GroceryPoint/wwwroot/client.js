﻿// A reference to Stripe.js initialized with your real test publishable API key.
var stripe = Stripe("pk_test_6GOw8AEvGpkAd5zmPuKJDgGa0092b1KIc5");

// The items the customer wants to buy
var purchase = {
    items: [{ id: "xl-tshirt" }]
};
$(document).ready(function () {
    var elements = stripe.elements();

    var style = {
        base: {
            color: "#32325d",
            fontFamily: 'Arial, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#32325d"
            }
        },
        invalid: {
            fontFamily: 'Arial, sans-serif',
            color: "#fa755a",
            iconColor: "#fa755a"
        }
    };

    var card = elements.create("card", { style: style });
    // Stripe injects an iframe into the DOM
    card.mount("#card-element");

    card.on("change", function (event) {
        // Disable the Pay button if there are no card details in the Element
        document.querySelector("button").disabled = event.empty;
        document.querySelector("#card-errors").textContent = event.error ? event.error.message : "";
    });

    var form = document.getElementById("payment-form");
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        // Complete payment when the submit button is clicked
        payWithCard(stripe, card);
    });
    $("#button-Customer").click(function () {
        CreateCustomer();
    });
    $("#button-Card").click(function () {
        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the customer that there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                CreateCard(result.token.id, "0");
            }
        });        
    });
    $("#button-Charge").click(function () {
        ChargeCard();
    });
    $("#button-Create-Charge").click(function () {
        CreateCharge();
    });
    $("#button-Capture-Charge").click(function () {
        CaptureCharge();
    });
    $("#button-Delete-Card").click(function () {
        DeleteCard();
    });
    $("#button-Delete-Customer").click(function () {
        DeleteCustomer();
    });
    $("#button-Refund-Charge").click(function () {
        RefundCharge();
    });
    $("#button-Authorize").click(function () {
        debugger;
        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the customer that there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                AuthorizeCard(result.token.id, "0");
            }
        });
    });
    // Disable the button until we have Stripe set up on the page
    document.querySelector("button").disabled = true;
});
function generateIntent() {
    fetch("/create-payment-intent", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(purchase)
    })
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            data.clientSecret = "";
        });
}
function CreateCustomer() {
    fetch("/create-payment-intent/CreateCustomer", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(purchase)
    })
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            data.clientSecret = "";
        });
}
var CreateCard = function (token) {    
    $.ajax({
        type: "POST",
        data: { "token": token },
        url: "/create-payment-intent/CreateCard?token="+token,
        success: function (data) {
            alert(data);
            //location.reload();
        },
        error: function (data) {
            alert("error");
        }
    });
};

var CreateCharge = function () {    
    $.ajax({
        type: "POST",
        //data: { "token": token },
        url: "/create-payment-intent/CreateCharge",
        success: function (data) {
            alert(data);
            //location.reload();
        },
        error: function (data) {
            alert("error");
        }
    });
};

var DeleteCard = function () {
    $.ajax({
        type: "Delete",      
        url: "/create-payment-intent/DeleteCard",
        success: function (data) {
            alert(data);
            //location.reload();
        }
    });
};

var DeleteCustomer = function () {
    $.ajax({
        type: "Delete",
        url: "/create-payment-intent/DeleteCustomer",
        success: function (data) {
            alert(data);
            //location.reload();
        }
    });
};

var CaptureCharge = function () {
    $.ajax({
        type: "POST",
        //data: { "token": token },
        url: "/create-payment-intent/CaptureCharge",
        success: function (data) {
            alert(data);
            //location.reload();
        }
    });
};

var RefundCharge = function () {
    $.ajax({
        type: "POST",       
        url: "/create-payment-intent/RefundCharge",
        success: function (data) {
            alert(data);
            //location.reload();
        }
    });
};

var AuthorizeCard = function (token, type) {   

    // Submit the form
    //form.submit();
    $.ajax({
        type: "GET",
        data: { "token": token, "type": type },
        url: "/create-payment-intent",
        success: function (data) {
            alert("Card Authorized Successfully");
            location.reload();
        },
        error: function (result, status, error) {
            var obj = jQuery.parseJSON(result.responseText);                  
            showError(obj.message);           
        }
    });
};
var ChargeCard = function () {
    //var form = document.getElementById('payment-form');
    //var hiddenInput = document.createElement('input');
    //hiddenInput.setAttribute('type', 'hidden');
    //hiddenInput.setAttribute('name', 'stripeToken');
    //hiddenInput.setAttribute('value', token.id);
    //form.appendChild(hiddenInput);

    // Submit the form
    //form.submit();

    AuthorizeCard($("#txtToken").val(), "1");
    //$.ajax({
    //    type: "GET",
    //    data: { "token": $("#txtToken").val() },
    //    url: "/create-payment-intent/ChargeCard",
    //    success: function (data) {
    //        alert("Card Authorized Successfully");
    //        location.reload();
    //    }
    //});
};
// Calls stripe.confirmCardPayment
// If the card requires authentication Stripe shows a pop-up modal to
// prompt the user to enter authentication details without leaving your page.
var payWithCard = function (stripe, card, clientSecret) {
    fetch("/create-payment-intent", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(purchase)
    })
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            debugger;
            var clientSecret = data.clientSecret;
            loading(true);
            stripe
                .confirmCardPayment(clientSecret, {
                    payment_method: {
                        card: card
                    }
                })
                .then(function (result) {
                    if (result.error) {
                        // Show error to your customer
                        showError(result.error.message);
                    } else {
                        // The payment succeeded!
                        orderComplete(result.paymentIntent.id);
                    }
                });
        });
   
};

/* ------- UI helpers ------- */

// Shows a success message when the payment is complete
var orderComplete = function (paymentIntentId) {
    loading(false);
    document
        .querySelector(".result-message a")
        .setAttribute(
            "href",
            "https://dashboard.stripe.com/test/payments/" + paymentIntentId
        );
    document.querySelector(".result-message").classList.remove("hidden");
    document.querySelector("button").disabled = true;
    alert("Payment received successfully");
    location.reload();
};

// Show the customer the error from Stripe if their card fails to charge
var showError = function (errorMsgText) {
    loading(false);
    var errorMsg = document.querySelector("#card-errors");
    errorMsg.textContent = errorMsgText;
    setTimeout(function () {
        errorMsg.textContent = "";
    }, 8000);
};

// Show a spinner on payment submission
var loading = function (isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        //document.querySelector("button").disabled = true;
        //document.querySelector("#spinner").classList.remove("hidden");
        //document.querySelector("#button-text").classList.add("hidden");
    } else {
        //document.querySelector("button").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
    }
};
