﻿// A reference to Stripe.js initialized with your real test publishable API key.
var stripe = Stripe("pk_test_6GOw8AEvGpkAd5zmPuKJDgGa0092b1KIc5");


$(document).ready(function () {
    var elements = stripe.elements();
    var style = {
        base: {
            color: "#32325d",
            fontFamily: 'Arial, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#32325d"
            }
        },
        invalid: {
            fontFamily: 'Arial, sans-serif',
            color: "#fa755a",
            iconColor: "#fa755a"
        }
    };




  //  var card = elements.create("card", { style: style });
    // Stripe injects an iframe into the DOM
    //card.mount("#card-element");


    var elementStyles = {
        base: {
            color: '#32325D',
            fontWeight: 500,
            fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
            fontSize: '16px',
            fontSmoothing: 'antialiased',

            '::placeholder': {
                color: '#CFD7DF',
            },
            ':-webkit-autofill': {
                color: '#e39f48',
            },
        },
        invalid: {
            color: '#E25950',

            '::placeholder': {
                color: '#FFCCA5',
            },
        },
    };

    var elementClasses = {
        focus: 'focused',
        empty: 'empty',
        invalid: 'invalid',
    };




    var card = elements.create('cardNumber', {
        style: elementStyles,
        classes: elementClasses,
    });
    card.mount('#card-number');

    var cardExpiry = elements.create('cardExpiry', {
        style: elementStyles,
        classes: elementClasses,
    });
    cardExpiry.mount('#card-expiry');

    var cardCvc = elements.create('cardCvc', {
        style: elementStyles,
        classes: elementClasses,
    });
    cardCvc.mount('#card-cvc');    

    card.on("change", function (event) {
        // Disable the Pay button if there are no card details in the Element
        document.querySelector("button").disabled = event.empty;
        document.querySelector("#card-errors").textContent = event.error ? event.error.message : "";
    });

    var form = document.getElementById("payment-form");
    form.addEventListener("submit", function (event) {
        event.preventDefault();
        // Complete payment when the submit button is clicked
        //payWithCard(stripe, card);
    }); 
    
    $("#button-text").click(function () {        
        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the customer that there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
                showError(result.error.message);
            } else {
                // Send the token to your server.
                var amount = document.getElementById("grandTotal").value;
                AuthorizeCard(result.token.id, amount);
            }
        });
    });
    // Disable the button until we have Stripe set up on the page
    document.querySelector("button").disabled = true;
});

var AuthorizeCard = function (token, amount) {
    loading(true);
    $.ajax({       
        type: "GET",
        data: { "token": token, "amount": amount },
        url: "/create-payment-intent/Authorize",
        success: function (data) {          
            SaveOrder(data.data, data.cardType, data.last4);            
        },
        error: function (result, status, error) {
            loading(false);
            var obj = jQuery.parseJSON(result.responseText);
            showError(obj.message);
        }
    });
};

/* ------- UI helpers ------- */
// Show the customer the error from Stripe if their card fails to charge
var showError = function (errorMsgText) {
    loading(false);
    var errorMsg = document.querySelector("#card-errors");
    errorMsg.textContent = errorMsgText;
    //setTimeout(function () {
    //    errorMsg.textContent = "";
    //}, 8000);
};

// Show a spinner on payment submission
var loading = function (isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("button").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
    } else {
        document.querySelector("button").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
    }
};