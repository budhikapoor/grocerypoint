using GroceryPoint.Area.Area.Models.Services;
using GroceryPoint.Areas.Area.Models.Services;
using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.Cart;
using GroceryPoint.IRepos.Order;
using GroceryPoint.IRepos.Store;
using GroceryPoint.IRepos.System;
using GroceryPoint.IRepos.Users;
using GroceryPoint.IServices.Cart;
using GroceryPoint.IServices.Order;
using GroceryPoint.IServices.Store;
using GroceryPoint.IServices.System;
using GroceryPoint.IServices.Users;
using GroceryPoint.Repos.Cart;
using GroceryPoint.Repos.Order;
using GroceryPoint.Repos.Store;
using GroceryPoint.Repos.System;
using GroceryPoint.Repos.Users;
using GroceryPoint.Services.Cart;
using GroceryPoint.Services.Store;
using GroceryPoint.Services.System;
using GroceryPoint.Services.Users;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Stripe;
using System;
using System.Text;


namespace GroceryPoint
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddNewtonsoftJson();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddCors();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddTransient<IProductRepository>(u => new ProductRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<ICartRepository>(u => new CartRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<ICampaignRepository>(u => new CampaignRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<IStoreRepository>(u => new StoreRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<IUsersRepository>(u => new UsersRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<IOrderRepository>(u => new OrderRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<IEmailRepository>(u => new EmailRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<IOrderStatusRepository>(u => new OrderStatusRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));
            services.AddTransient<IDeliveryDriverRepository>(u => new DeliveryDriverRepository(Configuration["ConnectionStrings:GroceryPointDBConnectionStrings"]));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
 options.TokenValidationParameters = new TokenValidationParameters
 {
     ValidateIssuer = true,
     ValidateAudience = true,
     ValidateLifetime = true,
     ValidateIssuerSigningKey = true,
     ValidIssuer = Configuration["Jwt:Issuer"],
     ValidAudience = Configuration["Jwt:Issuer"],
     IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
 };
});
            services.AddControllers()
    .AddNewtonsoftJson();
            services.AddControllers().AddJsonOptions(options => {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
                options.JsonSerializerOptions.DictionaryKeyPolicy = null;

            });

            //services.AddMvc()
            //.AddJsonOptions(options =>
            //{
            //    options.SerializerSettings.ContractResolver
            //        = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            //});
            services.AddSession(option=>
            {
                option.IdleTimeout = TimeSpan.FromMinutes(100);
            });
            services.AddMvc();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IProductService, Services.Store.ProductService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<ICampaignService, CampaignService>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IOrderService, Services.Order.OrderService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IOrderStatusService, OrderStatusService>();
            services.AddScoped<IDeliveryDriverService, DeliveryDriverService>();
            services.AddScoped<IProductsService, ProductSService>();
            services.AddCors();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddControllersWithViews();
            services.Configure<StripeDto>(Configuration.GetSection("Stripe"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.Extensions.Hosting.IHostingEnvironment env)
        {
            // This is your real test secret API key.
            StripeConfiguration.ApiKey = "sk_test_fUYfC21z4DGgY7iQUf0oIIJW00elc8JRV8";

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

    }
}
