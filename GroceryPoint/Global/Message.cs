﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace GroceryPoint.Global
{
    public class Message
    {        
        public static void SendTextMessage(string toPhoneNumber, string body)
        {
            // Find your Account Sid and Token at twilio.com/console
            // DANGER! This is insecure. See http://twil.io/secure
            const string accountSid = "AC38852d2f97c5fae7de4f6fa1abb2e0b7";
            const string authToken = "9ebeac8284c6137caa678fad5166a360";

            TwilioClient.Init(accountSid, authToken);

            var to = new PhoneNumber(toPhoneNumber);
            var from = new PhoneNumber("+17782002481");

            var message = MessageResource.Create(
                body: body,
                from: from,
                to: to
            ); 

            //return message.ToString();
        }

        public static void MakeCall(string toPhoneNumber, string body)
        {
            // Find your Account Sid and Token at twilio.com/console
            // DANGER! This is insecure. See http://twil.io/secure
            const string accountSid = "AC38852d2f97c5fae7de4f6fa1abb2e0b7";
            const string authToken = "9ebeac8284c6137caa678fad5166a360";

            TwilioClient.Init(accountSid, authToken);

            var to = new PhoneNumber(toPhoneNumber);
            var from = new PhoneNumber("+17782002481");
            var twimlMessage = new Twiml("<Response><Say voice='alice'>" + body + "</Say></Response>");

            var call = CallResource.Create(
               twiml: twimlMessage,
               to: to,
               from: from
                );

            //return call.ToString();
        }


    }
}
