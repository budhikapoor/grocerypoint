﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Stripe;


namespace GroceryPoint.Global
{
    public class Stripe
    {       
        public void CreateCustomer(string customerName)
        {
            var options = new CustomerCreateOptions
            {
                Description = customerName,
                Name = customerName,
               // Email = "test@test.com",
               // Phone = "1234567890"
            };
            var service = new CustomerService();
            var data = service.Create(options);
            //return Json(new { data = data });            
        }

        public void CreateCard(string token, string customerRef)
        {
            var options = new CardCreateOptions
            {
                Source = token,
            };
            var service = new CardService();
            var data = service.Create(customerRef, options);
            //return Json(new { data = data });
        }

        public void CreateCharge(long amount, string customerRef, string cardRef, string orderNumber)
        {
            // `source` is obtained with Stripe.js; see https://stripe.com/docs/payments/accept-a-payment-charges#web-create-token
            var options = new ChargeCreateOptions
            {
                Amount = amount,
                Currency = "cad",
                Customer = customerRef,
                Capture = false,
                Source = cardRef,
                Description = orderNumber,
            };
            var service = new ChargeService();
            var data = service.Create(options);
            //return Json(new { data = data });
        }

        public void CaptureCharge(string chargeRef, string amount)
        {
            //amount = amount.ToString().Replace(".", "");
            //Int64 finalAmount = Convert.ToInt64(amount);

            amount = amount.Replace("$", "");
            decimal holdAmount = Convert.ToDecimal(amount);
            holdAmount = decimal.Round(holdAmount, 2, MidpointRounding.AwayFromZero);

            amount = holdAmount.ToString().Replace(".", "");
            Int64 finalAmount = Convert.ToInt64(amount);


            var options = new ChargeCaptureOptions
            {
                Amount = finalAmount,
                StatementDescriptor = "Grocery Point",
            };

            var service = new ChargeService();
            var data = service.Capture(chargeRef, options);
           // return Json(new { data = data });
        }

        public void DeleteCard(string customerRef, string cardRef)
        {
            var service = new CardService();
            var data = service.Delete(
              customerRef,
              cardRef
            );
           // return Json(new { data = data });
        }

        public void DeleteCustomer(string customerRef)
        {
            var service = new CustomerService();
            var data = service.Delete(customerRef);
            //return Json(new { data = data });
        }

        public void RefundCharge(string chargeRef, long amount)
        {
            var options = new RefundCreateOptions
            {
                Charge = chargeRef,
                Amount = amount,
            };
            var service = new RefundService();
            var data = service.Create(options);
            //return Json(new { data = data });
        }
    }
}
