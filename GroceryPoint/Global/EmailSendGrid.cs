﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using GroceryPoint.Dtos.Campaign;
using GroceryPoint.Global;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace GroceryPoint.Global
{
    public  class EmailSendGrid
    {
        public string _ApiGlobalUrl;
        private readonly IConfiguration _configuration;       
        public EmailSendGrid( IConfiguration configuration)
        {            
            _configuration = configuration;
            _ApiGlobalUrl= configuration.GetValue<string>("apiUrl");           

        }

        public EmailSendGrid()
        {
           
        }

        public static string SendEmail(string FromEmail, string DisplayName, string ToEmail, string Subject, string plainTextContent, string HTMLContent)
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(FromEmail, DisplayName);
            var to = new EmailAddress(ToEmail, ToEmail);
            var msg = MailHelper.CreateSingleEmail(from, to, Subject, plainTextContent, HTMLContent);
            var response = client.SendEmailAsync(msg);
            return response.ToString();
        }

        public async  void  SendEmailToUser(int emailTemplateID, Int64 userID, Int64 storeID, Int64 orderID, int userTypeID)
        {
            EmailDto emailobj = new EmailDto();

            using (var httpClient = new HttpClient())
            {
                string _ApiGlobalUrl = "https://localhost:44330/";
                string apiUrl = _ApiGlobalUrl + "api/GetEmailDetails?emailTemplateID=" + emailTemplateID + "&userID=" + userID + "&storeID=" + storeID + "&orderID=" + orderID + "&userTypeID=" + userTypeID;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    emailobj = JsonConvert.DeserializeObject<EmailDto>(apiResponse);
                }
            }
            EmailSendGrid.SendEmail(emailobj.fromEmail, emailobj.displayName, emailobj.toEmail, emailobj.subject, "", emailobj.body);


        }
        public static void SendEmails(int emailTemplateID, Int64 userID, Int64 storeID, Int64 orderID, int userTypeID)
        {
            EmailSendGrid obj = new EmailSendGrid();
            obj.SendEmailToUser(emailTemplateID, userID, storeID, orderID, userTypeID);

        }

    

        //public Task SendSMS(string phoneNumber, string message)
        //{
        //    return Task.Run(() =>
        //    {
        //        TwilioRestClient twilio = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        //        twilio.SendSmsMessage(TWILIO_PHONE_NUMBER, phoneNumber, message);
        //    });
        //}


    }
}
