﻿using GroceryPoint.Dtos.Cart;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.IServices.Cart
{
    public interface ICartService
    {
        CartSummaryDto AddItemsToCart(CartDto cart);
        CartSummaryDto GetCartTotal(Int64 storeID, Int64 userID);
        IEnumerable<CartDetailsDto> GetCartDetails(Int64 storeID, Int64 userID);

        string DeleteCart(Int64 storeID, Int64 userID);

        string DeleteCartItem(Int64 cartID);

    }
}
