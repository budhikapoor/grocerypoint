﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Users;

namespace GroceryPoint.IServices.Users
{
    public interface IUsersService
    {
        Int64 UpdateDefaultStore(Int64 userID, Int64 storeID);
        Int64 AddAddress(AddressDto address);
        IEnumerable<AddressDetailsDto> GetUserAddresses(Int64 userID, Int64 addressID);
        UserDto GetUserProfile(Int64 userID);
        Int64 UpdateUserProfile(UserDto user);
        Int64 ChangePassword(Int64 userID, string password, string ipAddress);
        string VerifyEmail(Int64 userID, string email);
    }
}
