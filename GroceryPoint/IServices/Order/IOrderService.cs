﻿using GroceryPoint.Dtos.Order;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.IServices.Order
{
    public interface IOrderService
    {
        Int64 AddOrder(OrderDto order);
        PreOrderDetailsDto GetPreOrderDetails(Int64 storeID, Int64 userID, Int64 addressID);

        IEnumerable<object> GetAllDeliveryDates(DateTime todayDateTime);
        IEnumerable<object> GetAllDeliveryTimes(DateTime selectedDate);
        IEnumerable<UserOrdersDto> GetUserOrders(Int64 userID);
        UserOrderDetails GetUserOrderDetails(Int64 orderID);
    }
}
