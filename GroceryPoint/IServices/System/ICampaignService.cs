﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.IServices.System
{
    public interface ICampaignService
    {
        IEnumerable<object> GetAllCampaign();
    }
}
