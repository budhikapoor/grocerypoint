﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;

namespace GroceryPoint.IServices.System
{
    public interface IDeliveryDriverService
    {
        Int64 AddDeliveryDriver(DeliveryDriverDto deliveryDriver);
    }
}
