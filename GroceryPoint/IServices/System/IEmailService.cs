﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;

namespace GroceryPoint.IServices.System
{
    public interface IEmailService
    {
        EmailDto GetEmailDetails(Int64 emailTemplateID, Int64 userID, Int64 storeID, Int64 orderID, Int64 userTypeID);
    }
}
