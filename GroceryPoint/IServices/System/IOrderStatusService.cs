﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.IServices.System
{
    public interface IOrderStatusService
    {
        IEnumerable<object> GetAllOrderStatus();
    }
}
