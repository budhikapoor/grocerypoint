﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using System.Data.SqlClient;
using System.Data;
using GroceryPoint.Dtos.Store;
using GroceryPoint.IRepos.Store;

namespace GroceryPoint.Repos.Store
{
    public class StoreRepository: IStoreRepository
    {
        private SqlConnection _connection;
        public StoreRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public IEnumerable<StoreSummaryDto> GetAllStoresByCity(Int64 cityID)
        {  
            IList<StoreSummaryDto> storeList = new List<StoreSummaryDto>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetAllStoresByCity";
                command.Parameters.Add("@cityID", SqlDbType.BigInt).Value = cityID;
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var storedetails = new StoreSummaryDto
                            {                              
                                storeID = (Int64)reader["storeID"],        
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                streetAddress = !Convert.IsDBNull(reader["streetAddress"]) ? (string)reader["streetAddress"] : "",
                                city = !Convert.IsDBNull(reader["city"]) ? (string)reader["city"] : "",
                                province = !Convert.IsDBNull(reader["province"]) ? (string)reader["province"] : "",
                                postalCode = !Convert.IsDBNull(reader["postalCode"]) ? (string)reader["postalCode"] : "",
                                logo = !Convert.IsDBNull(reader["logo"]) ? (string)reader["logo"] : "",
                                callPhone = !Convert.IsDBNull(reader["callPhone"]) ? (string)reader["callPhone"] : "",
                                textPhone = !Convert.IsDBNull(reader["textPhone"]) ? (string)reader["textPhone"] : "",
                                isCall = (bool)reader["isCall"],
                                isText = (bool)reader["isText"],

                            };

                            storeList.Add(storedetails);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return storeList;
        }

        public IEnumerable<ProductDto> SearchStore(Int64 storeID, string searchPar)
        {
            IList<ProductDto> productList = new List<ProductDto>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_SearchStore";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@searchPar", SqlDbType.NVarChar).Value = searchPar;
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var productdetails = new ProductDto
                            {  
                                productID = (Int64)reader["productID"],
                                productName = !Convert.IsDBNull(reader["productName"]) ? (string)reader["productName"] : "",
                                image = !Convert.IsDBNull(reader["image"]) ? (string)reader["image"] : "",
                                price = (decimal)reader["price"],
                                storeID = (Int64)reader["storeID"],
                                categoyID = (Int64)reader["categoyID"],
                                isActive = (bool)reader["isActive"],
                            };

                            productList.Add(productdetails);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return productList;
        }

        public IEnumerable<StoreOrderDto> GetAllStoreOrders(Int64 storeID)
        {
            IList<StoreOrderDto> storeOrderList = new List<StoreOrderDto>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetStoreOrders";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;                
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var storeOrder = new StoreOrderDto
                            {                                
                                orderID = (Int64)reader["orderID"],
                                orderNumber = !Convert.IsDBNull(reader["orderNumber"]) ? (string)reader["orderNumber"] : "",
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                customerName = !Convert.IsDBNull(reader["customerName"]) ? (string)reader["customerName"] : "",                               
                                orderPhone = !Convert.IsDBNull(reader["orderPhone"]) ? (string)reader["orderPhone"] : "",
                                qty = (Int32)reader["qty"],                               
                                subtotal = (decimal)reader["subtotal"],
                                tax = (decimal)reader["tax"],
                                clientTotal = (decimal)reader["clientTotal"],
                                amountPaid = (decimal)reader["amountPaid"],
                                percentage = (decimal)reader["percentage"],
                                total = (decimal)reader["total"],
                                orderDate = !Convert.IsDBNull(reader["orderDate"]) ? (string)reader["orderDate"] : "",
                                orderStatus = !Convert.IsDBNull(reader["orderStatus"]) ? (string)reader["orderStatus"] : "",
                                deliveryDate = !Convert.IsDBNull(reader["deliveryDate"]) ? (string)reader["deliveryDate"] : "",
                                orderInstructions = !Convert.IsDBNull(reader["orderInstructions"]) ? (string)reader["orderInstructions"] : "",
                            };

                            storeOrderList.Add(storeOrder);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return storeOrderList;
        }

        public StoreOrderDetailsDto GetStoreOrderDetails(Int64 orderID)
        {
            #region Order Product Details

            IList<StoreOrderProductDetailsDto> orderProductList = new List<StoreOrderProductDetailsDto>();

            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetStoreOrderProductDetails";
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var products = new StoreOrderProductDetailsDto
                            {
                                productID = (Int64)reader["productID"],
                                orderDetailsID = (Int64)reader["orderDetailsID"],
                                userID = (Int64)reader["userID"],
                                productName = !Convert.IsDBNull(reader["productName"]) ? (string)reader["productName"] : "",
                                productUnit = !Convert.IsDBNull(reader["productUnit"]) ? (string)reader["productUnit"] : "",
                                price = (decimal)reader["price"],
                                qty = (Int32)reader["qty"],
                                subtotal = (decimal)reader["subtotal"],
                            };

                            orderProductList.Add(products);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
               {
                    _connection.Close();
                }
            }

            #endregion End Order Products

            #region Order Details

            StoreOrderDetailsDto orderDetails = null;

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetStoreOrdersDetails";
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            orderDetails = new StoreOrderDetailsDto
                            {
                                orderID = (Int64)reader["orderID"],
                                StatusId = (int)reader["orderStatusID"],
                                orderNumber = !Convert.IsDBNull(reader["orderNumber"]) ? (string)reader["orderNumber"] : "",
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                qty = (Int32)reader["qty"],
                                tax = (decimal)reader["tax"],
                                subtotal = (decimal)reader["subtotal"],
                                storeTotal = (decimal)reader["storeTotal"],
                                customerName = !Convert.IsDBNull(reader["customerName"]) ? (string)reader["customerName"] : "",
                                deliveryDate = !Convert.IsDBNull(reader["deliveryDate"]) ? (string)reader["deliveryDate"] : "",
                                orderInstructions = !Convert.IsDBNull(reader["orderInstructions"]) ? (string)reader["orderInstructions"] : "",
                                orderDate = !Convert.IsDBNull(reader["orderDate"]) ? (string)reader["orderDate"] : "",
                                orderStatus = !Convert.IsDBNull(reader["orderStatus"]) ? (string)reader["orderStatus"] : "",
                                orderPhone = !Convert.IsDBNull(reader["orderPhone"]) ? (string)reader["orderPhone"] : "",
                                deliveryAddress = !Convert.IsDBNull(reader["deliveryAddress"]) ? (string)reader["deliveryAddress"] : "",
                                clientTotal = (decimal)reader["clientTotal"],
                                amountPaid = (decimal)reader["amountPaid"],
                                percentage = (decimal)reader["percentage"],
                                orderProductList = orderProductList
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            #endregion End Order Details

            return orderDetails;
        }

        public Int64 UpdateOrderStatus(Int64 orderID, Int64 orderStatusID, Int64 userID)
        {
            using (var command = new SqlCommand())
            {                
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_UpdateOrderStatus";
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Parameters.Add("@orderStatusID", SqlDbType.NVarChar).Value = orderStatusID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return orderStatusID;
        }

        public Int64 AddStore(StoreDto store, byte[] passwordHash, byte[] passwordSalt)
        {
            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {
                if (store.ipAddress == null)
                {
                    store.ipAddress = "";
                }

                if (store.logo == null)
                {
                    store.logo = "";
                }

                if (store.unit == null)
                {
                    store.unit = "";
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_AddStore";               
                command.Parameters.Add("@storeName", SqlDbType.NVarChar).Value = store.storeName;
                command.Parameters.Add("@streetAddress", SqlDbType.NVarChar).Value = store.streetAddress;
                command.Parameters.Add("@unit", SqlDbType.NVarChar).Value = store.unit;
                command.Parameters.Add("@cityID", SqlDbType.BigInt).Value = store.cityID;
                command.Parameters.Add("@provinceID", SqlDbType.BigInt).Value = store.provinceID;
                command.Parameters.Add("@logo ", SqlDbType.NVarChar).Value = store.logo;
                command.Parameters.Add("@phone ", SqlDbType.NVarChar).Value = store.phone;
                command.Parameters.Add("@email ", SqlDbType.NVarChar).Value = store.email;
                command.Parameters.Add("@postalCode ", SqlDbType.NVarChar).Value = store.postalCode;
                command.Parameters.Add("@firstName ", SqlDbType.NVarChar).Value = store.firstName;
                command.Parameters.Add("@lastName ", SqlDbType.NVarChar).Value = store.lastName;
                command.Parameters.Add("@passwordHash ", SqlDbType.Binary).Value = passwordHash;
                command.Parameters.Add("@passwordSalt ", SqlDbType.Binary).Value = passwordSalt;
                command.Parameters.Add("@ipAddress ", SqlDbType.NVarChar).Value = store.ipAddress;              
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    store.storeID = Convert.ToInt64(command.Parameters["@storeID"].Value);
                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return store.storeID;
        }

        public StoreDto GetStoreDetails(Int64 storeID)
        {
            StoreDto store = new StoreDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetStoreDetails";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            store = new StoreDto
                            {
                                storeID = (Int64)reader["storeID"],
                            
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                callPhone = !Convert.IsDBNull(reader["callPhone"]) ? (string)reader["callPhone"] : "",
                                textPhone = !Convert.IsDBNull(reader["textPhone"]) ? (string)reader["textPhone"] : "",
                                isCall = (bool)reader["isCall"],
                                isText = (bool)reader["isText"],
                             
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return store;
        }

        public string DeleteOrderItem(Int64 orderDetailsID)
        {
            string result = "";
            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_DeleteOrderItem";
                command.Parameters.Add("@orderDetailsID", SqlDbType.BigInt).Value = orderDetailsID;              
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    result = "Order Item Deleted!";
                }
                catch (Exception ex)
                {
                    result = ex.Message.ToString();
                }
                finally
                {

                    _connection.Close();
                }

            }
            return result;
        }

        public string UpdateOrderItem(Int64 orderDetailsID, Int64 qty, Int64 userID)
        {
            string result = "";
            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_UpdateOrderItem";
                command.Parameters.Add("@orderDetailsID", SqlDbType.BigInt).Value = orderDetailsID;
                command.Parameters.Add("@qty", SqlDbType.BigInt).Value = qty;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    result = "Order Item Updated!";
                }
                catch (Exception ex)
                {
                    result = ex.Message.ToString();
                }
                finally
                {

                    _connection.Close();
                }

            }
            return result;
        }

        public StoreOrderDetailsDto GetStoreOrderBasicData(Int64 orderID)
        {  
            StoreOrderDetailsDto orderDetails = null;

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetStoreOrdersDetails";
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            orderDetails = new StoreOrderDetailsDto
                            {
                                orderID = (Int64)reader["orderID"],
                                StatusId = (int)reader["orderStatusID"],
                                orderNumber = !Convert.IsDBNull(reader["orderNumber"]) ? (string)reader["orderNumber"] : "",
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                qty = (Int32)reader["qty"],
                                tax = (decimal)reader["tax"],
                                subtotal = (decimal)reader["subtotal"],
                                total = (decimal)reader["total"],
                                storeTotal = (decimal)reader["storeTotal"],
                                customerName = !Convert.IsDBNull(reader["customerName"]) ? (string)reader["customerName"] : "",
                                deliveryDate = !Convert.IsDBNull(reader["deliveryDate"]) ? (string)reader["deliveryDate"] : "",
                                orderInstructions = !Convert.IsDBNull(reader["orderInstructions"]) ? (string)reader["orderInstructions"] : "",
                                orderDate = !Convert.IsDBNull(reader["orderDate"]) ? (string)reader["orderDate"] : "",
                                orderStatus = !Convert.IsDBNull(reader["orderStatus"]) ? (string)reader["orderStatus"] : "",
                                orderPhone = !Convert.IsDBNull(reader["orderPhone"]) ? (string)reader["orderPhone"] : "",
                                deliveryAddress = !Convert.IsDBNull(reader["deliveryAddress"]) ? (string)reader["deliveryAddress"] : "",
                                clientTotal = (decimal)reader["clientTotal"],
                                amountPaid = (decimal)reader["amountPaid"],
                                percentage = (decimal)reader["percentage"],
                                isPaid = (bool)reader["isPaid"],
                                chargeRef = !Convert.IsDBNull(reader["chargeRef"]) ? (string)reader["chargeRef"] : "",
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }
            return orderDetails;
        }
    }
}
