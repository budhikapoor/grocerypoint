﻿using GroceryPoint.Dtos.Store;
using GroceryPoint.IRepos.Store;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Repos.Store
{
    public class ProductRepository: IProductRepository
    {
        private SqlConnection _connection;
        public ProductRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public Int64 AddProduct(ProductDto product)
        {
            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {
                if (product.description == null)
                {
                    product.description = "";
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_AddProduct";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = product.storeID;
                command.Parameters.Add("@productName", SqlDbType.NVarChar).Value = product.productName;
                command.Parameters.Add("@price", SqlDbType.Money).Value = product.price;
                command.Parameters.Add("@isGST", SqlDbType.Bit).Value = product.isGST;
                command.Parameters.Add("@totalQuantity", SqlDbType.BigInt).Value = product.totalQuantity;
                command.Parameters.Add("@productUnitID", SqlDbType.BigInt).Value = product.productUnitID;
                command.Parameters.Add("@maxLimit", SqlDbType.BigInt).Value = product.maxLimit;
                command.Parameters.Add("@minLimit", SqlDbType.NVarChar).Value = product.minLimit;
                command.Parameters.Add("@subCategoryID", SqlDbType.BigInt).Value = product.subCategoryID;                
                command.Parameters.Add("@categoyID", SqlDbType.BigInt).Value = product.categoyID;
                command.Parameters.Add("@description", SqlDbType.NVarChar).Value = product.description;
                command.Parameters.Add("@image", SqlDbType.NVarChar).Value = product.image;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = product.ipAddress;
                command.Parameters.Add("@addedUserID", SqlDbType.BigInt).Value = product.addedUserID;            
                command.Parameters.Add("@productID", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    product.productID = Convert.ToInt64(command.Parameters["@productID"].Value);
                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return product.productID;
        }

        public IEnumerable<ProductDto> GetStoreProducts(Int64 categoryID, Int64 storeID)
        {
            IList<ProductDto> productList = new List<ProductDto>();

            using (var command = new SqlCommand())
            {
                if (categoryID == null)
                {
                    categoryID = 0;
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetAllProductsByStoreCategory";
                command.Parameters.Add("@categoryID", SqlDbType.BigInt).Value = categoryID;
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;               

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var product = new ProductDto
                            {
                                productID = (Int64)reader["productID"],
                                categoyID = (Int64)reader["categoyID"],
                                productName = !Convert.IsDBNull(reader["productName"]) ? (string)reader["productName"] : "",
                                description = !Convert.IsDBNull(reader["description"]) ? (string)reader["description"] : "",
                                image = !Convert.IsDBNull(reader["image"]) ? (string)reader["image"] : "",
                                isGST = (bool)reader["isGST"],
                                isActive = (bool)reader["isActive"],
                                maxLimit = (Int64)reader["maxLimit"],
                                minLimit = (Int64)reader["minLimit"],
                                price = (decimal)reader["price"],
                                productUnitID = (Int64)reader["productUnitID"],
                                totalQuantity = (Int64)reader["totalQuantity"],                               
                                category = !Convert.IsDBNull(reader["category"]) ? (string)reader["category"] : "",
                                productUnit = !Convert.IsDBNull(reader["productUnit"]) ? (string)reader["productUnit"] : "",
                            };

                            productList.Add(product);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    _connection.Close();
                }
            }

            return productList;
        }

        public ProductDto GetProductDetails(Int64 productID)
        {
            ProductDto product = new ProductDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetProductByID";
                command.Parameters.Add("@productID", SqlDbType.BigInt).Value = productID;              
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            product = new ProductDto
                            {
                                productID = (Int64)reader["productID"],
                                categoyID = (Int64)reader["categoyID"],
                                productName = !Convert.IsDBNull(reader["productName"]) ? (string)reader["productName"] : "",
                                description = !Convert.IsDBNull(reader["description"]) ? (string)reader["description"] : "",
                                image = !Convert.IsDBNull(reader["image"]) ? (string)reader["image"] : "",
                                isGST = (bool)reader["isGST"],
                                isActive = (bool)reader["isActive"],
                                maxLimit = (Int64)reader["maxLimit"],
                                minLimit = (Int64)reader["minLimit"],
                                price = (decimal)reader["price"],
                                productUnitID = (Int64)reader["productUnitID"],
                                totalQuantity = (Int64)reader["totalQuantity"],                               
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return product;
        }

        public Int64 UpdateProduct(ProductDto product)
        {
            Int64 productID = 0;
            using (var command = new SqlCommand())
            {
                if (product.ipAddress == null)
                {
                    product.ipAddress = "";
                }
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_UpdateProduct";
                command.Parameters.Add("@productID", SqlDbType.BigInt).Value = product.productID;
                command.Parameters.Add("@productName", SqlDbType.NVarChar).Value = product.productName;
                command.Parameters.Add("@price", SqlDbType.Money).Value = product.price;
                command.Parameters.Add("@isGST", SqlDbType.Bit).Value = product.isGST;
                command.Parameters.Add("@totalQuantity", SqlDbType.BigInt).Value = product.totalQuantity;
                command.Parameters.Add("@productUnitID", SqlDbType.BigInt).Value = product.productUnitID;
                command.Parameters.Add("@maxLimit", SqlDbType.BigInt).Value = product.maxLimit;
                command.Parameters.Add("@minLimit", SqlDbType.BigInt).Value = product.minLimit;
                command.Parameters.Add("@categoyID", SqlDbType.BigInt).Value = product.categoyID;
                command.Parameters.Add("@description", SqlDbType.NVarChar).Value = product.description;
                command.Parameters.Add("@image", SqlDbType.NVarChar).Value = product.image;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = product.ipAddress;
                command.Parameters.Add("@updatedUserID", SqlDbType.BigInt).Value = product.updatedUserID;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    productID = product.productID;

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return productID;
        }

        public Int64 DeleteProduct(Int64 productID, string ipAddress, Int64 updatedUserID)
        {          
            using (var command = new SqlCommand())
            {
                if (ipAddress == null)
                {
                    ipAddress = "";
                }
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_DeleteProduct";
                command.Parameters.Add("@productID", SqlDbType.BigInt).Value = productID;               
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = ipAddress;
                command.Parameters.Add("@updatedUserID", SqlDbType.BigInt).Value = updatedUserID;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();                 

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return productID;
        }

        public Int64 DeleteCategory(Int64 categoryID, string ipAddress, Int64 updatedUserID)
        {
            using (var command = new SqlCommand())
            {
                if (ipAddress == null)
                {
                    ipAddress = "";
                }
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_DeleteCategory";
                command.Parameters.Add("@categoryID", SqlDbType.BigInt).Value = categoryID;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = ipAddress;
                command.Parameters.Add("@updatedUserID", SqlDbType.BigInt).Value = updatedUserID;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return categoryID;
        }

        public Int64 AddProductUnit(ProductUnitDto productUnit)
        {
            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_AddProductUnit";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = productUnit.storeID;
                command.Parameters.Add("@productUnit", SqlDbType.NVarChar).Value = productUnit.productUnit;
                command.Parameters.Add("@productUnitID", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    productUnit.productUnitID = Convert.ToInt64(command.Parameters["@productUnitID"].Value);
                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return productUnit.productUnitID;
        }

        public IEnumerable<ProductUnitDto> GetStoreProductUnits(Int64 storeID)
        {
            IList<ProductUnitDto> productUnitList = new List<ProductUnitDto>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetAllProductsUnitsByStore";             
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var productUnit = new ProductUnitDto
                            {
                                productUnitID = (Int64)reader["productUnitID"],
                                isActive = (bool)reader["isActive"],
                                productUnit = !Convert.IsDBNull(reader["productUnit"]) ? (string)reader["productUnit"] : "",                              
                            };

                            productUnitList.Add(productUnit);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    _connection.Close();
                }
            }

            return productUnitList;
        }

        public ProductUnitDto GetProductUnitDetails(Int64 productUnitID)
        {
            ProductUnitDto productUnit = new ProductUnitDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetProductUnitByID";
                command.Parameters.Add("@productUnitID", SqlDbType.BigInt).Value = productUnitID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            productUnit = new ProductUnitDto
                            {
                               
                                productUnit = !Convert.IsDBNull(reader["productUnit"]) ? (string)reader["productUnit"] : "",                              
                                isActive = (bool)reader["isActive"],                               
                                productUnitID = (Int64)reader["productUnitID"],
                             
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return productUnit;
        }

        public Int64 UpdateProductUnit(ProductUnitDto productUnit)
        {
            Int64 productUnitID = 0;
            using (var command = new SqlCommand())
            {              
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_UpdateProductUnit";
                command.Parameters.Add("@productUnitID", SqlDbType.BigInt).Value = productUnit.productUnitID;
                command.Parameters.Add("@productUnit", SqlDbType.NVarChar).Value = productUnit.productUnit;              

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    productUnitID = productUnit.productUnitID;

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return productUnitID;
        }

        public Int64 DeleteProductUnit(Int64 productUnitID)
        {
            using (var command = new SqlCommand())
            {               
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_DeleteProductUnit";
                command.Parameters.Add("@productUnitID", SqlDbType.BigInt).Value = productUnitID;            

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return productUnitID;
        }
    }
}
