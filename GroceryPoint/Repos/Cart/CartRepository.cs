﻿using GroceryPoint.Dtos.Cart;
using GroceryPoint.IRepos.Cart;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;

namespace GroceryPoint.Repos.Cart
{
    public class CartRepository: ICartRepository
    {
        private SqlConnection _connection;
        public CartRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public CartSummaryDto AddItemsToCart(CartDto cart)
        {
            CartSummaryDto cartSummary = new CartSummaryDto();

            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {
                if (cart.ipAddress == null)
                {
                    cart.ipAddress = "";
                }
               

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spCart_AddProductToCart";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = cart.storeID;
                command.Parameters.Add("@productID", SqlDbType.BigInt).Value = cart.productID;
                command.Parameters.Add("@qty", SqlDbType.BigInt).Value = cart.qty;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = cart.userID;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = cart.ipAddress;
              
                command.Parameters.Add("@totalItems", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                command.Parameters.Add("@totalCost", SqlDbType.Money).Direction = ParameterDirection.Output;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    Int64 TotalItems = Convert.ToInt64(command.Parameters["@totalItems"].Value);
                    decimal TotalCost = Convert.ToDecimal(command.Parameters["@totalCost"].Value);

                    cartSummary.TotalItems= TotalItems;
                    cartSummary.TotalCost = TotalCost;

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return cartSummary;
        }

        public CartSummaryDto GetCartTotal(Int64 storeID, Int64 userID)
        {
            CartSummaryDto cartSummary = new CartSummaryDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spCart_GetCartTotal";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            cartSummary = new CartSummaryDto
                            {
                                TotalCost = (decimal)reader["totalCost"],
                                TotalItems = (Int64)reader["totalItems"],
                                TotalTax = (decimal)reader["totalTax"],
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }        

            return cartSummary;
        }

        //public IEnumerable<CartDetailsDto> GetCartDetails(Int64 storeID, Int64 userID)
        //{        
        //    IList<CartDetailsDto> cartDetails = new List<CartDetailsDto>();
        //    using (var command = new SqlCommand())
        //    {
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.CommandText = "spCart_GetCartDetails";
        //        command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
        //        command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
        //        command.Connection = _connection;
        //        _connection.Open();
        //        try
        //        {
        //            SqlDataAdapter oDa = new SqlDataAdapter();
        //            DataSet oDs = new DataSet();
        //            oDa = new SqlDataAdapter(command);   // retrieve data from the database
        //            oDa.Fill(oDs);

        //            if (oDs.Tables.Count > 0)
        //            {
        //                if (oDs.Tables[1].Rows.Count > 0)
        //                {
        //                    foreach (DataRow dr in oDs.Tables[1].Rows)
        //                    {
        //                        cartDetails.productID = Convert.ToInt64(dr["productID"]);
        //                        cartDetails.price = Convert.ToDecimal(dr["price"]);
        //                        cartDetails.productName = dr["productName"].ToString();
        //                        cartDetails.qty = Convert.ToInt64(dr["qty"]);
        //                        cartDetails.storeID = Convert.ToInt64(dr["storeID"]);
        //                        cartDetails.subtotal = Convert.ToDecimal(dr["subtotal"]);                                
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //        finally
        //        {                    
        //            _connection.Close();
        //        }
        //    }

        //    return cartDetails;
        //}

        public IEnumerable<CartDetailsDto> GetCartDetails(Int64 storeID, Int64 userID)
        {

            #region Cart Total

            CartSummaryDto cartSummary = new CartSummaryDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spCart_GetCartTotal";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            cartSummary = new CartSummaryDto
                            {
                                TotalCost = (decimal)reader["totalCost"],
                                TotalItems = (Int64)reader["totalItems"],
                                TotalTax = (decimal)reader["totalTax"]
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            #endregion End Cart Total

            #region Store details

            StoreSummaryDto storeSummary = new StoreSummaryDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spStore_GetStoreDetails";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;                
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            storeSummary = new StoreSummaryDto
                            {
                                storeID = (Int64)reader["storeID"],
                                storeName = (string)reader["storeName"],
                                city = (string)reader["city"],
                                textPhone = (string)reader["textPhone"],
                                callPhone = (string)reader["callPhone"],
                                isCall = (bool)reader["isCall"],
                                isText = (bool)reader["isText"],
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            #endregion End Cart Total

            #region Cart Details

            IList<CartDetailsDto> cartList = new List<CartDetailsDto>();
            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spCart_GetCartDetails";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var cartDetails = new CartDetailsDto
                            {
                                cartSummary = cartSummary,
                                storeSummary = storeSummary,
                                productID = (Int64)reader["productID"],
                                userID = (Int64)reader["userID"],
                                productName = !Convert.IsDBNull(reader["productName"]) ? (string)reader["productName"] : "",
                                price = (decimal)reader["price"],
                                qty = (Int64)reader["qty"],
                                subtotal = (decimal)reader["subtotal"],
                                image = !Convert.IsDBNull(reader["image"]) ? (string)reader["image"] : "",
                                cartID = (Int64)reader["cartID"],
                                storeID = (Int64)reader["storeID"],
                                productUnit = !Convert.IsDBNull(reader["productUnit"]) ? (string)reader["productUnit"] : "",
                            };

                            cartList.Add(cartDetails);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            #endregion End Cart details

            return cartList;
        }

        public string DeleteCart(Int64 storeID, Int64 userID)
        {
            string result = "";
            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spCart_DeleteCart";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    result = "Cart Deleted!";
                }
                catch (Exception ex)
                {
                    result = ex.Message.ToString();
                }
                finally
                {

                    _connection.Close();
                }

            }
            return result;
        }

        public string DeleteCartItem(Int64 cartID)
        {
            string result = "";
            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spCart_DeleteCartItem";
                command.Parameters.Add("@cartID", SqlDbType.BigInt).Value = cartID;
               // command.Parameters.Add("@productID", SqlDbType.BigInt).Value = productID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    result = "Cart Item Deleted!";
                }
                catch (Exception ex)
                {
                    result = ex.Message.ToString();
                }
                finally
                {

                    _connection.Close();
                }

            }
            return result;
        }
    }
}
