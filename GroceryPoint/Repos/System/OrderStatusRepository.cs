﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using System.Data.SqlClient;
using System.Data;

namespace GroceryPoint.Repos.System
{
    public class OrderStatusRepository: IOrderStatusRepository
    {
        private SqlConnection _connection;
        public OrderStatusRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public IEnumerable<object> GetAllOrderStatus()
        {
            IList<object> orderstatus = new List<object>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spSystem_GetOrderStatus";
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var orderstatuses = new
                            {
                                Value = (Int64)reader["orderStatusID"],
                                Label = (string)reader["orderStatus"]
                            };

                            orderstatus.Add(orderstatuses);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return orderstatus;
        }
    }
}
