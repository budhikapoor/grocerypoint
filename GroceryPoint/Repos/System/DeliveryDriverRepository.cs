﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using System.Data.SqlClient;
using System.Data;

namespace GroceryPoint.Repos.System
{
    public class DeliveryDriverRepository: IDeliveryDriverRepository
    {
        private SqlConnection _connection;
        public DeliveryDriverRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public Int64 AddDeliveryDriver(DeliveryDriverDto deliveryDriver)
        {
            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {
                if (deliveryDriver.ipAddress == null)
                {
                    deliveryDriver.ipAddress = "";
                }             

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spSystem_AddDeliveryDriver";
                command.Parameters.Add("@firstName", SqlDbType.NVarChar).Value = deliveryDriver.firstName;
                command.Parameters.Add("@lastName", SqlDbType.NVarChar).Value = deliveryDriver.lastName;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = deliveryDriver.phone;
                command.Parameters.Add("@email", SqlDbType.NVarChar).Value = deliveryDriver.email;
                command.Parameters.Add("@dateOfBirth", SqlDbType.SmallDateTime).Value = deliveryDriver.dateOfBirth;
                command.Parameters.Add("@postalCode", SqlDbType.NVarChar).Value = deliveryDriver.postalCode;
                command.Parameters.Add("@provinceID", SqlDbType.BigInt).Value = deliveryDriver.provinceID;
                command.Parameters.Add("@cityID", SqlDbType.BigInt).Value = deliveryDriver.cityID;

                command.Parameters.Add("@isSundayMorning ", SqlDbType.Bit).Value = deliveryDriver.isSundayMorning;
                command.Parameters.Add("@isSundayEvening ", SqlDbType.Bit).Value = deliveryDriver.isSundayEvening;
               
                command.Parameters.Add("@isMondayMorning ", SqlDbType.Bit).Value = deliveryDriver.isMondayMorning;
                command.Parameters.Add("@isMondayAfternoon ", SqlDbType.Bit).Value = deliveryDriver.isMondayAfternoon;
                command.Parameters.Add("@isMondayEvening ", SqlDbType.Bit).Value = deliveryDriver.isMondayEvening;

                command.Parameters.Add("@isTuesdayMorning ", SqlDbType.Bit).Value = deliveryDriver.isTuesdayMorning;
                command.Parameters.Add("@isTuesdayAfternoon ", SqlDbType.Bit).Value = deliveryDriver.isTuesdayAfternoon;
                command.Parameters.Add("@isTuesdayEvening ", SqlDbType.Bit).Value = deliveryDriver.isTuesdayEvening;

                command.Parameters.Add("@isWednesdayMorning ", SqlDbType.Bit).Value = deliveryDriver.isWednesdayMorning;
                command.Parameters.Add("@isWednesdayAfternoon ", SqlDbType.Bit).Value = deliveryDriver.isWednesdayAfternoon;
                command.Parameters.Add("@isWednesdayEvening ", SqlDbType.Bit).Value = deliveryDriver.isWednesdayEvening;

                command.Parameters.Add("@isThursdayMorning ", SqlDbType.Bit).Value = deliveryDriver.isThursdayMorning;
                command.Parameters.Add("@isThursdayAfternoon ", SqlDbType.Bit).Value = deliveryDriver.isThursdayAfternoon;
                command.Parameters.Add("@isThursdayEvening ", SqlDbType.Bit).Value = deliveryDriver.isThursdayEvening;

                command.Parameters.Add("@isFridayMorning ", SqlDbType.Bit).Value = deliveryDriver.isFridayMorning;
                command.Parameters.Add("@isFridayAfternoon ", SqlDbType.Bit).Value = deliveryDriver.isFridayAfternoon;
                command.Parameters.Add("@isFridayEvening ", SqlDbType.Bit).Value = deliveryDriver.isFridayEvening;

                command.Parameters.Add("@isSaturdayMorning ", SqlDbType.Bit).Value = deliveryDriver.isSaturdayMorning;
                command.Parameters.Add("@isSaturdayAfternoon ", SqlDbType.Bit).Value = deliveryDriver.isSaturdayAfternoon;
                command.Parameters.Add("@isSaturdayEvening ", SqlDbType.Bit).Value = deliveryDriver.isSaturdayEvening;


                command.Parameters.Add("@hoursWork ", SqlDbType.Int).Value = deliveryDriver.hoursWork;
                command.Parameters.Add("@driverLicense ", SqlDbType.NVarChar).Value = deliveryDriver.driverLicense;
                command.Parameters.Add("@isLiftWeight  ", SqlDbType.Bit).Value = deliveryDriver.isLiftWeight;
                command.Parameters.Add("@campaignID ", SqlDbType.Int).Value = deliveryDriver.campaignID;
                command.Parameters.Add("@description ", SqlDbType.NVarChar).Value = deliveryDriver.description;

      
                command.Parameters.Add("@ipAddress ", SqlDbType.NVarChar).Value = deliveryDriver.ipAddress;
                command.Parameters.Add("@deliveryDriverID", SqlDbType.BigInt).Direction = ParameterDirection.Output;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    deliveryDriver.deliveryDriverID = Convert.ToInt64(command.Parameters["@deliveryDriverID"].Value);
                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return deliveryDriver.deliveryDriverID;
        }
    }
}
