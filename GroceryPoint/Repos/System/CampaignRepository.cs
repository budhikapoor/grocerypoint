﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using System.Data.SqlClient;
using System.Data;

namespace GroceryPoint.Repos.System
{
    public class CampaignRepository: ICampaignRepository
    {
        private SqlConnection _connection;
        public CampaignRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public IEnumerable<object> GetAllCampaign()
        {
            IList<object> campaign = new List<object>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spSystem_GetCampaign";
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var campaigns = new
                            {
                                Value = (Int64)reader["campaignID"],
                                Label = (string)reader["campaign"]
                            };

                            campaign.Add(campaigns);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return campaign;
        }
    }
}
