﻿using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Repos.System
{
    public class EmailRepository: IEmailRepository
    {
        private SqlConnection _connection;
        public EmailRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public EmailDto GetEmailDetails(Int64 emailTemplateID, Int64 userID, Int64 storeID, Int64 orderID, Int64 userTypeID)
        {
            EmailDto email = new EmailDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spEmail_GetEmailDetails";
                command.Parameters.Add("@emailTemplateID", SqlDbType.Int).Value = emailTemplateID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Parameters.Add("@userTypeID", SqlDbType.Int).Value = userTypeID;            
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            email = new EmailDto
                            {
                                fromEmail = !Convert.IsDBNull(reader["fromEmail"]) ? (string)reader["fromEmail"] : "",
                                toEmail = !Convert.IsDBNull(reader["toEmail"]) ? (string)reader["toEmail"] : "",
                                subject = !Convert.IsDBNull(reader["subject"]) ? (string)reader["subject"] : "",
                                body = !Convert.IsDBNull(reader["body"]) ? (string)reader["body"] : "",
                                displayName = !Convert.IsDBNull(reader["displayName"]) ? (string)reader["displayName"] : "",                              
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return email;
        }
    }
}
