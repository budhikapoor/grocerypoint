﻿using GroceryPoint.Dtos.Order;
using GroceryPoint.IRepos.Order;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace GroceryPoint.Repos.Order
{
    public class OrderRepository: IOrderRepository
    {
        private SqlConnection _connection;
        public OrderRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }

        public Int64 AddOrder(OrderDto order)
        {
            Int64 orderID = 0;

            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {
                if (order.ipAddress == null)
                {
                    order.ipAddress = "";
                }

                if (order.chargeRef == null)
                {
                    order.chargeRef = "";
                }

                if (order.time == null)
                {
                    order.time = "";
                }

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_AddOrder";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = order.storeID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = order.userID;
                command.Parameters.Add("@addressID", SqlDbType.BigInt).Value = order.addressID;
                command.Parameters.Add("@qty", SqlDbType.Int).Value = order.qty;
                command.Parameters.Add("@subtotal", SqlDbType.Money).Value = order.subtotal;
                command.Parameters.Add("@tax", SqlDbType.Money).Value = order.tax;
                command.Parameters.Add("@deliveryCharge", SqlDbType.Money).Value = order.deliveryCharge;
                command.Parameters.Add("@serviceCharge", SqlDbType.Money).Value = order.serviceCharge;
                command.Parameters.Add("@deliveryChargeTax", SqlDbType.Money).Value = order.deliveryChargeTax;
                command.Parameters.Add("@total", SqlDbType.Money).Value = order.total;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = order.ipAddress;
                command.Parameters.Add("@notes", SqlDbType.NVarChar).Value = order.notes;
                command.Parameters.Add("@deliveryDate", SqlDbType.SmallDateTime).Value = order.deliveryDate;
                command.Parameters.Add("@time", SqlDbType.NVarChar).Value = order.time;
                command.Parameters.Add("@orderPhone", SqlDbType.NVarChar).Value = order.orderPhone;
                command.Parameters.Add("@tip", SqlDbType.Money).Value = order.tip;
                command.Parameters.Add("@chargeRef", SqlDbType.NVarChar).Value = order.chargeRef;
                command.Parameters.Add("@cardType", SqlDbType.NVarChar).Value = order.cardType;
                command.Parameters.Add("@last4", SqlDbType.NVarChar).Value = order.last4;
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Direction = ParameterDirection.Output;           

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                   orderID = Convert.ToInt64(command.Parameters["@orderID"].Value);                   

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return orderID;
        }

        public PreOrderDetailsDto GetPreOrderDetails(Int64 storeID, Int64 userID, Int64 addressID)
        {
            PreOrderDetailsDto preOrder = new PreOrderDetailsDto();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_GetPreOrderDetails";
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Parameters.Add("@addressID", SqlDbType.BigInt).Value = addressID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            preOrder = new PreOrderDetailsDto
                            {
                                productSubtotal = (decimal)reader["productSubtotal"],
                                totalItems = (Int64)reader["totalItems"],
                                productTax = (decimal)reader["productTax"],
                                deliveryCharge = (decimal)reader["deliveryCharge"],
                                deliveryChargeTax = (decimal)reader["deliveryChargeTax"],
                                totalWithoutTips = (decimal)reader["totalWithoutTips"],                                
                                
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return preOrder;
        }

        public IEnumerable<object> GetAllDeliveryDates(DateTime todayDateTime)
        {
            IList<object> deliverDate = new List<object>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_GetDeliveryDates";
                command.Parameters.Add("@todayDateTime", SqlDbType.SmallDateTime).Value = todayDateTime;
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var deliverDates = new
                            {
                                Value = (DateTime)reader["date"],
                                Label = (string)reader["dateShow"]
                            };

                            deliverDate.Add(deliverDates);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return deliverDate;
        }

        public IEnumerable<object> GetAllDeliveryTimes(DateTime selectedDate)
        {
            IList<object> deliverTimes = new List<object>();

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_GetDeliveryTimes";
                command.Parameters.Add("@selectedDate", SqlDbType.SmallDateTime).Value = selectedDate;
                command.Connection = _connection;
                _connection.Open();

                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var deliverTime = new
                            {
                                Value = (TimeSpan)reader["time"],
                                Label = (string)reader["timeShow"]
                            };

                            deliverTimes.Add(deliverTime);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            return deliverTimes;
        }

        public IEnumerable<UserOrdersDto> GetUserOrders(Int64 userID)
        {
            IList<UserOrdersDto> orderList = new List<UserOrdersDto>();
            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_GetUserOrders";            
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var orderDetails = new UserOrdersDto
                            {
                                orderID = (Int64)reader["orderID"],
                                orderNumber = !Convert.IsDBNull(reader["orderNumber"]) ? (string)reader["orderNumber"] : "",
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                qty = (Int32)reader["qty"],
                                tax = (decimal)reader["tax"],                              
                                subtotal = (decimal)reader["subtotal"],
                                total = (decimal)reader["total"],
                                orderDate = !Convert.IsDBNull(reader["orderDate"]) ? (string)reader["orderDate"] : "",        orderStatus = !Convert.IsDBNull(reader["orderStatus"]) ? (string)reader["orderStatus"] : "",
                            };

                            orderList.Add(orderDetails);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }
           
            return orderList;
        }

        public UserOrderDetails GetUserOrderDetails(Int64 orderID)
        {
            #region Order Product Details

            IList<UserOrderProductDetails> orderProductList = new List<UserOrderProductDetails>();

            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_GetUserOrderProductDetails";
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var products = new UserOrderProductDetails
                            {
                                productID = (Int64)reader["productID"],
                                userID = (Int64)reader["userID"],
                                productName = !Convert.IsDBNull(reader["productName"]) ? (string)reader["productName"] : "",
                                productUnit = !Convert.IsDBNull(reader["productUnit"]) ? (string)reader["productUnit"] : "",
                                price = (decimal)reader["price"],
                                qty = (Int32)reader["qty"],
                                subtotal = (decimal)reader["subtotal"],                               
                            };

                            orderProductList.Add(products);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;

                }
                finally
                {
                    _connection.Close();
                }
            }

            #endregion End Order Products

            #region Order Details

            UserOrderDetails orderDetails = null;

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spOrder_GetUserOrdersDetails";
                command.Parameters.Add("@orderID", SqlDbType.BigInt).Value = orderID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            orderDetails = new UserOrderDetails
                            {
                                orderID = (Int64)reader["orderID"],
                                orderNumber = !Convert.IsDBNull(reader["orderNumber"]) ? (string)reader["orderNumber"] : "",
                                storeName = !Convert.IsDBNull(reader["storeName"]) ? (string)reader["storeName"] : "",
                                qty = (Int32)reader["qty"],
                                tax = (decimal)reader["tax"],
                                subtotal = (decimal)reader["subtotal"],
                                deliveryCharge = (decimal)reader["deliveryCharge"],
                                deliveryChargeTax = (decimal)reader["deliveryChargeTax"],
                                tip = (decimal)reader["tip"],
                                total = (decimal)reader["total"],
                                orderDate = !Convert.IsDBNull(reader["orderDate"]) ? (string)reader["orderDate"] : "",
                                orderStatus = !Convert.IsDBNull(reader["orderStatus"]) ? (string)reader["orderStatus"] : "",
                                orderPhone = !Convert.IsDBNull(reader["orderPhone"]) ? (string)reader["orderPhone"] : "",
                                deliveryAddress = !Convert.IsDBNull(reader["deliveryAddress"]) ? (string)reader["deliveryAddress"] : "",                               
                                orderProductList = orderProductList
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }

            #endregion End Order Details
            return orderDetails;
        }
    }
}
