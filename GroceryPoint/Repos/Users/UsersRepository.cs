﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using GroceryPoint.Dtos.Users;

using GroceryPoint.IRepos.Users;

namespace GroceryPoint.Repos.Users
{
    public class UsersRepository:IUsersRepository
    {
        private SqlConnection _connection;
        public UsersRepository(string connection)
        {
            _connection = new SqlConnection(connection);
        }
        public Int64 UpdateDefaultStore(Int64 userID, Int64 storeID)
        {
            //Int64 PropertyID = 0;
            using (var command = new SqlCommand())
            {

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_UpdateStore";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Parameters.Add("@storeID", SqlDbType.BigInt).Value = storeID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return storeID;
        }

        public Int64 AddAddress(AddressDto address)
        {
            Int64 addressID = 0;
            using (var command = new SqlCommand())
            {
                if (address.ipAddress == null)
                {
                    address.ipAddress = "";
                }
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_AddAddress";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = address.userID;
                command.Parameters.Add("@streetAddress", SqlDbType.NVarChar).Value = address.streetAddress;
                command.Parameters.Add("@unit", SqlDbType.NVarChar).Value = address.unit;
                command.Parameters.Add("@provinceID", SqlDbType.BigInt).Value = address.provinceID;
                command.Parameters.Add("@cityID", SqlDbType.BigInt).Value = address.cityID;
                command.Parameters.Add("@postalCode", SqlDbType.NVarChar).Value = address.postalCode;
                command.Parameters.Add("@addressTitle", SqlDbType.NVarChar).Value = address.addressTitle;
                command.Parameters.Add("@addressTypeID", SqlDbType.Int).Value = address.addressTypeID;
                command.Parameters.Add("@addressDirections", SqlDbType.NVarChar).Value = address.addressDirections;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = address.ipAddress;
                command.Parameters.Add("@addressID", SqlDbType.BigInt).Direction = ParameterDirection.Output;
               
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    addressID = Convert.ToInt64(command.Parameters["@addressID"].Value);                   

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return addressID;
        }

        public IEnumerable<AddressDetailsDto> GetUserAddresses(Int64 userID, Int64 addressID)
        {  
            IList<AddressDetailsDto> addressList = new List<AddressDetailsDto>();
            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_GetUserAddresses";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Parameters.Add("@addressID", SqlDbType.BigInt).Value = addressID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var addressDetails = new AddressDetailsDto
                            {                                
                                addressID = (Int64)reader["addressID"],
                                userID = (Int64)reader["userID"],
                                isSelected = (bool)reader["isSelected"],
                                addressDirections = !Convert.IsDBNull(reader["addressDirections"]) ? (string)reader["addressDirections"] : "",                               
                                addressType = !Convert.IsDBNull(reader["addressType"]) ? (string)reader["addressType"] : "",  addressTitle = !Convert.IsDBNull(reader["addressTitle"]) ? (string)reader["addressTitle"] : "",
                                city = !Convert.IsDBNull(reader["city"]) ? (string)reader["city"] : "",
                                postalCode = !Convert.IsDBNull(reader["postalCode"]) ? (string)reader["postalCode"] : "",
                                province = !Convert.IsDBNull(reader["provinceAbbr"]) ? (string)reader["provinceAbbr"] : "",
                                streetAddress = !Convert.IsDBNull(reader["streetAddress"]) ? (string)reader["streetAddress"] : "",
                                unit = !Convert.IsDBNull(reader["unit"]) ? (string)reader["unit"] : "",
                            };

                            addressList.Add(addressDetails);
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }


            return addressList;
        }
        public UserDto GetUserProfile(Int64 userID)
        {  
            UserDto user = null;

            using (var command = new SqlCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_GetUserByID";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            user = new UserDto
                            {
                                userID = (Int64)reader["userID"],
                                email = !Convert.IsDBNull(reader["email"]) ? (string)reader["email"] : "",                           
                               userTypeID = (Int32)reader["userTypeID"],
                                userFirstName = !Convert.IsDBNull(reader["userFirstName"]) ? (string)reader["userFirstName"] : "",
                                userLastName = !Convert.IsDBNull(reader["userLastName"]) ? (string)reader["userLastName"] : "",
                                phone = !Convert.IsDBNull(reader["phone"]) ? (string)reader["phone"] : "",
                                phoneShow = !Convert.IsDBNull(reader["phoneShow"]) ? (string)reader["phoneShow"] : "",
                                isEmailVerified = (bool)reader["isEmailVerified"],
                                city = !Convert.IsDBNull(reader["city"]) ? (string)reader["city"] : "",
                                cityID = (Int64)reader["cityID"],
                                storeID = (Int64)reader["storeID"],
                                province = !Convert.IsDBNull(reader["province"]) ? (string)reader["province"] : "",
                                provinceID = (Int64)reader["provinceID"],
                                postalCode = !Convert.IsDBNull(reader["postalCode"]) ? (string)reader["postalCode"] : "",
                            };
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    _connection.Close();
                }
            }
          
            return user;
        }

        public Int64 UpdateUserProfile(UserDto user)
        {
            Int64 userID = 0;
            using (var command = new SqlCommand())
            {
                if (user.ipAddress == null)
                {
                    user.ipAddress = "";
                }
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_UpdateUserProfile";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = user.userID;
                command.Parameters.Add("@email", SqlDbType.NVarChar).Value = user.email;
                command.Parameters.Add("@userFirstName", SqlDbType.NVarChar).Value = user.userFirstName;
                command.Parameters.Add("@userLastName", SqlDbType.NVarChar).Value = user.userLastName;
                command.Parameters.Add("@phone", SqlDbType.NVarChar).Value = user.phone;                
                command.Parameters.Add("@provinceID", SqlDbType.BigInt).Value = user.provinceID;
                command.Parameters.Add("@cityID", SqlDbType.BigInt).Value = user.cityID;
                command.Parameters.Add("@postalCode", SqlDbType.NVarChar).Value = user.postalCode;
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = user.ipAddress;
                
                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    userID = user.userID;

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return userID;
        }

        public Int64 ChangePassword(Int64 userID, byte[] userPasswordHash, byte[] userPasswordSalt, string ipAddress)
        {            
            using (var command = new SqlCommand())
            {
                if (ipAddress == null)
                {
                    ipAddress = "";
                }
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_ChangePassword";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Parameters.Add("@userPasswordHash", SqlDbType.Binary).Value = userPasswordHash;
                command.Parameters.Add("@userPasswordSalt", SqlDbType.Binary).Value = userPasswordSalt;               
                command.Parameters.Add("@ipAddress", SqlDbType.NVarChar).Value = ipAddress;

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();                    

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return userID;
        }

        public string VerifyEmail(Int64 userID, string email)
        {
            string message = "";
            using (var command = new SqlCommand())
            {               
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spUser_VerifyEmail";
                command.Parameters.Add("@userID", SqlDbType.BigInt).Value = userID;
                command.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;             
                command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                //SqlParameter job1 = cmd2.Parameters.Add("@job", SqlDbType.VarChar, 50);

                command.Connection = _connection;
                _connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    //message = command.Parameters["message"].Value.ToString();
                    message = Convert.ToString(command.Parameters["@message"].Value);

                }
                catch (Exception ex)
                {
                }
                finally
                {

                    _connection.Close();
                }

            }
            return message;
        }
    }
}
