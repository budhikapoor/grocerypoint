﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Models
{
    //[JsonObject(MemberSerialization.Fields)]
    public class Category
    {
        public int categoryId { get; set; }
        //[Required(ErrorMessage = "Name is Required")]
        public string categoryName { get; set; }
        public Int64 storeID { get; set; }
        public int addedUserID { get; set; }
        public DateTime ? dateAdded { get; set; }
        public bool isActive { get; set; }
    }
}
