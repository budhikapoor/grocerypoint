﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Models
{
    public class Product
    {
        public int productID { get; set; }
        public string productName { get; set; }
        public int categoyID { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public bool isGST { get; set; }
        public double maxLimit { get; set; }
        public double minLimit { get; set; }

        public double price { get; set; }
        public double productUnitID { get; set; }
        public int subCategoryID { get; set; }
        public double totalQuantity { get; set; }
        public bool isActive { get; set; }
        public int storeID { get; set; }
        public DateTime dateUpdated { get; set; }
       
    }
}
