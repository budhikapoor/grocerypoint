﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Models
{
    public class UserInfo
    {
        public  UserInfo()
        {
            UserId = 0;
            CityId = 0;
           // Current = new UserInfo();
        }
        public static UserInfo Current { get; set; }
      

        public int Id { get; set; }

        public Int64 hearByID { get; set; }
        public int UserTypeID { get; set; }
        public string profilePhoto { get; set; }
        public string facebook { get; set; }
        public string linkedin { get; set; }
        public string phoneShow { get; set; }
        public int i { get; set; }
        public string ext { get; set; }
        public int userGetID { get; set; }
      
        public string Email { get; set; }
        [Required]
        public string FirstName{ get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string Password { get; set; }
        public string RepPassword { get; set; }
        [Required]
        public string Telephone { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int provinceID { get; set; }
        public byte[] UserPasswordHash { get; set; }
        public byte[] UserPasswordSalt { get; set; }
        [Required]
        public string UserName { get; set; }
        public string StoreName { get; set; }
        public Int64 UserId { get; set; }
        public Int64 storeId { get; set; }
        public int campaignID { get; set; }

        public bool isCall { get; set; }
        public bool isText { get; set; }
        public string callPhone { get; set; }

        public string textPhone { get; set; }

    }
}
