﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Models
{
    public class Province
    {
        public int provinceID { get; set; }
        public string provinceAbbr { get; set; }
    }
}
