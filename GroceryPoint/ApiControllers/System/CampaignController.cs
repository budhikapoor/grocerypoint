﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.IServices.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.System
{
    [Route("api")]
    [ApiController]
    public class CampaignController : Controller
    { 
        ICampaignService _campaignService;
        private IConfiguration _configuration;

        public CampaignController(ICampaignService campaignService, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _campaignService = campaignService;
        }


        // GET api/<controller>/5
        [HttpGet("GetAllCampaign")]
        public IActionResult GetAllCampaign()
        {
            return Ok(_campaignService.GetAllCampaign());
        }
       
    }
}
