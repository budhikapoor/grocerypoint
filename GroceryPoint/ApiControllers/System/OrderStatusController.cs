﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.IServices.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.System
{
    [Route("api")]
    [ApiController]
    public class OrderStatusController : Controller
    {
        IOrderStatusService _orderstatusService;
        private IConfiguration _configuration;

        public OrderStatusController(IOrderStatusService orderstatusService, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _orderstatusService = orderstatusService;
        }


        // GET api/<controller>/5
        [HttpGet("GetAllOrderStatus")]
        public IActionResult GetAllOrderStatus()
        {
            return Ok(_orderstatusService.GetAllOrderStatus());
        }
    }
}
