﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.System;
using GroceryPoint.IServices.System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.System
{
    [Route("api")]
    [ApiController]
    public class EmailController : Controller
    {
        IEmailService _emailService;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EmailController(IEmailService emailService, IConfiguration Configuration, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = Configuration;
            _emailService = emailService;
        }

        [HttpGet("GetEmailDetails")]
        public IActionResult GetEmailDetails(Int64 emailTemplateID, Int64 userID, Int64 storeID, Int64 orderID, Int64 userTypeID)
        {
            return Ok(_emailService.GetEmailDetails(emailTemplateID, userID, storeID, orderID, userTypeID));
        }
    }
}
