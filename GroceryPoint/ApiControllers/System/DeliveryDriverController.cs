﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.IServices.System;
using GroceryPoint.Services.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using GroceryPoint.Dtos.System;
using GroceryPoint.IRepos.System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.System
{
    [Route("api")]
    [ApiController]
    public class DeliveryDriverController : Controller
    {
        IDeliveryDriverService _deliveryDriverService;
        private IConfiguration _configuration;

        public DeliveryDriverController(IDeliveryDriverService deliveryDriverService, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _deliveryDriverService = deliveryDriverService;
        }

        [HttpPost("AddDeliveryDriver")]
        public IActionResult AddDeliveryDriver([FromBody]DeliveryDriverDto deliveryDriver)
        {
            Int64 deliveryDriverID = 0;
            try
            {

                //product.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();            
                deliveryDriverID = _deliveryDriverService.AddDeliveryDriver(deliveryDriver);
                return Ok(new { deliveryDriverID = deliveryDriverID });
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
