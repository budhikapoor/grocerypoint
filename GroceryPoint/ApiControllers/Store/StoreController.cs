﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.IServices.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using GroceryPoint.Dtos.Store;
using GroceryPoint.IServices.Store;
using GroceryPoint.IRepos.Store;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.Store
{
    [Route("api")]
    [ApiController]
    public class StoreController : Controller
    {
        IStoreService _storeService;
        private IConfiguration _configuration;

        public StoreController(IStoreService storeService, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _storeService = storeService;
        }


        // GET api/<controller>/5
        [HttpGet("GetAllStoresByCity")]
        public IActionResult GetAllStoresByCity(Int64 cityID)
        {
            return Ok(_storeService.GetAllStoresByCity(cityID));
        }

        [HttpGet("SearchStore")]
        public IActionResult SearchStore(Int64 storeID, string searchPar)
        {
            return Ok(_storeService.SearchStore(storeID, searchPar));
        }

        [HttpGet("GetAllStoreOrders")]
        public IActionResult GetAllStoreOrders(Int64 storeID)
        {
            return Ok(_storeService.GetAllStoreOrders(storeID));
        }

        [HttpGet("GetStoreOrderDetails")]
        public IActionResult GetStoreOrderDetails(Int64 orderID)
        {
            return Ok(_storeService.GetStoreOrderDetails(orderID));
        }

        [HttpGet("UpdateOrderStatus")]
        public IActionResult UpdateOrderStatus(Int64 orderID, Int64 orderStatusID, Int64 userID)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_storeService.UpdateOrderStatus(orderID, orderStatusID, userID));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("AddStore")]
        public IActionResult AddStore([FromBody]StoreDto store, string password)
        {
            Int64 storeID = 0;
            try
            {
                //product.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                storeID = _storeService.AddStore(store, password);
                return Ok(new { storeID = storeID });
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetStoreDetails")]
        public IActionResult GetStoreDetails(Int64 storeID)
        {
            return Ok(_storeService.GetStoreDetails(storeID));
        }

        [HttpGet("DeleteOrderItem")]
        public IActionResult DeleteOrderItem(Int64 orderDetailsID)
        {

            return Ok(_storeService.DeleteOrderItem(orderDetailsID));
        }

        [HttpGet("UpdateOrderItem")]
        public IActionResult UpdateOrderItem(Int64 orderDetailsID, Int64 qty, Int64 userID)
        {

            return Ok(_storeService.UpdateOrderItem(orderDetailsID, qty, userID));
        }

        [HttpGet("GetStoreOrderBasicData")]
        public IActionResult GetStoreOrderBasicData(Int64 orderID)
        {
            return Ok(_storeService.GetStoreOrderBasicData(orderID));
        }
    }
}
