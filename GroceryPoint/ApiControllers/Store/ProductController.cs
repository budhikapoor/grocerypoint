﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;
using GroceryPoint.IServices.Store;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.Store
{
    
    [Route("api")]
    [ApiController]
    public class ProductController : Controller
    {
        IProductService _productService;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductController(IProductService productService, IConfiguration Configuration, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = Configuration;
            _productService = productService;
        }

        [HttpPost("AddProduct")]
        public IActionResult AddProduct([FromBody]ProductDto product)
        {
            Int64 productID = 0;
            try
            {
                //product.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();            
                productID = _productService.AddProduct(product);
                return Ok(new { productID = productID });                
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetStoreProducts")]
        public IActionResult GetStoreProducts(Int64 categoryID, Int64 storeID)
        {
            return Ok(_productService.GetStoreProducts(categoryID, storeID));
        }

        [HttpGet("GetProductDetails")]
        public IActionResult GetProductDetails(Int64 productID)
        {
            return Ok(_productService.GetProductDetails(productID));
        }

        [HttpPut("UpdateProduct")]
        public IActionResult UpdateProduct([FromBody]ProductDto product)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_productService.UpdateProduct(product));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("DeleteProduct")]
        public IActionResult DeleteProduct(Int64 productID, string ipAddress, Int64 updatedUserID)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_productService.DeleteProduct(productID, ipAddress, updatedUserID));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("DeleteCategory")]
        public IActionResult DeleteCategory(Int64 categoryID, string ipAddress, Int64 updatedUserID)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_productService.DeleteCategory(categoryID, ipAddress, updatedUserID));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("AddProductUnit")]
        public IActionResult AddProductUnit([FromBody]ProductUnitDto productUnit)
        {
            Int64 productUnitID = 0;
            try
            {
                productUnitID = _productService.AddProductUnit(productUnit);
                return Ok(new { productUnitID = productUnitID });
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetStoreProductUnits")]
        public IActionResult GetStoreProductUnits(Int64 storeID)
        {
            return Ok(_productService.GetStoreProductUnits(storeID));
        }

        [HttpGet("GetProductUnitDetails")]
        public IActionResult GetProductUnitDetails(Int64 productUnitID)
        {
            return Ok(_productService.GetProductUnitDetails(productUnitID));
        }

        [HttpPut("UpdateProductUnit")]
        public IActionResult UpdateProductUnit([FromBody]ProductUnitDto productUnit)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_productService.UpdateProductUnit(productUnit));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("DeleteProductUnit")]
        public IActionResult DeleteProductUnit(Int64 productUnitID)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_productService.DeleteProductUnit(productUnitID));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
