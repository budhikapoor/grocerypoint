﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Order;
using GroceryPoint.IServices.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.Order
{
    [Route("api")]
    [ApiController]
    public class OrderController : Controller
    {
        IOrderService _orderService;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OrderController(IOrderService orderService, IConfiguration Configuration, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = Configuration;
            _orderService = orderService;
        }

        [HttpPost("AddOrder")]
        public IActionResult AddOrder([FromBody]OrderDto order)
        {
            try
            {
                order.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();                
                return Ok(_orderService.AddOrder(order));
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetPreOrderDetails")]
        public IActionResult GetPreOrderDetails(Int64 storeID, Int64 userID, Int64 addressID)
        {
            return Ok(_orderService.GetPreOrderDetails(storeID, userID, addressID));
        }

        
        [HttpGet("GetAllDeliveryDates")]
        public IActionResult GetAllDeliveryDates(DateTime todayDateTime)
        {
            return Ok(_orderService.GetAllDeliveryDates(todayDateTime));
        }

        [HttpGet("GetAllDeliveryTimes")]
        public IActionResult GetAllDeliveryTimes(DateTime selectedDate)
        {
            return Ok(_orderService.GetAllDeliveryTimes(selectedDate));
        }

        [HttpGet("GetUserOrders")]
        public IActionResult GetUserOrders(Int64 userID)
        {
            return Ok(_orderService.GetUserOrders( userID));
        }

        [HttpGet("GetUserOrderDetails")]
        public IActionResult GetUserOrderDetails(Int64 orderID)
        {
            return Ok(_orderService.GetUserOrderDetails(orderID));
        }
    }
}
