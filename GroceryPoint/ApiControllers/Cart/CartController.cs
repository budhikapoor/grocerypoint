﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Cart;
using GroceryPoint.IServices.Cart;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.Cart
{
    [Route("api")]
    [ApiController]
    public class CartController : Controller
    {
        ICartService _cartService;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartController(ICartService cartService, IConfiguration Configuration, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = Configuration;
            _cartService = cartService;
        }

        [HttpPost("AddItemsToCart")]
        public IActionResult AddItemsToCart([FromBody]CartDto cart)
        {            
            try
            {
                cart.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                //cartSummary = _cartService.AddItemsToCart(cart);
                //return Ok(new { cartSummary = cartSummary });
                return Ok(_cartService.AddItemsToCart(cart));
            }
            catch (Exception ex)
            {
                        
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetCartTotal")]
        public IActionResult GetCartTotal(Int64 storeID, Int64 userID)
        {
            return Ok(_cartService.GetCartTotal(storeID, userID));
        }

        [HttpGet("GetCartDetails")]
        public IActionResult GetCartDetails(Int64 storeID, Int64 userID)
        {
            return Ok(_cartService.GetCartDetails(storeID, userID));
        }

        [HttpGet("DeleteCart")]
        public IActionResult DeleteCart(Int64 storeID, Int64 userID)
        {

            return Ok(_cartService.DeleteCart(storeID, userID));
        }

        [HttpGet("DeleteCartItem")]
        public IActionResult DeleteCartItem(Int64 cartID)
        {

            return Ok(_cartService.DeleteCartItem(cartID));
        }
    }
}
