﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Stripe;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.Cart
{
    [Route("create-payment-intent")]
    [ApiController]
    public class PaymentIntentApiController : Controller
    {

        [HttpPost("CreateCustomer")]        
        public ActionResult CreateCustomer(PaymentIntentCreateRequest request)
        {
            var options = new CustomerCreateOptions
            {
                Description = "Customer 1",
                Name ="cad",
                Email ="test@test.com",
                Phone ="1234567890"                
            };
            var service = new CustomerService();
           var data = service.Create(options);
            return Json(new { data = data });
          
           // return Json(new { clientSecret = paymentIntent.ClientSecret });
        }

        [HttpPost("CreateCard")]
        public ActionResult CreateCard(string token)
        {
            var options = new CardCreateOptions
            {
                Source = token,
            };
            var service = new CardService();
            var data = service.Create("cus_HDnFQ05YlQKkri", options);         
            return Json(new { data = data });

            // return Json(new { clientSecret = paymentIntent.ClientSecret });
        }

        [HttpPost("CreateCharge")]
        public ActionResult CreateCharge()
        {
            // `source` is obtained with Stripe.js; see https://stripe.com/docs/payments/accept-a-payment-charges#web-create-token
            var options = new ChargeCreateOptions
            {
                Amount = 2000,
                Currency = "cad",
                Customer = "cus_HDnFQ05YlQKkri",
                Capture = false,
                Source = "card_1GfLpqBUqJG3bkJEEQZEuP1v",
                Description = "My First Test Charge (created for API docs)",
            };
            var service = new ChargeService();
            var data = service.Create(options);
            return Json(new { data = data });
        }

        [HttpPost("CreateChargeWithoutCustomer")]
        public ActionResult CreateChargeWithoutCustomer(string token, long amount)
        {
            var options = new ChargeCreateOptions
            {
                Amount = amount,
                Currency = "cad",
                Description = "Grocery Point Charge",
                Source = token,
                Capture = false,
            };
            var service = new ChargeService();
            var charge = service.Create(options);
            var chargeToken = charge.Id;
            return Json(new { data = charge });           
        }

        [HttpPost("CaptureCharge")]
        public ActionResult CaptureCharge()
        {
            var options = new ChargeCaptureOptions
            {
                Amount = 1500,
                StatementDescriptor ="Grocery Point",
            };          

            var service = new ChargeService();
            var data = service.Capture("ch_1GfM3RBUqJG3bkJEo36fYeSw", options);
            return Json(new { data = data });
        }


        [HttpDelete("DeleteCard")]
        public ActionResult DeleteCard()
        {
            var service = new CardService();
            var data = service.Delete(
              "cus_HDnFQ05YlQKkri",
              "card_1GfLpqBUqJG3bkJEEQZEuP1v"
            );
            return Json(new { data = data });
        }

        [HttpDelete("DeleteCustomer")]
        public ActionResult DeleteCustomer()
        {
            var service = new CustomerService();
            var data = service.Delete("cus_HDnFQ05YlQKkri");
            return Json(new { data = data });
        }

        [HttpPost("RefundCharge")]
        public ActionResult RefundCharge()
        {
            var options = new RefundCreateOptions
            {
                Charge = "ch_1Gfh2yBUqJG3bkJEchjFLkDu",
                Amount = 700,               
            };
            var service = new RefundService();
            var data =  service.Create(options);
            return Json(new { data = data });
        }

        [HttpPost]
        public ActionResult Create(PaymentIntentCreateRequest request)
        {
            var paymentIntents = new PaymentIntentService();
            var paymentIntent = paymentIntents.Create(new PaymentIntentCreateOptions
            {
                Amount = CalculateOrderAmount(request.Items),
                Currency = "cad",
            });

            return Json(new { clientSecret = paymentIntent.ClientSecret });
        }


        [HttpGet("Authorize")]
        public ActionResult AuthorizeCard(string token, string amount)
        {
            amount = amount.Replace("$", "");
            decimal holdAmount = (Convert.ToDecimal(amount)* 0.10m) + Convert.ToDecimal(amount);
            holdAmount =  decimal.Round(holdAmount, 2, MidpointRounding.AwayFromZero);

            amount = holdAmount.ToString().Replace(".","");
            Int64 finalAmount = Convert.ToInt64(amount);
            var options = new ChargeCreateOptions
            {
                Amount = finalAmount,
                Currency = "cad",
                Description = "Grocery Point Charge",
                Source = token,
                Capture = false,
                StatementDescriptor ="Grocery Point"
            };
            var service = new ChargeService();
            try
            {
                var charge = service.Create(options);                
                var chargeToken = charge.Id;
                var cardType = charge.PaymentMethodDetails.Card.Brand;
                var last4 = charge.PaymentMethodDetails.Card.Last4;
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { data = charge.Id, cardType, last4 });
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { message = ex.Message });
            }

        }
        private int CalculateOrderAmount(Item[] items)
        {
            // Replace this constant with a calculation of the order's amount
            // Calculate the order total on the server to prevent
            // people from directly manipulating the amount on the client
            return 1500;
        }

        public class Item
        {
            [JsonProperty("id")]
            public string Id { get; set; }
        }

        public class PaymentIntentCreateRequest
        {
            [JsonProperty("items")]
            public Item[] Items { get; set; }
        }

    }
}
