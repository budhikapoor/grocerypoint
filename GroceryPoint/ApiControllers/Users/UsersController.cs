﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.IServices.System;
using GroceryPoint.IServices.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using GroceryPoint.Dtos.Users;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GroceryPoint.ApiControllers.Users
{
    [Route("api")]
    [ApiController]
    public class UsersController : Controller
    {
        IUsersService _usersService;
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UsersController(IUsersService usersService, IConfiguration Configuration)
        {
            _configuration = Configuration;
            _usersService = usersService;
        }

        [HttpGet("UpdateDefaultStore")]
        public IActionResult UpdateDefaultStore(Int64 userID, Int64 storeID)
        {
            try
            {
                return Ok(_usersService.UpdateDefaultStore(userID, storeID));
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }            
        }

        [HttpPost("AddAddress")]
        public IActionResult AddAddress([FromBody]AddressDto address)
        {
            try
            {
                //address.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();            
                return Ok(_usersService.AddAddress(address));
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetUserAddresses")]
        public IActionResult GetUserAddresses(Int64 userID, Int64 addressID)
        {
            return Ok(_usersService.GetUserAddresses(userID, addressID));
        }

        [HttpGet("GetUserProfile")]
        public IActionResult GetUserProfile(Int64 userID)
        {
            return Ok(_usersService.GetUserProfile(userID));
        }

        [HttpPut("UpdateUserProfile")]
        public IActionResult UpdateUserProfile([FromBody]UserDto user)
        {
            try
            {
               // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_usersService.UpdateUserProfile(user));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("ChangePassword")]
        public IActionResult ChangePassword(Int64 userID, string password, string ipAddress)
        {
            try
            {
                // user.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();               
                return Ok(_usersService.ChangePassword(userID, password, ipAddress));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("VerifyEmail")]
        public IActionResult VerifyEmail(Int64 userID, string email)
        {
            try
            {
                //address.ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();            
                return Ok(_usersService.VerifyEmail(userID, email));
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
