﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class BaseController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var res = HttpContext.Session.GetString("userSession");
            if (res != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));

                if (user.CityId == 0)
                {
                    filterContext.Result = new RedirectResult(Url.Action("getstart", "Accounts"));

                }
                else if (user.UserId == 0)
                    filterContext.Result = new RedirectResult(Url.Action("Login", "Accounts"));
                //else
                //    UserInfo.Current = user;
            }
            else
            {
                filterContext.Result = new RedirectResult(Url.Action("getstart", "Accounts"));

            }

        }
    }
}