﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class CategoryController : StoreBaseController
    {
        private readonly IConfiguration _configuration;

        private string ApiGlobalUrl;

        public CategoryController(IConfiguration configuration)
        {
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public async Task<IActionResult> Index()
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;
            string apiUrl = ApiGlobalUrl + "api/CategoryList?storeid=" + user.storeId;
            List<Category> CategoryList = new List<Category>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CategoryList = JsonConvert.DeserializeObject<List<Category>>(apiResponse);
                }
            }

            return View(CategoryList);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Category categoryobj)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string apiUrl = ApiGlobalUrl + "api/addCategory";

            Category category = new Category();
            category.dateAdded = DateTime.Now;
            category.categoryName = categoryobj.categoryName;
            category.storeID = user.storeId;
            using (var httpClient = new HttpClient())
            {
                var res = JsonConvert.SerializeObject(categoryobj);
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse !=null)
                    {
                        TempData["success"] = "Added Successfully";

                        return RedirectToAction("Index", "Category");
                    }
                    categoryobj = JsonConvert.DeserializeObject<Category>(apiResponse);
                }
            }
            return View();
        }

        public async Task<IActionResult> Edit(int id)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;
            string apiUrl = ApiGlobalUrl + "api/CategoryList?storeid=" + user.storeId;
            List<Category> CategoryList = new List<Category>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CategoryList = JsonConvert.DeserializeObject<List<Category>>(apiResponse);
                }
            }
            Category category = new Category();
            var res = CategoryList.Where(c => c.categoryId == id).FirstOrDefault();
            return View(res);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int id, Category categoryobj)
        {
            string apiUrl = ApiGlobalUrl + "api/updateCategory";

            Category category = new Category();
            category.dateAdded = DateTime.Now;
            category.categoryName = categoryobj.categoryName;
            category.categoryId = categoryobj.categoryId;
            using (var httpClient = new HttpClient())
            {
                var res = JsonConvert.SerializeObject(categoryobj);
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse == "true")
                    {
                        TempData["success"] = "Update Successfully";
                        return RedirectToAction("Index", "Category");
                    }
                    categoryobj = JsonConvert.DeserializeObject<Category>(apiResponse);
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            string apiUrl = ApiGlobalUrl + "api/DeleteCategory?categoryID=" + id+ "&ipAddress=&updatedUserID="+ 1;
            using (var httpClient = new HttpClient())
            {

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse !=null)
                    {
                        TempData["success"] = "Delete Successfully";
                        return Json(true);
                    }
                }
            }
            return Json(false);
        }
    }
}