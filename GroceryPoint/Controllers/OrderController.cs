﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Order;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class OrderController : StoreBaseController
    {
        private readonly IConfiguration _configuration;
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;
        private string ApiGlobalUrl;

        [Obsolete]
        public OrderController(IConfiguration configuration, IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public async Task<IActionResult> Index()
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;

            string apiUrl = ApiGlobalUrl + "api/GetAllStoreOrders?storeID=" + user.storeId;
            List<StoreOrderDto> StoreOrderList = new List<StoreOrderDto>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    StoreOrderList = JsonConvert.DeserializeObject<List<StoreOrderDto>>(apiResponse);
                }
            }

            return View(StoreOrderList);
        }
        public async Task<IActionResult> OrderDetail(int? id)
        {
            ViewBag.Id = id;
            return View();
        }
        public async Task<IActionResult> OrderDetailView(int? id)
        {

            string apiUrl = ApiGlobalUrl + "api/GetStoreOrderDetails?orderID=" + id;

            StoreOrderDetailsDto storeOrderDetailsDtoObj = new StoreOrderDetailsDto();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync(apiUrl))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        storeOrderDetailsDtoObj = JsonConvert.DeserializeObject<StoreOrderDetailsDto>(apiResponse);
                        ViewBag.OrderStatusId = new SelectList(await OrderStatusList(), "value", "label",
                            storeOrderDetailsDtoObj.StatusId);

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return PartialView("_OrderDetailViewPrtial",storeOrderDetailsDtoObj);
        }
        public async Task<List<OrderStatusDto>> OrderStatusList()
        {
            string apiUrl = ApiGlobalUrl + "api/GetAllOrderStatus";

            List<OrderStatusDto> OrderStatusDtoList = new List<OrderStatusDto>();

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    OrderStatusDtoList = JsonConvert.DeserializeObject<List<OrderStatusDto>>(apiResponse);
                }
            }
            return OrderStatusDtoList;
        }
        public async Task<JsonResult> UpdateOrderStatus(int orderID, int orderStatusID)
        {
            string apiUrl = ApiGlobalUrl + "api/UpdateOrderStatus?orderID=" + orderID + "&orderStatusID=" + orderStatusID + "&userID=1";
            List<StoreOrderDto> ProductList = new List<StoreOrderDto>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Json(apiResponse);

                }
            }

            return Json(true);
        }
        public async Task<JsonResult> UpdateOrderItem(Int64? orderDetailsID, int qty)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            }
            else
            {
                user.UserId = 0;
            }

            string apiUrl = ApiGlobalUrl + "api/UpdateOrderItem?orderDetailsID=" + orderDetailsID + "&qty=" + qty + "&userID" + user.UserId;
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    //string StorName = Getuser.StoreName.Trim();
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Json(apiResponse);
                }
            }
        }
        public IActionResult CheckBox()
        {
            return View();
        }

    }
}