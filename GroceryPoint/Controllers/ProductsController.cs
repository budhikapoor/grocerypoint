﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class ProductsController : StoreBaseController
    {
        private readonly IConfiguration _configuration;
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;
        private string ApiGlobalUrl;

        [Obsolete]
        public ProductsController(IConfiguration configuration, IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public async Task<IActionResult> Index()
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;
            string apiUrl = ApiGlobalUrl + "api/GetStoreProducts?categoryID=" + 0 + "&storeID=" + user.storeId;
            List<ProductDto> ProductList = new List<ProductDto>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProductList = JsonConvert.DeserializeObject<List<ProductDto>>(apiResponse);
                }
            }

            return View(ProductList);
        }
        public async Task<IActionResult> Create()
        {

            ViewBag.categoyID = new SelectList(await CategoryList(), "categoryId", "categoryName");

            ViewBag.productUnitID = new SelectList(await ProductUnitDtoList(), "productUnitID", "productUnit"); return View();
        }
        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> Create(ProductDto productDto, IFormFile file)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            string FileNameForDb="";
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
                ViewBag.storeName = user.StoreName.Trim();
                ViewBag.CityName = user.CityName;
            }
           
            string StorName = user.StoreName.Trim();
            string path_Root = _appEnvironment.WebRootPath;
            if (file != null)
            {
                string filename = file.FileName;
                string filepath = path_Root + "//ProductImages";
                FileNameForDb = Guid.NewGuid() + file.FileName;

                if (!(Directory.Exists(path_Root + "//ProductImages//" + StorName)))
                {
                    Directory.CreateDirectory(path_Root + "//ProductImages//" + StorName);
                }

                string saveImg = path_Root + "\\ProductImages\\" + StorName + "\\" + FileNameForDb;
                using (var stream = new FileStream(saveImg, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            string apiUrl = ApiGlobalUrl + "api/AddProduct";

            ProductDto _productDto = new ProductDto();
            _productDto.productID = productDto.productID;
            _productDto.productName = productDto.productName;
            _productDto.storeID = user.storeId;
            _productDto.price = productDto.price;
            _productDto.productUnitID = productDto.productUnitID;
            _productDto.isGST = productDto.isGST;
            _productDto.totalQuantity = productDto.totalQuantity;
            _productDto.maxLimit = productDto.maxLimit;
            _productDto.minLimit = productDto.minLimit;
            _productDto.subCategoryID = 1;
            _productDto.categoyID = productDto.categoyID;
            _productDto.image = FileNameForDb;
            _productDto.ipAddress = "1";
            _productDto.addedUserID = 1;
            _productDto.description = productDto.description;

            _productDto.productID = productDto.productID;



            using (var httpClient = new HttpClient())
            {
                var res = JsonConvert.SerializeObject(_productDto);
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(_productDto), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != null)
                    {
                        TempData["success"] = "Added Successfully";

                        return RedirectToAction("Index", "Products");
                    }
                    //_productDto = JsonConvert.DeserializeObject<ProductUnitDto>(apiResponse);
                }
            }


            ViewBag.FileNameForDb = FileNameForDb;

            ViewBag.categoyID = new SelectList(await CategoryList(), "categoryId", "categoryName");
            ViewBag.productUnitID = new SelectList(await ProductUnitDtoList(), "productUnitID", "productUnit");

            return View();
        }

        public async Task<IActionResult> Edit(int id)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
                ViewBag.storeName = user.StoreName.Trim();
                ViewBag.CityName = user.CityName;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;

            ProductDto productDto = new ProductDto();
            string apiUrl = ApiGlobalUrl + "api/GetProductDetails?productID=" + id;
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    productDto = JsonConvert.DeserializeObject<ProductDto>(apiResponse);
                }
                ViewBag.categoyID = new SelectList(await CategoryList(), "categoryId", "categoryName", productDto.categoyID);
                ViewBag.productUnitID = new SelectList(await ProductUnitDtoList(), "productUnitID", "productUnit", productDto.productUnitID);
                return View(productDto);
            }
        }
        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> Edit(int id, ProductDto productDto, IFormFile file)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
                ViewBag.storeName = user.StoreName.Trim();
                ViewBag.CityName = user.CityName;
            }

            string StorName = user.StoreName.Trim();
            ProductDto _productDto = new ProductDto();
            if (file == null)
            {
                _productDto.image = productDto.image;
                _productDto.productID = productDto.productID;
                _productDto.productName = productDto.productName;
                _productDto.storeID = 1;
                _productDto.price = productDto.price;
                _productDto.productUnitID = productDto.productUnitID;
                _productDto.isGST = productDto.isGST;
                _productDto.totalQuantity = productDto.totalQuantity;
                _productDto.maxLimit = productDto.maxLimit;
                _productDto.minLimit = productDto.minLimit;
                _productDto.subCategoryID = 1;
                _productDto.categoyID = productDto.categoyID;
                _productDto.ipAddress = "1";
                _productDto.addedUserID = 1;
                _productDto.productID = productDto.productID;
                _productDto.description = productDto.description;
                string apiUrl = ApiGlobalUrl + "api/UpdateProduct";

                using (var httpClient = new HttpClient())
                {
                    var res = JsonConvert.SerializeObject(_productDto);
                    StringContent contentData = new StringContent(JsonConvert.SerializeObject(_productDto), Encoding.UTF8, "application/json");

                    using (var response = await httpClient.PutAsync(apiUrl, contentData))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        if (apiResponse != null)
                        {
                            TempData["success"] = "Updated Successfully";

                            return RedirectToAction("Index", "Products");
                        }
                        //_productDto = JsonConvert.DeserializeObject<ProductUnitDto>(apiResponse);
                    }
                }
            }
            else
            {

                string path_Root = _appEnvironment.WebRootPath;
                string filename = file.FileName;
                string filepath = path_Root + "//ProductImages";
                string FileNameForDb = Guid.NewGuid() + file.FileName;

                if (!(Directory.Exists(path_Root + "//ProductImages//" + StorName)))
                {
                    Directory.CreateDirectory(path_Root + "//ProductImages//" + StorName);
                }

                string saveImg = path_Root + "\\ProductImages\\" + StorName + "\\" + FileNameForDb;
                using (var stream = new FileStream(saveImg, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                _productDto.productID = productDto.productID;
                _productDto.productName = productDto.productName;
                _productDto.storeID = 1;
                _productDto.price = productDto.price;
                _productDto.productUnitID = productDto.productUnitID;
                _productDto.isGST = productDto.isGST;
                _productDto.totalQuantity = productDto.totalQuantity;
                _productDto.maxLimit = productDto.maxLimit;
                _productDto.minLimit = productDto.minLimit;
                _productDto.subCategoryID = 1;
                _productDto.categoyID = productDto.categoyID;
                _productDto.image = FileNameForDb;
                _productDto.ipAddress = "1";
                _productDto.addedUserID = 1;
                _productDto.productID = productDto.productID;
                _productDto.description = productDto.description;

                string apiUrl = ApiGlobalUrl + "api/UpdateProduct";

                using (var httpClient = new HttpClient())
                {
                    var res = JsonConvert.SerializeObject(_productDto);
                    StringContent contentData = new StringContent(JsonConvert.SerializeObject(_productDto), Encoding.UTF8, "application/json");

                    using (var response = await httpClient.PutAsync(apiUrl, contentData))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        if (apiResponse != null)
                        {
                            TempData["success"] = "Updated Successfully";

                            return RedirectToAction("Index", "Products");
                        }
                        //_productDto = JsonConvert.DeserializeObject<ProductUnitDto>(apiResponse);
                    }
                }
            }
            ViewBag.categoyID = new SelectList(await CategoryList(), "categoryId", "categoryName");
            ViewBag.productUnitID = new SelectList(await ProductUnitDtoList(), "productUnitID", "productUnit");
            return View();

        }
        public async Task<List<Category>> CategoryList()
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));

            }

            string StorName = user.StoreName.Trim();
            List<Category> CategoryList = new List<Category>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/CategoryList?storeid=" + user.storeId;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CategoryList = JsonConvert.DeserializeObject<List<Category>>(apiResponse);
                }
            }
            return CategoryList;
        }
        public async Task<List<ProductUnitDto>> ProductUnitDtoList()
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;

            }

            string StorName = user.StoreName.Trim();
            string apiUrl = ApiGlobalUrl + "api/GetStoreProductUnits?storeid=" + user.storeId;
            List<ProductUnitDto> ProductUnitDtoList = new List<ProductUnitDto>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProductUnitDtoList = JsonConvert.DeserializeObject<List<ProductUnitDto>>(apiResponse);
                }
            }
            return ProductUnitDtoList;
        }
        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            string apiUrl = ApiGlobalUrl + "api/DeleteProduct?productID=" + id + "&ipAddress=&updatedUserID=" + 1;
            using (var httpClient = new HttpClient())
            {

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != null)
                    {
                        TempData["success"] = "Delete Successfully";
                        return Json(true);
                    }
                }
            }
            return Json(false);
        }



    }
}