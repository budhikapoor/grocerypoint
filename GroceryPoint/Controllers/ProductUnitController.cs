﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class ProductUnitController : Controller
    {
        private readonly IConfiguration _configuration;

        private string ApiGlobalUrl;
        public ProductUnitController(IConfiguration configuration)
        {
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public async Task<IActionResult> Index()
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;

            string apiUrl = ApiGlobalUrl + "api/GetStoreProductUnits?storeid=" + user.storeId;
            List<ProductUnitDto> ProductUnitDtoList = new List<ProductUnitDto>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProductUnitDtoList = JsonConvert.DeserializeObject<List<ProductUnitDto>>(apiResponse);
                }
            }

            return View(ProductUnitDtoList);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(ProductUnitDto productUnitDto)
        {
            UserInfo user = new UserInfo();
            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
            }

            string StorName = user.StoreName.Trim();
            ViewBag.StorName = StorName;
            string apiUrl = ApiGlobalUrl + "api/AddProductUnit";

            ProductUnitDto productUnitObj= new ProductUnitDto();
            productUnitObj.sequence = productUnitDto.sequence;
            productUnitObj.storeID = user.storeId;
            productUnitObj.productUnit = productUnitDto.productUnit;
            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(productUnitObj), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse !="")
                    {
                        return RedirectToAction("Index", "ProductUnit");
                    }
                   
                }
            }
            return View();
        }


        public async Task<IActionResult> Edit(int id)
        {
           ProductUnitDto productUnitDto = new ProductUnitDto();
             string apiUrl = ApiGlobalUrl + "api/GetProductUnitDetails?productUnitID=" + id;
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    productUnitDto = JsonConvert.DeserializeObject< ProductUnitDto > (apiResponse);
            }
            
            return View(productUnitDto);
        }            }
    
        [HttpPost]
        public async Task<IActionResult> Edit(int id, ProductUnitDto productUnits)
        {
            string apiUrl = ApiGlobalUrl + "api/UpdateProductUnit";

            ProductUnitDto productUnitDtoObj = new ProductUnitDto();
            productUnitDtoObj.productUnitID = productUnits.productUnitID;
            productUnitDtoObj.storeID = productUnits.storeID;
            productUnitDtoObj.productUnit = productUnits.productUnit;
           
            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(productUnitDtoObj), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PutAsync(apiUrl, contentData))
                {
                    int ? apiResponse = Convert.ToInt32(await response.Content.ReadAsStringAsync());
                    
                    if (apiResponse !=0 || apiResponse!=null)
                    {
                        TempData["success"] = "Update Successfully";
                        return RedirectToAction("Index", "productUnit");
                    }
                    //productUnitDtoObj = JsonConvert.DeserializeObject<ProductUnitDto>(apiResponse);
                }
            }
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            string apiUrl = ApiGlobalUrl + "api/DeleteProductUnit?productUnitID=" + id ;
            using (var httpClient = new HttpClient())
            {

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != null)
                    {
                        TempData["success"] = "Delete Successfully";
                        return Json(true);
                    }
                }
            }
            return Json(false);
        }
    }
}