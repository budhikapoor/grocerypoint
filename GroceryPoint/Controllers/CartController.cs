﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Cart;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using GroceryPoint.Models;

namespace GroceryPoint.Controllers
{
    public class CartController : BaseController
    {
        private readonly IConfiguration _configuration;

        private string ApiGlobalUrl;
        public CartController(IConfiguration configuration)
        {
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        [HttpPost]
        public async Task<JsonResult> addToCart(CartDto cart)
        {
            CartDto cartDtoObj = new CartDto();
            UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));

            cartDtoObj.addedUserID = cart.addedUserID;
            cartDtoObj.productID = cart.productID;
            cartDtoObj.qty = cart.qty;
            cartDtoObj.userID = user.UserId;
            cartDtoObj.storeID = user.storeId;
            cartDtoObj.ipAddress = cart.ipAddress;
            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(cartDtoObj), Encoding.UTF8, "application/json");
                string apiUrl = ApiGlobalUrl + "api/AddItemsToCart";
                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    cartDtoObj = null;
                    JObject json = JObject.Parse(apiResponse);
                    return Json(json);
                }
            }
        }
      
        public async Task<IActionResult> GetCartDetails(Int64 storeID, Int64 userID)
        {
            var res = HttpContext.Session.GetString("userSession");
            if (res != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.user1d = user.UserId;
            }
          
            List<cart> CartList = new List<cart>();
           
            UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetCartDetails?storeID=" + Getuser.storeId + "&userID=" + Getuser.UserId;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string StorName = Getuser.StoreName.Trim();
                    ViewBag.StorName = StorName;
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CartList = JsonConvert.DeserializeObject<List<cart>>(apiResponse);

                    return PartialView("_CartDetail", CartList);
                }
            }
        }
        public async Task<JsonResult> GetCartTotal(Int64 storeID, Int64 userID)
        {
            
            using (var httpClient = new HttpClient())
            {
            UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                string apiUrl = ApiGlobalUrl + "api/GetCartTotal?storeID=" + Getuser.storeId + "&userID=" + Getuser.UserId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                   
                    JObject json = JObject.Parse(apiResponse);
                    return Json(json);
                }
            }
        }
        public async Task<IActionResult> catdeatail()
        {
            var res = HttpContext.Session.GetString("userSession");
            if (res != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.user1d = user.UserId;
                string StorName = user.StoreName.Trim();
                ViewBag.StorName = StorName;
            }

            List<cart> CartList = new List<cart>();
            //if (storeID == 0)
            //{
            //    storeID = 1;
            //}
            //if (userID == 0)
            //{
            //    userID = 1;
            //}
            UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetCartDetails?storeID=" + Getuser.storeId + "&userID=" + Getuser.UserId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CartList = JsonConvert.DeserializeObject<List<cart>>(apiResponse);

                    return PartialView("_CartDetailCheckout", CartList);
                }
            }
        } 
        public async Task<JsonResult> EmptyCart()
        {

            using (var httpClient = new HttpClient())
            {
                UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                string apiUrl = ApiGlobalUrl + "api/DeleteCart?storeID=" + Getuser.storeId + "&userID=" + Getuser.UserId;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string StorName = Getuser.StoreName.Trim();

                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Json(apiResponse);
                }
            }
        }
        public async Task<JsonResult> deleteCart(int?cartID)
        {

            string apiUrl = ApiGlobalUrl + "api/DeleteCartItem?cartID=" + cartID;

            using (var httpClient = new HttpClient())
            {
                UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string StorName = Getuser.StoreName.Trim();

                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Json(apiResponse);
                }
            }
        }
    }
}