﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Global;
using Microsoft.AspNetCore.Mvc;

namespace GroceryPoint.Controllers
{
    public class PageController : Controller
    {
        public IActionResult Faq()
        {
            return View();
        }
        public IActionResult privacy()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
        public IActionResult Contact()        {
           
            return View();
        }
        [HttpPost]
        public IActionResult ContactSubmit(string name, string email, string phone, string message)
        {
            string fromEmail = "no-reply@grocerypoint.ca";
            string displayName = name;
            string toEmail = "grocerypoint2020@gmail.com";
            string subject = "Grocery Point - Contact Us";

            string body = @"Hi there, <br/>";
            body  = body + "Somebody just sent a message in Contact Us page <br/><br/>";
            body = body + "Name: " + name + "<br/>";
            body = body + "Phone: " + phone + "<br/>";
            body = body + "Email: " + email + "<br/>";
            body = body + "Message: " + message + "<br/>";
            EmailSendGrid.SendEmail(fromEmail, displayName, toEmail, subject, "", body);
            TempData["success"] = "Thanks for Contacting us, we will get back to you soon.";
            return RedirectToAction("Contact", "Page");            
        }

        public IActionResult Howitworks()
        {
            return View();
        }
        public IActionResult TermsandCondition()
        {
            return View();
        }
    }
}