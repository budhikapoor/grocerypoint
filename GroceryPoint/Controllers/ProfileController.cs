﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.DateDto;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Dtos.Order;
using GroceryPoint.Dtos.Users;
using GroceryPoint.Global;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GroceryPoint.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IConfiguration _configuration;
        private string ApiGlobalUrl;

        public ProfileController(IConfiguration configuration)
        {
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public async Task<IActionResult> Index()
        {

            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            UserDto UserDtoObj = new UserDto();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetUserProfile?userID=" + Getuser.UserId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    UserDtoObj = JsonConvert.DeserializeObject<UserDto>(apiResponse);
                }
            }


            return View(UserDtoObj);
        }
        public IActionResult ChangePassword()
        {
            return View();
        }
        
        public async Task<IActionResult> ChangeUserPassword(Int64 userID, string password, string ipAddress)
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/ChangePassword?userID=" + Getuser.UserId + "&password=" + password + "&ipAddress" + ipAddress;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                }
            }
            return Json(true);
        }

        [HttpGet]
        public async Task<JsonResult> orderTexDetail()
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetPreOrderDetails?storeID=" + Getuser.storeId + "&userID=" + Getuser.UserId + "&addressID=" + 1;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();

                    JObject json = JObject.Parse(apiResponse);
                    return Json(json);
                }
            }
        }

        public async Task<IActionResult> EditProfile()
        {
            UserDto UserDtoObj = new UserDto();
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
      

            }
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetUserProfile?userID=" + Getuser.UserId;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    UserDtoObj = JsonConvert.DeserializeObject<UserDto>(apiResponse);
                }
            }
            //List<City> CityList = new List<City>();
            //using (var httpClient = new HttpClient())
            //{
            //    using (var response = await httpClient.GetAsync("https://localhost:44330/api/CityList"))
            //    {
            //        string apiResponse = await response.Content.ReadAsStringAsync();
            //        CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
            //    }
            //}
            ViewBag.cityID = new SelectList(await CityList(), "CityId", "CityName", UserDtoObj.cityID);
            ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr", UserDtoObj.provinceID);
            return View(UserDtoObj);
        }
        [HttpPost]
        public async Task<IActionResult> EditProfile(UserDto userDto)
        {
            UserDto UserDtoObj = new UserDto();
            UserDtoObj.userID = userDto.userID;
            UserDtoObj.email = userDto.email;
            UserDtoObj.userFirstName = userDto.userFirstName;
            UserDtoObj.userLastName = userDto.userLastName;
            UserDtoObj.phone = userDto.phone;
            UserDtoObj.cityID = userDto.cityID;
            UserDtoObj.storeID = userDto.storeID;
            UserDtoObj.provinceID = userDto.provinceID;
            if (userDto.postalCode == null || userDto.postalCode=="")
            {
                UserDtoObj.postalCode = "P";
            }
            else
            {
                UserDtoObj.postalCode = userDto.postalCode;
            }

            UserDtoObj.ipAddress = userDto.ipAddress;

            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(UserDtoObj), Encoding.UTF8, "application/json");
                string apiUrl = ApiGlobalUrl + "api/UpdateUserProfile";

                using (var response = await httpClient.PutAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ViewBag.cityID = new SelectList(await CityList(), "CityId", "CityName", UserDtoObj.cityID);
                    ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr", UserDtoObj.provinceID);

                    ViewBag.success = "done";

                    return View();
                }
            }
        }
        public async Task<List<City>> CityList()
        {
            List<City> CityList = new List<City>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/CityList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            return CityList;
        }
        public async Task<List<Province>> ProvinceList()
        {
            List<Province> ProvinceList = new List<Province>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/provinceList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProvinceList = JsonConvert.DeserializeObject<List<Province>>(apiResponse);
                }
            }
            return ProvinceList;
        }
        public async Task<IActionResult> OrderDetail()
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            List<UserOrdersDto> UserOrdersDto = new List<UserOrdersDto>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetUserOrders?userID=" + Getuser.UserId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    UserOrdersDto = JsonConvert.DeserializeObject<List<UserOrdersDto>>(apiResponse);
                }
            }
            return View(UserOrdersDto);
        }
        public IActionResult Order()
        {
            return View();
        }
        public IActionResult cart()
        {
            return View();
        }
        public async Task<IActionResult> Checkout()
        {


            var result = HttpContext.Session.GetString("userSession");
            if (result != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                var cityId = user.CityId;
                ViewBag.storeName = user.StoreName;
                ViewBag.CityName = user.CityName;
            }
            List<City> CityList = new List<City>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/CityList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            ViewBag.CityId = new SelectList(CityList, "CityId", "CityName");
            ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr");
            ViewBag.dateid = new SelectList(await DateDtoList(), "Value", "Label");
            return View();
        }
        // get aadress 
        [HttpGet]
        public async Task<IActionResult> UserAddress()
        {
            List<AddressDetailsDto> AddressDtoList = new List<AddressDetailsDto>();
            using (var httpClient = new HttpClient())
            {
                UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                string apiUrl = ApiGlobalUrl + "api/GetUserAddresses?userID=" + Getuser.UserId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    AddressDtoList = JsonConvert.DeserializeObject<List<AddressDetailsDto>>(apiResponse);
                }
            }
            return PartialView("_AddressPartial", AddressDtoList);
        }
        [HttpPost]
        public async Task<IActionResult> AddAddress(AddressDto addressDto)
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            AddressDto addressDtoObj = new AddressDto();
            addressDtoObj.userID = Getuser.UserId;
            addressDtoObj.streetAddress = addressDto.streetAddress;
            addressDtoObj.unit = addressDto.unit;
            addressDtoObj.provinceID = addressDto.provinceID;
            addressDtoObj.cityID = addressDto.cityID;
            addressDtoObj.postalCode = addressDto.postalCode;
            addressDtoObj.addressTitle = addressDto.addressTitle;
            addressDtoObj.addressTypeID = addressDto.addressTypeID;
            addressDtoObj.addressDirections = addressDto.addressDirections;
            addressDtoObj.ipAddress = addressDto.ipAddress;
            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(addressDtoObj), Encoding.UTF8, "application/json");
                string apiUrl = ApiGlobalUrl + "api/AddAddress";

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    //cartDtoObj = null;
                    //JObject json = JObject.Parse(apiResponse);
                    return Json(true);
                }
            }

        }

        //public async Task<IActionResult> selectedDate()
        //{
        //    ViewBag.campaignID = new SelectList(await DateDtoList(), "campaignID", "campaignName");

        //}
        public async Task<List<DateDto>> DateDtoList()
        {
            List<DateDto> DateDtoDtoList = new List<DateDto>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetAllDeliveryDates?todayDateTime=" + DateTime.Now.Date;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    DateDtoDtoList = JsonConvert.DeserializeObject<List<DateDto>>(apiResponse);
                }
            }
            return DateDtoDtoList;
        }
        [HttpGet]
        public async Task<IActionResult> Gettime(string date)
            {
            ViewBag.TimeId = new SelectList(await timeByDate(Convert.ToDateTime(date)), "Value", "Label");

            return PartialView("_TimePartial");

        }
        public async Task<List<TimeDto>> timeByDate(DateTime date)
        {
            DateTime dateTime = Convert.ToDateTime(date);
            List<TimeDto> TimeDtoList = new List<TimeDto>();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string apiUrl = ApiGlobalUrl + "api/GetAllDeliveryTimes?selectedDate=" + DateTime.Now;

                    using (var response = await httpClient.GetAsync(apiUrl))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        TimeDtoList = JsonConvert.DeserializeObject<List<TimeDto>>(apiResponse);
                    }
                }
            }
            catch (Exception wx)
            {

                throw;
            }
            return TimeDtoList;
        }

        [HttpPost]
        public async Task<IActionResult> AddOrder(OrderDto order)
        {
            try
            {
                var res = HttpContext.Session.GetString("userSession");
                UserInfo Getuser = new UserInfo();
                if (res != null)
                {
                    Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                    ViewBag.CityName = Getuser.CityName;

                }
                OrderDto orderDtoObj = new OrderDto();
                orderDtoObj.storeID = Getuser.storeId;
                orderDtoObj.addressID = order.addressID;
                orderDtoObj.qty = order.qty;
                orderDtoObj.userID = Getuser.UserId;
                orderDtoObj.subtotal = order.subtotal;
                orderDtoObj.tax = order.tax;
                orderDtoObj.deliveryCharge = order.deliveryCharge;
                orderDtoObj.serviceCharge = order.serviceCharge;
                orderDtoObj.deliveryChargeTax = order.deliveryChargeTax;
                orderDtoObj.total = order.total;

                orderDtoObj.notes = order.notes;
                orderDtoObj.deliveryDate = order.deliveryDate;
                orderDtoObj.ipAddress = order.ipAddress;
                orderDtoObj.time = order.time;
                orderDtoObj.orderPhone = order.orderPhone;
                orderDtoObj.tip = order.tip;
                orderDtoObj.chargeRef = order.chargeRef;
                orderDtoObj.cardType = order.cardType;
                orderDtoObj.last4 = order.last4;

                orderDtoObj.orderID = 0;


                using (var httpClient = new HttpClient())
                {
                    StringContent contentData = new StringContent(JsonConvert.SerializeObject(orderDtoObj), Encoding.UTF8, "application/json");
                    string apiUrl = ApiGlobalUrl + "api/AddOrder";

                    using (var response = await httpClient.PostAsync(apiUrl, contentData))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();

                        orderDtoObj.orderID = JsonConvert.DeserializeObject<Int64>(apiResponse);

                        #region Send Email to user

                        int emailTemplateID = 2; //  EmailTemplateID = 2 for order confirmation
                        int usertTypeID = 3; // for customer
                        EmailSendGrid.SendEmails(emailTemplateID, Getuser.UserId, 0, orderDtoObj.orderID, usertTypeID);

                        #endregion End Send Email to user

                        #region Send Email to Store

                        int emailTemplateStoreID = 3; //  EmailTemplateID = 3 for store order confirmation
                        int usertTypeStoreID = 2; // for store
                        EmailSendGrid.SendEmails(emailTemplateStoreID, Getuser.UserId, orderDtoObj.storeID, orderDtoObj.orderID, usertTypeStoreID);

                        #endregion End Send Email to Store

                        #region Call and text Store with confirmation

                        try
                        {
                            StoreDto store = new StoreDto();
                            using (var httpClientText = new HttpClient())
                            {
                                string apiUrlText = ApiGlobalUrl + "api/GetStoreDetails?storeID=" + orderDtoObj.storeID;

                                using (var responseText = await httpClientText.GetAsync(apiUrlText))
                                {
                                    string apiResponseText = await responseText.Content.ReadAsStringAsync();
                                    store = JsonConvert.DeserializeObject<StoreDto>(apiResponseText);
                                }
                            }
                            if(store != null)
                            {
                               
                                bool isCall = store.isCall;
                                bool isText = store.isText;
                                string textPhone = store.textPhone;
                                string callPhone = store.callPhone;

                                //Make call
                                if (isCall)
                                {
                                    string callMessage = "Message from Grocery Point. There is a new order.          Please check your text message or email for details. Thank you";
                                    Message.MakeCall(callPhone, callMessage);
                                }

                                //Send Text Message
                                if (isText)
                                {
                                    string textMessage = "Hello there is new order. Click here to view new order. %0a " + ApiGlobalUrl + "stores/order?id=" + orderDtoObj.orderID;
                                    Message.SendTextMessage(textPhone, textMessage);
                                }

                            }
                        }
                        catch (Exception ec)
                        {

                           // throw;
                        }

                        #endregion Call and text Store with confirmation 

                        return Json(new { data = orderDtoObj.orderID });                        
                    }
                }
            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { message = ex.Message });
            }

        }
        public async Task<IActionResult> OrderDetailbyId(int id)
        {
            UserOrderDetails userOrderDetailObj = new UserOrderDetails();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string apiUrl = ApiGlobalUrl + "api/GetUserOrderDetails?orderID=" + id;

                    using (var response = await httpClient.GetAsync(apiUrl))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        userOrderDetailObj = JsonConvert.DeserializeObject<UserOrderDetails>(apiResponse);
                    }
                }
            }
            catch (Exception wx)
            {

                throw;
            }
            return PartialView("_OrderDetailByIdForPopUp", userOrderDetailObj);
        }
        public JsonResult GetTotalWithText()
        {

            return Json(true);
        } 
        public IActionResult Orderplaced()
        {

            return View();
        }
    }
}