﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Campaign;
using GroceryPoint.Dtos.System;
using GroceryPoint.Global;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class AccountsController : Controller
    {
        private readonly IConfiguration _configuration;
        private string ApiGlobalUrl;

      

        public AccountsController(IConfiguration configuration)
        {
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        private SqlConnection con;
        public IActionResult Login()
        {
            var res = HttpContext.Session.GetString("userSession");
            if (res != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = user.CityName;
            }
            return View();
        }

        
        public async Task<IActionResult> EmailVerified(Int64 id, string email)
        {
            
            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(null), Encoding.UTF8, "application/json");
                string apiUrl = ApiGlobalUrl + "api/VerifyEmail?userID="+ id + "&email="+email;
                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != "0" && apiResponse != "")
                    {
                        TempData["Message"] = apiResponse.ToString();
                    }

                }
            }

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(UserInfo userInfo)
        {

            UserInfo userInfoDetail = new UserInfo();
            userInfoDetail.UserName = userInfo.UserName;
            userInfoDetail.Password = userInfo.Password;

            //UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            //userInfoDetail.UserId = 1;
            //userInfoDetail.UserName = userInfo.UserName;
            //userInfoDetail.Password = userInfo.Password;
            //userInfoDetail.CityId = user.CityId;
            using (var httpClient = new HttpClient())
            {

                string apiUrl = ApiGlobalUrl + "api/login?username=" + userInfoDetail.UserName + "&password=" + userInfoDetail.Password;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse == null || apiResponse == "")
                    {
                        ViewBag.error = "Failed to login";
                        return View();
                    }
                    else
                    {
                        apiResponse = await response.Content.ReadAsStringAsync();
                        UserInfo UserInfoAfterLogin = JsonConvert.DeserializeObject<UserInfo>(apiResponse);
                        try
                        {
                            var result = HttpContext.Session.GetString("userSession");
                            if (result != null)
                            {
                                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                                var cityId = user.CityId;

                                user = JsonConvert.DeserializeObject<UserInfo>(apiResponse);
                                UserInfo userInfos = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                                user.CityId = cityId;
                                if (user.CityId == 0 || user.CityId == 0)
                                {
                                    user.CityId = UserInfoAfterLogin.CityId;
                                    user.CityName = UserInfoAfterLogin.CityName;
                                }
                                HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(user));


                                userInfoDetail = userInfos;
                                UserInfo.Current = user;
                                if (user.UserId != 0)
                                {
                                    return RedirectToAction("index", "home");
                                }
                            }
                            else
                            {


                                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(apiResponse);
                                user.UserTypeID = 3;
                                HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(user));

                                UserInfoAfterLogin.CityId = 1;
                                userInfoDetail = user;
                                UserInfo.Current = user;
                                userInfoDetail.CityId = UserInfoAfterLogin.CityId;
                                userInfoDetail.CityName = UserInfoAfterLogin.CityName;
                                HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(userInfoDetail));
                                if (user.UserId != 0)
                                {
                                    return RedirectToAction("index", "home");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ViewBag.error = "faild to login";
                            return View();
                        }

                    }
                }
            }

            return View();
        }
        public async Task<IActionResult> Register()
        {
            var res = HttpContext.Session.GetString("userSession");

            if (res != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = user.CityName;
            }

            ViewBag.campaignID = new SelectList(await CampaignDtoList(), "campaignID", "campaignName");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(UserInfo userInfo)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View();

            //}
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            UserInfo user = new UserInfo();
            user.FirstName = userInfo.FirstName;
            user.LastName = userInfo.LastName;
            user.Email = userInfo.Email;
            user.UserName = userInfo.UserName;
            user.Password = userInfo.Password;
            user.provinceID = userInfo.provinceID;
            user.Telephone = userInfo.Telephone;
            user.CityId = Getuser.CityId;
            user.campaignID = userInfo.campaignID;
            user.CityId = Getuser.CityId;
            UserInfo userResult = new UserInfo();
            using (var httpClient = new HttpClient())
            {
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                string apiUrl = ApiGlobalUrl + "api/addUser";
                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != "0" && apiResponse != "")
                    {
                        var result = HttpContext.Session.GetString("userSession");
                        if (result != null)
                        {
                            userResult = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));

                        }
                        userResult.UserId = Convert.ToInt32(apiResponse);
                        userResult.FirstName = userInfo.FirstName;
                        userResult.LastName = userInfo.LastName;
                        userResult.UserName = userInfo.UserName;
                        userResult.Email = userInfo.UserName;
                        HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(userResult));

                        // send email for verify email                       
                        int emailTemplateID = 1; //  EmailTemplateID = 1 for Verify Email tblEmailtemplate
                        int usertTypeID = 3; // for customer
                        EmailSendGrid.SendEmails(emailTemplateID, userResult.UserId, 0, 0, usertTypeID);


                        return RedirectToAction("Selectstore", "Home");
                    }

                }
            }
            ViewBag.campaignID = new SelectList(await CampaignDtoList(), "campaignID", "campaignName");


            return View();
        }
        public async Task<List<Dtos.Campaign.CampaignDto>> CampaignDtoList()
        {
            List<Dtos.Campaign.CampaignDto> CampaignDtoList = new List<Dtos.Campaign.CampaignDto>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/CampaignList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CampaignDtoList = JsonConvert.DeserializeObject<List<Dtos.Campaign.CampaignDto>>(apiResponse);
                }
            }
            return CampaignDtoList;
        }
        public async Task<List<Province>> ProvinceList()
        {
            List<Province> provinceList = new List<Province>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/provinceList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    provinceList = JsonConvert.DeserializeObject<List<Province>>(apiResponse);
                }
            }
            return provinceList;
        }
        public async Task<IActionResult> GetStart()
        {
            List<City> CityList = new List<City>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/CityList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            ViewBag.CityId = new SelectList(CityList, "CityId", "CityName");

            return View();
        }
        [HttpPost]
        public IActionResult GetStart(City city)
        {
            UserInfo userInfoDetail = new UserInfo();
            userInfoDetail.CityId = city.CityId;
            userInfoDetail.CityName = city.CityName;

            HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(userInfoDetail));
            UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            userInfoDetail = user;
            UserInfo.Current = user;

            return RedirectToAction("register", "accounts");

        }

        [HttpGet]
        public IActionResult GetCity(int? id)
        {
            int Id = Convert.ToInt32(id);
            List<City> cityList = new List<City>();
            cityList = CityByProvinceId(Id);

            ViewBag.CityOptions = new SelectList(cityList, "CityId", "CityName");

            return PartialView("_CityPartial");
        }
        //public ActionResult GetProgram(int SessionId)
        //{
        //    City state = new City();
        //    using (var httpClient = new HttpClient())
        //    {
        //        using (var response = await httpClient.GetAsync("http://localhost:44330/api/stateListById/" + CityId))
        //        {
        //            string apiResponse = await response.Content.ReadAsStringAsync();
        //            state = JsonConvert.DeserializeObject<City>(apiResponse);
        //        }
        //    }

        //    return PartialView("ProgramOptionPartial");
        //}
        public List<City> CityByProvinceId(int id)
        {
            connection();
            List<City> Statelist = new List<City>();

            SqlCommand cmd = new SqlCommand("spSystem_GetCityByProvince", con);
            SqlParameter parameterActive = new SqlParameter();

            SqlParameter provinceID = new SqlParameter();
            provinceID.ParameterName = "@provinceID";
            provinceID.Value = id;
            provinceID.SqlDbType = SqlDbType.NVarChar;
            cmd.Parameters.Add(provinceID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Statelist.Add(
                    new City
                    {
                        CityId = Convert.ToInt32(dr["cityID"]),
                        CityName = Convert.ToString(dr["city"]),
                    });
            }

            return Statelist;
        }
        private void connection()
        {
            string connectionString = _configuration.GetConnectionString("GroceryPointDBConnectionStrings");
            con = new SqlConnection(connectionString);
        }
        public IActionResult logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("getstart", "accounts");
        }
        public IActionResult delapp()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> DeliveryDriver(DeliveryDriverDto deliveryDriverDto)
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            DeliveryDriverDto deliveryDriverDtoObj = new DeliveryDriverDto();
            {

                deliveryDriverDtoObj.deliveryDriverID = 0;
                deliveryDriverDtoObj.cityID = deliveryDriverDto.cityID;
                deliveryDriverDtoObj.provinceID = deliveryDriverDto.provinceID;
                deliveryDriverDtoObj.phone = deliveryDriverDto.phone;
                deliveryDriverDtoObj.email = deliveryDriverDto.email;
                deliveryDriverDtoObj.postalCode = deliveryDriverDto.postalCode;
                deliveryDriverDtoObj.firstName = deliveryDriverDto.firstName;
                deliveryDriverDtoObj.lastName = deliveryDriverDto.lastName;
                deliveryDriverDtoObj.ipAddress = deliveryDriverDto.ipAddress;
                deliveryDriverDtoObj.dateOfBirth = deliveryDriverDto.dateOfBirth;
                deliveryDriverDtoObj.isSundayMorning = deliveryDriverDto.isSundayMorning;
                deliveryDriverDtoObj.isSundayEvening = deliveryDriverDto.isSundayEvening;
                deliveryDriverDtoObj.isMondayMorning = deliveryDriverDto.isMondayMorning;
                deliveryDriverDtoObj.isMondayAfternoon = deliveryDriverDto.isMondayAfternoon;
                deliveryDriverDtoObj.isMondayEvening = deliveryDriverDto.isMondayEvening;
                deliveryDriverDtoObj.isTuesdayMorning = deliveryDriverDto.isTuesdayMorning;
                deliveryDriverDtoObj.isTuesdayAfternoon = deliveryDriverDto.isTuesdayAfternoon;
                deliveryDriverDtoObj.isTuesdayEvening = deliveryDriverDto.isTuesdayEvening;
                deliveryDriverDtoObj.isWednesdayMorning = deliveryDriverDto.isWednesdayMorning;
                deliveryDriverDtoObj.isWednesdayAfternoon = deliveryDriverDto.isWednesdayAfternoon;
                deliveryDriverDtoObj.isWednesdayEvening = deliveryDriverDto.isWednesdayEvening;
                deliveryDriverDtoObj.isThursdayMorning = deliveryDriverDto.isThursdayMorning;
                deliveryDriverDtoObj.isThursdayAfternoon = deliveryDriverDto.isThursdayAfternoon;
                deliveryDriverDtoObj.isThursdayEvening = deliveryDriverDto.isThursdayEvening;
                deliveryDriverDtoObj.isFridayMorning = deliveryDriverDto.isFridayMorning;
                deliveryDriverDtoObj.isFridayAfternoon = deliveryDriverDto.isFridayAfternoon;
                deliveryDriverDtoObj.isFridayEvening = deliveryDriverDto.isFridayEvening;
                deliveryDriverDtoObj.isSaturdayMorning = deliveryDriverDto.isSaturdayMorning;
                deliveryDriverDtoObj.isSaturdayAfternoon = deliveryDriverDto.isSaturdayAfternoon;
                deliveryDriverDtoObj.hoursWork = deliveryDriverDto.hoursWork;
                deliveryDriverDtoObj.driverLicense = deliveryDriverDto.driverLicense;
                deliveryDriverDtoObj.isLiftWeight = deliveryDriverDto.isLiftWeight;
                deliveryDriverDtoObj.campaignID = deliveryDriverDto.campaignID;
                deliveryDriverDtoObj.description = deliveryDriverDto.description;

                using (var httpClient = new HttpClient())
                {
                    string apiUrl = ApiGlobalUrl + "api/AddDeliveryDriver";
                    StringContent contentData = new StringContent(JsonConvert.SerializeObject(deliveryDriverDtoObj), Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(apiUrl, contentData))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        //cartDtoObj = null;
                        //JObject json = JObject.Parse(apiResponse);
                        return Json(true);
                    }
                }
            }

        }
        public async Task<IActionResult> Shop()
        {
            ViewBag.cityID = new SelectList(await CityList(), "CityId", "CityName");
            ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr");
            ViewBag.campaignID = new SelectList(await CampaignDtoList(), "campaignID", "campaignName");
            return View();
        }
        public async Task<List<City>> CityList()
        {
            List<City> CityList = new List<City>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/CityList";

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            return CityList;
        }
    }
}