﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GroceryPoint.Models;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using GroceryPoint.Dtos.Store;

namespace GroceryPoint.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;
        private string ApiGlobalUrl;

        private int pageSiz;
        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            pageSiz = configuration.GetValue<int>("pageSize");
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");

        }

        public async Task<IActionResult> Index()
        {
            UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            ViewBag.usr1 = user.UserName;
            ViewBag.userid = user.UserId;
            return View();
        }


        public async Task<IActionResult> Selectstore()
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            List<StoreSummaryDto> StoreSummaryDtoList = new List<StoreSummaryDto>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetAllStoresByCity?cityID=" + Getuser.CityId;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    StoreSummaryDtoList = JsonConvert.DeserializeObject<List<StoreSummaryDto>>(apiResponse);
                }
            }
            return View(StoreSummaryDtoList);
        }
        public async Task<IActionResult> StorePartial(string cityId)
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            List<StoreSummaryDto> StoreSummaryDtoList = new List<StoreSummaryDto>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetAllStoresByCity?cityID=" + Getuser.CityId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    StoreSummaryDtoList = JsonConvert.DeserializeObject<List<StoreSummaryDto>>(apiResponse);
                }
            }
            return PartialView("_StorePartial", StoreSummaryDtoList);
        }
        public async Task<JsonResult> chooseStore(string id, string name)
        {
            var res = HttpContext.Session.GetString("userSession");

            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                Getuser.StoreName = name;
                Getuser.storeId = Convert.ToInt32(id);

            }
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/UpdateDefaultStore?userID=" + Getuser.UserId + "&storeID=" + id;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(Getuser));
                    return Json(Getuser.StoreName);
                }
            }
        }
        public async Task<IActionResult> Product(int id, int pageNo = 1, int next = 0, int prev = 0)
        {
            //int pageSiz = 3;
            if (id == 0)
            {
                id = 1;
            }

            List<ProductDto> ProductList = new List<ProductDto>();
            using (var httpClient = new HttpClient())
            {
                UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                string apiUrl = ApiGlobalUrl + "api/GetStoreProducts?categoryID=" + id+ "&storeID=" + Getuser.storeId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ViewBag.StoreName = Getuser.StoreName.Trim();
                    ViewData["pageSize"] = pageSiz;
                    ProductList = JsonConvert.DeserializeObject<List<ProductDto>>(apiResponse);
                }
            }
            if (next > 0)
            {
                pageNo++;
            }
            else if (prev > 0 && pageNo != 1)
            {
                pageNo--;
            }
            string PageSize = pageSiz.ToString();
            ViewData["PaginationActive"] = PageSize;
            ViewData["SelectedPage"] = pageNo;
            ViewData["TotalReturnPage"] = ProductList.Count;
            ViewData["NoOfPage"] = Math.Ceiling(Convert.ToDecimal(ProductList.Count) / pageSiz);
            return PartialView("_ProductPartial", ProductList.Skip((pageNo - 1) * pageSiz).Take(pageSiz));
        }
        public async Task<IActionResult> Category()
        {
            List<Category> CategoryList = new List<Category>();
            using (var httpClient = new HttpClient())
            {
                UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                string apiUrl = ApiGlobalUrl + "api/CategoryList?storeid=" + Getuser.storeId;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CategoryList = JsonConvert.DeserializeObject<List<Category>>(apiResponse);
                }
            }
            return PartialView("_CategoryPartial", CategoryList);
        }
        public async Task<IActionResult> Search(string searchString)
        {
            UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            ViewBag.usr1 = user.UserName;
            ViewBag.userid = user.UserId;
            List<ProductDto> ProductList = new List<ProductDto>();
            using (var httpClient = new HttpClient())
            {
                UserInfo Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                string apiUrl = ApiGlobalUrl + "api/SearchStore?storeID=" + Getuser.storeId + "&searchPar=" + searchString;

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProductList = JsonConvert.DeserializeObject<List<ProductDto>>(apiResponse);
                }
            }
            return View(ProductList);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public async Task<IActionResult> GetStart()
        {
            List<City> CityList = new List<City>();
            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/SCityList";
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            ViewBag.CityId = new SelectList(CityList, "CityId", "CityName");

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetStart(City city)
        {

            return View();
        }
        public IActionResult CheckOut()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }
        public IActionResult Faq()
        {
            return View();
        } 
        public IActionResult Howitworks()
        {
            return View();
        } public IActionResult TermsandCondition()
        {
            return View();
        }
        public IActionResult Bshopper()
        {
            return View();
        }


    }
}
