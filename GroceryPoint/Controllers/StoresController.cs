﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GroceryPoint.Areas.Area.Controllers;
using GroceryPoint.Controllers;
using GroceryPoint.Dtos.Store;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using GroceryPoint.Global; 

namespace GroceryPoint.Area.Area.Controllers
{
    public class StoresController : Controller
    {
        private readonly IConfiguration _configuration;
        [Obsolete]
        private readonly IHostingEnvironment _appEnvironment;
        private string ApiGlobalUrl;

        public IActionResult dashboard()
        {
            return View();
        }
        [Obsolete]
        public StoresController(IConfiguration configuration, IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
            _configuration = configuration;
            ApiGlobalUrl = configuration.GetValue<string>("apiUrl");
        }
        public async Task<IActionResult> Selectstore()
        {
            var res = HttpContext.Session.GetString("userSession");
            UserInfo Getuser = new UserInfo();
            if (res != null)
            {
                Getuser = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = Getuser.CityName;

            }
            List<StoreSummaryDto> StoreSummaryDtoList = new List<StoreSummaryDto>();

            using (var httpClient = new HttpClient())
            {
                string apiUrl = ApiGlobalUrl + "api/GetAllStoresByCity?cityID=" + 1;
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    StoreSummaryDtoList = JsonConvert.DeserializeObject<List<StoreSummaryDto>>(apiResponse);
                }
            }
            return View(StoreSummaryDtoList);
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }
        public async Task<IActionResult> AddStore()
        {

            ViewBag.CityId = new SelectList(await CityList(), "CityId", "CityName");
            ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr");

            return View();
        }
        public IActionResult Login()
        {
            var res = HttpContext.Session.GetString("userSession");
            if (res != null)
            {
                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                ViewBag.CityName = user.CityName;
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(UserInfo userInfo)
        {

            UserInfo userInfoDetail = new UserInfo();
            userInfoDetail.UserName = userInfo.Email;
            userInfoDetail.Password = userInfo.Password;

            //UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
            //userInfoDetail.UserId = 1;
            //userInfoDetail.UserName = userInfo.UserName;
            //userInfoDetail.Password = userInfo.Password;
            //userInfoDetail.CityId = user.CityId;
            string apiUrl = ApiGlobalUrl + "api/login?username=" + userInfoDetail.UserName + "&password=" + userInfoDetail.Password;

            using (var httpClient = new HttpClient())
            {


                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse == null || apiResponse == "")
                    {
                        ViewBag.error = "Failed to login";
                        return View();
                    }
                    else
                    {
                        apiResponse = await response.Content.ReadAsStringAsync();
                        UserInfo UserInfoAfterLogin = JsonConvert.DeserializeObject<UserInfo>(apiResponse);
                        try
                        {
                            var result = HttpContext.Session.GetString("userSession");
                            if (result != null)
                            {
                                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                                var cityId = UserInfoAfterLogin.CityId;

                                user = JsonConvert.DeserializeObject<UserInfo>(apiResponse);
                                UserInfo userInfos = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));
                                user.CityId = cityId;
                                userInfo.UserTypeID = UserInfoAfterLogin.UserTypeID;
                                if (user.CityId == 0 || user.CityId == 0)
                                {
                                    user.CityId = UserInfoAfterLogin.CityId;
                                    user.CityName = UserInfoAfterLogin.CityName;
                                }
                                //user.UserTypeID = 2;
                                HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(user));


                                userInfoDetail = userInfos;
                                UserInfo.Current = user;
                                //user.UserTypeID = 2;
                                if (user.UserId != 0 && user.UserTypeID == 2)
                                {

                                    return RedirectToAction("dashboard", "stores");
                                }
                            }
                            else
                            {


                                UserInfo user = JsonConvert.DeserializeObject<UserInfo>(apiResponse);

                                HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(user));

                                UserInfoAfterLogin.CityId = 1;
                                userInfo.UserTypeID = UserInfoAfterLogin.UserTypeID;
                                userInfoDetail = user;
                                UserInfo.Current = user;
                                userInfoDetail.CityId = UserInfoAfterLogin.CityId;
                                userInfoDetail.CityName = UserInfoAfterLogin.CityName;
                                HttpContext.Session.SetString("userSession", JsonConvert.SerializeObject(userInfoDetail));
                                //user.UserTypeID = 2;

                                if (user.UserId != 0 && UserInfoAfterLogin.UserTypeID == 2)
                                {

                                    return RedirectToAction("dashboard", "stores");

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ViewBag.error = "faild to login";
                            return View();
                        }

                    }
                }
            }

            return View();
        }
        [HttpPost]
        [Obsolete]
        public async Task<IActionResult> AddStore(StoreDto storeDto, IFormFile file)
        {
            string path_Root = _appEnvironment.WebRootPath;
            string filename = file.FileName;
            string filepath = path_Root + "//StoreImages";
            string FileNameForDb = Guid.NewGuid() + file.FileName;

            //if (!(Directory.Exists(path_Root + "//StoreImages//" + StorName)))
            //{
            //    Directory.CreateDirectory(path_Root + "//ProductImages//" + StorName);
            //}

            string saveImg = path_Root + "\\StoreImages\\" + FileNameForDb;
            using (var stream = new FileStream(saveImg, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            string apiUrl = ApiGlobalUrl + "api/AddStore?password=" + storeDto.password;

            StoreDto _storeDto = new StoreDto();
            _storeDto.storeName = storeDto.storeName;
            _storeDto.streetAddress = storeDto.streetAddress;
            _storeDto.unit = storeDto.unit;
            _storeDto.cityID = storeDto.cityID;
            _storeDto.provinceID = storeDto.provinceID;
            _storeDto.logo = FileNameForDb;
            _storeDto.phone = storeDto.phone;
            _storeDto.email = storeDto.email;
            _storeDto.postalCode = storeDto.postalCode;
            _storeDto.firstName = storeDto.firstName;
            _storeDto.lastName = storeDto.lastName;

            _storeDto.ipAddress = "1";



            using (var httpClient = new HttpClient())
            {
                var res = JsonConvert.SerializeObject(_storeDto);
                StringContent contentData = new StringContent(JsonConvert.SerializeObject(_storeDto), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync(apiUrl, contentData))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    if (apiResponse != null)
                    {
                        TempData["success"] = "Added Successfully";

                        return RedirectToAction("login", "stores");
                    }
                    //_productDto = JsonConvert.DeserializeObject<ProductUnitDto>(apiResponse);
                }
            }


            //ViewBag.FileNameForDb = FileNameForDb;

            ViewBag.CityId = new SelectList(await CityList(), "CityId", "CityName");
            ViewBag.provinceID = new SelectList(await ProvinceList(), "provinceID", "provinceAbbr");

            return View();
        }
        public async Task<List<City>> CityList()
        {
            string apiUrl = ApiGlobalUrl + "api/CityList";

            List<City> CityList = new List<City>();

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    CityList = JsonConvert.DeserializeObject<List<City>>(apiResponse);
                }
            }
            return CityList;
        }
        public async Task<List<Province>> ProvinceList()
        {
            string apiUrl = ApiGlobalUrl + "api/provinceList";

            List<Province> ProvinceList = new List<Province>();

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ProvinceList = JsonConvert.DeserializeObject<List<Province>>(apiResponse);
                }
            }
            return ProvinceList;
        }

        public async Task<IActionResult> order(int? id)
        {
            ViewBag.Id = id;


            return View();

        }
        public async Task<IActionResult> orderView(int? id)
        {

            string apiUrl = ApiGlobalUrl + "api/GetStoreOrderDetails?orderID=" + id;

            StoreOrderDetailsDto storeOrderDetailsDtoObj = new StoreOrderDetailsDto();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync(apiUrl))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        storeOrderDetailsDtoObj = JsonConvert.DeserializeObject<StoreOrderDetailsDto>(apiResponse);
                        //ViewBag.OrderStatusId = new SelectList(await OrderStatusList(), "value", "label",
                        //    storeOrderDetailsDtoObj.StatusId);

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return PartialView("_storeOrderDetailsById", storeOrderDetailsDtoObj);

        }
        public async Task<JsonResult> UpdateOrderStatus(int orderID, int orderStatusID)
        {
            #region Get Order Details
            string apiUrlOrder = ApiGlobalUrl + "api/GetStoreOrderBasicData?orderID=" + orderID;
            StoreOrderDetailsDto orderDetailsDto = new StoreOrderDetailsDto();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrlOrder))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    orderDetailsDto = JsonConvert.DeserializeObject<StoreOrderDetailsDto>(apiResponse);     
                }
            }

            #endregion End Order Details

            string apiUrl = ApiGlobalUrl + "api/UpdateOrderStatus?orderID=" + orderID + "&orderStatusID=" + orderStatusID + "&userID=1";
            List<StoreOrderDto> ProductList = new List<StoreOrderDto>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();

                    //Charge Credit card
                    if (orderDetailsDto != null)
                    {
                        bool isPaid = orderDetailsDto.isPaid;
                        string chargeID = orderDetailsDto.chargeRef;
                        string total = orderDetailsDto.total.ToString();
                        if (!isPaid && (orderStatusID == 2 || orderStatusID == 3 || orderStatusID == 4 || orderStatusID == 5))
                        {
                            Global.Stripe obj = new Global.Stripe();
                            obj.CaptureCharge(chargeID, total);
                        }
                    }                   

                    return Json(apiResponse);
                }
            }
        }
        public async Task<JsonResult> DeleteOrderItem(Int64? orderDetailsID)
        {

            string apiUrl = ApiGlobalUrl + "api/DeleteOrderItem?orderDetailsID=" + orderDetailsID;

            using (var httpClient = new HttpClient())
            {
               

                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    //string StorName = Getuser.StoreName.Trim();

                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Json(apiResponse);
                }
            }
        }
        public async Task<JsonResult> UpdateOrderItem(Int64? orderDetailsID , int qty)
        {
            string apiUrl = ApiGlobalUrl + "api/UpdateOrderItem?orderDetailsID=" + orderDetailsID+ "&qty="+ qty+ "&userID"+0;
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiUrl))
                {
                    //string StorName = Getuser.StoreName.Trim();
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Json(apiResponse);
                }
            }
        }
        
    }
}