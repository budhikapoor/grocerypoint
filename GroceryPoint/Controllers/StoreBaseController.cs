﻿using System;
using System.Collections.Generic;
using System.Linq;
using GroceryPoint.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace GroceryPoint.Controllers
{
    public class StoreBaseController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        var res = HttpContext.Session.GetString("userSession");
        if (res != null)
        {
            UserInfo user = JsonConvert.DeserializeObject<UserInfo>(HttpContext.Session.GetString("userSession"));

            if (user.storeId == 0 || user.UserTypeID!= 2 )
            {
                filterContext.Result = new RedirectResult(Url.Action("login", "stores"));

            }
            else if (user.UserId == 0)
                filterContext.Result = new RedirectResult(Url.Action("Login", "Accounts"));
            //else
            //    UserInfo.Current = user;
        }
        else
        {
            filterContext.Result = new RedirectResult(Url.Action("login", "stores"));

        }

    }
}
}