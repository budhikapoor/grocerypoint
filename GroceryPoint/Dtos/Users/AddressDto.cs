﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Users
{
    public class AddressDto
    {       
        public Int64 addressID { get; set; }
        public Int64 userID { get; set; }
        public string streetAddress { get; set; }
        public string unit { get; set; }
        public Int64 cityID { get; set; }
        public Int64 provinceID { get; set; }
        public string postalCode { get; set; }

        public string addressTitle { get; set; }
        public Int64 addressTypeID { get; set; }
        public string addressDirections { get; set; }

        public string ipAddress { get; set; }

    }
}
