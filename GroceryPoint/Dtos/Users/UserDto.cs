﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Users
{
    public class UserDto
    {
		public Int64 userID { get; set; }	
		public string email { get; set; }	
		public Int32 userTypeID { get; set; }		
		public string userFirstName { get; set; }
		public string userLastName { get; set; }
		public string phone { get; set; }
		public bool isEmailVerified { get; set; }
		public string phoneShow { get; set; }
		public string ext { get; set; }
		public Int64 storeID { get; set; }
		public string storeName { get; set; }
		public Int64 cityID { get; set; }
		public string city { get; set; }

		public Int64 provinceID { get; set; }
		public string province { get; set; }

		public string ipAddress { get; set; }
		public string postalCode { get; set; }
	}
}
