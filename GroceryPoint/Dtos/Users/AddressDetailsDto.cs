﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Users
{
    public class AddressDetailsDto
    {
        public Int64 addressID { get; set; }
        public Int64 userID { get; set; }
        public string streetAddress { get; set; }
        public string unit { get; set; }
        public string city { get; set; }
        public string province { get; set; }
        public string postalCode { get; set; }

        public string addressTitle { get; set; }
        public string addressType { get; set; }
        public string addressDirections { get; set; }
        public bool isSelected { get; set; }
    }
}
