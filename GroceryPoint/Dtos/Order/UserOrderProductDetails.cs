﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Order;

namespace GroceryPoint.Dtos.Order
{
    public class UserOrderProductDetails
    {
        public Int64 productID { get; set; }
        public int qty { get; set; }
        public decimal price { get; set; }      
        public decimal subtotal { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }
        public Int64 userID { get; set; }

        public string notes { get; set; }       

        public string productName { get; set; }

        public string productUnit { get; set; }        

    }
}
