﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Order
{
    public class OrderDto
    {
        public Int64 orderID {get;set;}
        public Int64 userID {get;set;}
        public Int64 addressID {get;set;}     
        public int qty { get; set; }
        public decimal subtotal    {get;set;}
        public decimal tax {get;set;}
        public decimal serviceCharge   {get;set;}
        public decimal deliveryCharge  {get;set;}
        public decimal total   {get;set;}
        public string ipAddress   {get;set;}
       
        public Int64 storeID {get;set;}
        public string notes   {get;set;}
        public int orderStatusID { get; set; }
        public DateTime deliveryDate    {get;set;}
        public decimal deliveryChargeTax   {get;set;}
        public string orderPhone  {get;set;}

        public string chargeRef { get; set; }
        public bool isPaid { get; set; }
        public decimal tip {get;set;}
        public string time { get; set; }
        public DateTime orderdate { get; set; }
        public TimeSpan ordertime { get; set; }
        public string cardType { get; set; }
        public string last4 { get; set; }
    }
}
