﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Order
{
    public class UserOrdersDto
    {
        public Int64 orderID { get; set; }
        public string orderNumber { get; set; }
        public string storeName { get; set; }        
        public Int32 qty { get; set; }
        public decimal subtotal { get; set; }
        public decimal tax { get; set; }       
        public decimal total { get; set; }
        public string orderDate { get; set; }
        public string orderStatus { get; set; }      
    }
}
