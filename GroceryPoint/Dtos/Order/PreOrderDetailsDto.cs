﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Order
{
    public class PreOrderDetailsDto
    {
        public decimal productSubtotal { get; set; }
        public Int64 totalItems { get; set; }
        public decimal productTax { get; set; }
        public decimal deliveryCharge { get; set; }
        public decimal deliveryChargeTax { get; set; }
        public decimal totalWithoutTips { get; set; }
    }
}
