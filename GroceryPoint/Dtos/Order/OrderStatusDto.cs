﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Order
{
    public class OrderStatusDto
    {
        public int value { get; set; }
        public string label { get; set; }
    }
}
