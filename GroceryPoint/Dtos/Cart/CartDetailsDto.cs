﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GroceryPoint.Dtos.Store;

namespace GroceryPoint.Dtos.Cart
{
    
    public class CartDetailsDto
    {
        public Int64 cartID { get; set; }
        public Int64 productID { get; set; }
        public Int64 qty { get; set; }
        public decimal price { get; set; }

        public string image { get; set; }
        public decimal subtotal { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }
        public Int64 userID { get; set; }      
       
        public string notes { get; set; }
        public Int64 storeID { get; set; }

        public string productName { get; set; }

        public string productUnit { get; set; }
        public CartSummaryDto cartSummary { get; set; }
        public StoreSummaryDto storeSummary { get; set; }
    }
}
