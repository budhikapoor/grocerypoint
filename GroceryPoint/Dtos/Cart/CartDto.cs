﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Cart
{
    public class CartDto
    {
        public Int64 cartID { get; set; }
        public Int64 productID { get; set; }
        public Int64 qty { get; set; }
        public decimal price { get; set; }
        public decimal subtotal { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }
        public Int64 userID { get; set; }
        public DateTime dateAdded { get; set; }
        public Int64 addedUserID { get; set; }
        public DateTime dateUpdated { get; set; }
        public Int64 updatedUserID { get; set; }
        public string ipAddress { get; set; }
        public string ipAddressUpdated { get; set; }
        public string notes { get; set; }
        public Int64 storeID { get; set; }
    }

    public class CartSummaryDto
    {
        public Int64 TotalItems { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalTax { get; set; }
    }
    public class CartSummary
    {
        public int totalItems { get; set; }
        public double totalCost { get; set; }
    }
    //Fazi

    public class StoreSummary
    {
        public int storeID { get; set; }
        public string storeName { get; set; }
        public string city { get; set; }
    }

    public class cart
    {
        public int cartID { get; set; }
        public int productID { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public string image { get; set; }
        public double subtotal { get; set; }
        public double tax { get; set; }
        public double total { get; set; }
        public int userID { get; set; }
        public object notes { get; set; }
        public int storeID { get; set; }
        public string productName { get; set; }
        public string productUnit { get; set; }
        public CartSummary cartSummary { get; set; }
        public StoreSummary storeSummary { get; set; }
    }
}
