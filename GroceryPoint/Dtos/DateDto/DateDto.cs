﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.DateDto
{
    public class DateDto
    {
        public string Label { get; set; }
        public DateTime Value { get; set; }
    }
    public class TimeDto
    {
        public string Label { get; set; }
        public TimeSpan Value { get; set; }
    }
}
