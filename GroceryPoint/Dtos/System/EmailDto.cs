﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.System
{
    public class EmailDto
    {
        public string fromEmail { get; set; }
	    public string subject { get; set; }
        public string body { get; set; }
        public string toEmail { get; set; }
        public string displayName { get; set; }
    }
}
