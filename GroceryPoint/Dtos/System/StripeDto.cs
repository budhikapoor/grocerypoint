﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.System
{
    public class StripeDto
    {
        public string SecretKey { get; set; }
        public string PublishableKey { get; set; }
    }
}
