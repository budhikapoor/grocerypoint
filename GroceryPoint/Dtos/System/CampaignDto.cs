﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.System
{
    public class CampaignDto
    {
        public Int64 campaignID { get; set; }
        public string campaign { get; set; }
        public bool isActive { get; set; }
    }
}
