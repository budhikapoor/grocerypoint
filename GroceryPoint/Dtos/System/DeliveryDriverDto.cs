﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.System
{
    public class DeliveryDriverDto
    {
        public Int64 deliveryDriverID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public DateTime dateOfBirth { get; set; }
        public string postalCode { get; set; }
        public Int64 provinceID { get; set; }
        public Int64 cityID { get; set; }
        public bool isSundayMorning { get; set; }
        public bool isSundayEvening { get; set; }
        public bool isMondayMorning { get; set; }
        public bool isMondayAfternoon { get; set; }
        public bool isMondayEvening { get; set; }
        public bool isTuesdayMorning { get; set; }
        public bool isTuesdayAfternoon { get; set; }
        public bool isTuesdayEvening { get; set; }
        public bool isWednesdayMorning { get; set; }
        public bool isWednesdayAfternoon { get; set; }
        public bool isWednesdayEvening { get; set; }
        public bool isThursdayMorning { get; set; }
        public bool isThursdayAfternoon { get; set; }
        public bool isThursdayEvening { get; set; }
        public bool isFridayMorning { get; set; }
        public bool isFridayAfternoon { get; set; }
        public bool isFridayEvening { get; set; }
        public bool isSaturdayMorning { get; set; }
        public bool isSaturdayAfternoon { get; set; }
        public bool isSaturdayEvening { get; set; }
        public int hoursWork { get; set; }
        public string driverLicense   { get; set; }
        public bool isLiftWeight    { get; set; }
        public int campaignID  { get; set; }
        public string description { get; set; }
     
        public string ipAddress   { get; set; }
    }
}
