﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Campaign
{
    public class CampaignDto
    {
        public Int64 campaignID { get; set; }
        public string campaignName { get; set; }
    }
}
