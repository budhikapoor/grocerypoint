﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Store
{
    public class ProductDto
    {
        public string productName { get; set; }
        public decimal price { get; set; }
        public bool isGST { get; set; }
        public Int64 totalQuantity { get; set; }
        public Int64 productUnitID { get; set; }
        public Int64 maxLimit { get; set; }
        public Int64 minLimit { get; set; }
        public Int64 subCategoryID { get; set; }
        public Int64 categoyID { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public Int64 storeID { get; set; }
        public DateTime dateAdded { get; set; }
        public Int64 addedUserID { get; set; }
        public DateTime dateUpdated { get; set; }
        public Int64 updatedUserID { get; set; }
        public string ipAddress { get; set; }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }
        public Int64 productID { get; set; }

        public string category { get; set; }
        public string productUnit { get; set; }
    }
}
