﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Store
{
    public class StoreOrderDetailsDto
    {
        public Int64 orderID { get; set; }       

        public string customerName { get; set; }
        public string orderNumber { get; set; }
        public string storeName { get; set; }
        public Int32 qty { get; set; }
        public decimal subtotal { get; set; }
        public decimal tax { get; set; }       
        public string orderDate { get; set; }
        public string deliveryDate { get; set; }

        public string orderInstructions { get; set; }
        public string orderStatus { get; set; }
        public int StatusId { get; set; }
        public string orderPhone { get; set; }
        public string deliveryAddress { get; set; }
        public decimal total { get; set; }

        public decimal clientTotal { get; set; }
        public decimal storeTotal { get; set; }
        public decimal amountPaid { get; set; }
        public decimal percentage { get; set; }

        public bool isPaid { get; set; }
        public string chargeRef { get; set; }
        public IEnumerable<StoreOrderProductDetailsDto> orderProductList { get; set; }
    }
}
