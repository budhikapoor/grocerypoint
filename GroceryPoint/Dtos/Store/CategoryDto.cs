﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Store
{
    public class CategoryDto
    {
        public Int64 categoryID {get;set;}
        public string category { get; set; }
        public int sequence { get; set; }
        public bool isActive    {get;set;}
        public DateTime dateAdded   {get;set;}
        public Int64 addedUserID {get;set;}
        public DateTime dateUpdated {get;set;}
        public Int64 updatedUserID   {get;set;}
        public Int64 storeID {get;set;}
    }
}
