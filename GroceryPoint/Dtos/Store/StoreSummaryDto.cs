﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Store
{
    public class StoreSummaryDto
    {
        public Int64 storeID { get; set; }
        public string storeName { get; set; }
        public string city { get; set; }

        public string logo { get; set; }
        public string streetAddress { get; set; }
        public string province { get; set; }
        public string postalCode { get; set; }

        public string callPhone { get; set; }
        public string textPhone { get; set; }
        public bool isText { get; set; }

        public bool isCall { get; set; }
    }
}
