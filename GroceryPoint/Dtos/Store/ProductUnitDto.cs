﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Store
{
    public class ProductUnitDto
    {
        public Int64 productUnitID { get; set; }
        public string productUnit { get; set; }
        public bool isActive { get; set; }
        public int sequence { get; set; }
        public Int64 storeID { get; set; }
    }
}
