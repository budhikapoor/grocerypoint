﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroceryPoint.Dtos.Store
{
    public class StoreDto
    {
        public Int64 storeID {get; set; }
        public string storeName {get; set; }
        public string streetAddress {get; set; }
        public string unit {get; set; }
        public Int64 cityID {get; set; }
        public Int64 provinceID {get; set; }       
       
        public string logo {get; set; }
        public string phone {get; set; }
        public string email {get; set; }
       
        public string postalCode {get; set; }
        public decimal percentage { get; set; }

        public string ipAddress { get; set; }
        public string password{ get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

        public string textPhone { get; set; }
        public string callPhone { get; set; }
        public bool isCall { get; set; }
        public bool isText { get; set; }
    }
}
